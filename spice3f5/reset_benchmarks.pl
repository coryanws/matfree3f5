#!/usr/bin/perl -w
use File::Copy;

@files = <./../benchmarks/circuit_benchmarks/*/*.*>;
foreach $file (@files) {

  @frags = split(/\//, $file);
  $fname = $frags[-1];
  @namefrags = split(/\./, $fname);
  $ext = $namefrags[-1];

#  print $file . "\n";
#  print $fname . "\n";
#  print $ext . "\n";
  
  #if it's a .cir or a .sp, we can run it.
  if($ext =~ /cir/ || $ext =~ /sp/){
#    print "runnable!!\n";
     print "now running " . $fname . "...";
     system("./src/bin/spice3", "-b", $file);
     print " complete!\n";
     
     #create and clear a place for files to go.
     #if directory does not exist, make it.
     mkdir("./../graphs/perlgen/".$namefrags[-2]);
     
     #delete stuff already in this directory.
     @contents = <"./../graphs/perlgen/".$namefrags[-2]."/*">;
     foreach $content (@contents){
       system("rm", $content);
     }
     
     #rename and move generated output files.
     @outputs = <./../graphs/raw/*>;
     foreach $output (@outputs) {
     
       @opname = split(/\//, $output);
       #move files to directory.
       move($output, "./../graphs/perlgen/".$namefrags[-2]."/".$opname[-1]);

     }
     
     #rename .cir to let us know that it's done.
     move($file, $file."done");
     
  }
#  print "\n";
}
