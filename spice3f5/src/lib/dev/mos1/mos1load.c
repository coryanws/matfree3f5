/**********
Copyright 1990 Regents of the University of California.  All rights reserved.
Author: 1985 Thomas L. Quarles
**********/

#include "spice.h"
#include <stdio.h>
#include "cktdefs.h"
#include "devdefs.h"
#include "mos1defs.h"
#include "util.h"
#include "trandefs.h"
#include "const.h"
#include "sperror.h"
#include "suffix.h"

#include "matfree_interface.h"

int
MOS1load(inModel,ckt)
    GENmodel *inModel;
    register CKTcircuit *ckt;
        /* actually load the current value into the 
         * sparse matrix previously provided 
         */
{
  //matfree stuff
  
    double matfree_GG = 0;
    double matfree_GD = 0;
    double matfree_GB = 0;
    double matfree_GS = 0;
    
    double matfree_DD = 0;
    double matfree_DB = 0;
    double matfree_DS = 0;
    double matfree_DG = 0;
    
    double matfree_BB = 0;
    double matfree_BS = 0;
    double matfree_BG = 0;
    double matfree_BD = 0;
    
    double matfree_SS = 0;
    double matfree_SG = 0;
    double matfree_SD = 0;
    double matfree_SB = 0;
    
  //end matfree


    register MOS1model *model = (MOS1model *) inModel;
    register MOS1instance *here;
    double Beta;
    double DrainSatCur;
    double EffectiveLength;
    double GateBulkOverlapCap;
    double GateDrainOverlapCap;
    double GateSourceOverlapCap;
    double OxideCap;
    double SourceSatCur;
    double arg;
    double cbhat;
    double cdhat;
    double cdrain;
    double cdreq;
    double ceq;
    double ceqbd;
    double ceqbs;
    double ceqgb;
    double ceqgd;
    double ceqgs;
    double delvbd;
    double delvbs;
    double delvds;
    double delvgd;
    double delvgs;
    double evbd;
    double evbs;
    double gcgb;
    double gcgd;
    double gcgs;
    double geq;
    double sarg;
    double sargsw;
    double tol;
    double vbd;
    double vbs;
    double vds;
    double vdsat;
    double vgb1;
    double vgb;
    double vgd1;
    double vgd;
    double vgdo;
    double vgs1;
    double vgs;
    double von;
    double vt;
    double xfact;
    int xnrm;
    int xrev;
    double capgs;   /* total gate-source capacitance */
    double capgd;   /* total gate-drain capacitance */
    double capgb;   /* total gate-bulk capacitance */
    int Check;
#ifndef NOBYPASS
    double tempv;
#endif /*NOBYPASS*/
    int error;
#ifdef CAPBYPASS
    int senflag;
#endif /* CAPBYPASS */ 
    int SenCond;


#ifdef CAPBYPASS
    senflag = 0;
    if(ckt->CKTsenInfo && ckt->CKTsenInfo->SENstatus == PERTURBATION &&
        (ckt->CKTsenInfo->SENmode & (ACSEN | TRANSEN))) {
        senflag = 1;
    }
#endif /* CAPBYPASS */ 

    /*  loop through all the MOS1 device models */
    for( ; model != NULL; model = model->MOS1nextModel ) {

        /* loop through all the instances of the model */
        for (here = model->MOS1instances; here != NULL ;
                here=here->MOS1nextInstance) {

            vt = CONSTKoverQ * here->MOS1temp;
            Check=1;
            if(ckt->CKTsenInfo){
#ifdef SENSDEBUG
                printf("MOS1load \n");
#endif /* SENSDEBUG */

                if((ckt->CKTsenInfo->SENstatus == PERTURBATION)&&
                    (here->MOS1senPertFlag == OFF))continue;

            }
            SenCond = ckt->CKTsenInfo && here->MOS1senPertFlag;

/*

*/

#ifdef DETAILPROF
asm("   .globl mospta");
asm("mospta:");
#endif /*DETAILPROF*/

            /* first, we compute a few useful values - these could be
             * pre-computed, but for historical reasons are still done
             * here.  They may be moved at the expense of instance size
             */

            EffectiveLength=here->MOS1l - 2*model->MOS1latDiff;
            if( (here->MOS1tSatCurDens == 0) || 
                    (here->MOS1drainArea == 0) ||
                    (here->MOS1sourceArea == 0)) {
                DrainSatCur = here->MOS1tSatCur;
                SourceSatCur = here->MOS1tSatCur;
            } else {
                DrainSatCur = here->MOS1tSatCurDens * 
                        here->MOS1drainArea;
                SourceSatCur = here->MOS1tSatCurDens * 
                        here->MOS1sourceArea;
            }
            GateSourceOverlapCap = model->MOS1gateSourceOverlapCapFactor * 
                    here->MOS1w;
            GateDrainOverlapCap = model->MOS1gateDrainOverlapCapFactor * 
                    here->MOS1w;
            GateBulkOverlapCap = model->MOS1gateBulkOverlapCapFactor * 
                    EffectiveLength;
            Beta = here->MOS1tTransconductance * here->MOS1w/EffectiveLength;
            OxideCap = model->MOS1oxideCapFactor * EffectiveLength * 
                    here->MOS1w;
            /* 
             * ok - now to do the start-up operations
             *
             * we must get values for vbs, vds, and vgs from somewhere
             * so we either predict them or recover them from last iteration
             * These are the two most common cases - either a prediction
             * step or the general iteration step and they
             * share some code, so we put them first - others later on
             */

            if(SenCond){
#ifdef SENSDEBUG
                printf("MOS1senPertFlag = ON \n");
#endif /* SENSDEBUG */
                if((ckt->CKTsenInfo->SENmode == TRANSEN) &&
                (ckt->CKTmode & MODEINITTRAN)) {
                    vgs = *(ckt->CKTstate1 + here->MOS1vgs);
                    vds = *(ckt->CKTstate1 + here->MOS1vds);
                    vbs = *(ckt->CKTstate1 + here->MOS1vbs);
                    vbd = *(ckt->CKTstate1 + here->MOS1vbd);
                    vgb = vgs - vbs;
                    vgd = vgs - vds;
                }
                else if (ckt->CKTsenInfo->SENmode == ACSEN){
                    vgb = model->MOS1type * ( 
                        *(ckt->CKTrhsOp+here->MOS1gNode) -
                        *(ckt->CKTrhsOp+here->MOS1bNode));
                    vbs = *(ckt->CKTstate0 + here->MOS1vbs);
                    vbd = *(ckt->CKTstate0 + here->MOS1vbd);
                    vgd = vgb + vbd ;
                    vgs = vgb + vbs ;
                    vds = vbs - vbd ;
                }
                else{
                    vgs = *(ckt->CKTstate0 + here->MOS1vgs);
                    vds = *(ckt->CKTstate0 + here->MOS1vds);
                    vbs = *(ckt->CKTstate0 + here->MOS1vbs);
                    vbd = *(ckt->CKTstate0 + here->MOS1vbd);
                    vgb = vgs - vbs;
                    vgd = vgs - vds;
                }
#ifdef SENSDEBUG
                printf(" vbs = %.7e ,vbd = %.7e,vgb = %.7e\n",vbs,vbd,vgb);
                printf(" vgs = %.7e ,vds = %.7e,vgd = %.7e\n",vgs,vds,vgd);
#endif /* SENSDEBUG */
                goto next1;
            }


            if((ckt->CKTmode & (MODEINITFLOAT | MODEINITPRED | MODEINITSMSIG
                    | MODEINITTRAN)) ||
                    ( (ckt->CKTmode & MODEINITFIX) && (!here->MOS1off) )  ) {
#ifndef PREDICTOR
                if(ckt->CKTmode & (MODEINITPRED | MODEINITTRAN) ) {

                    /* predictor step */

                    xfact=ckt->CKTdelta/ckt->CKTdeltaOld[1];
                    *(ckt->CKTstate0 + here->MOS1vbs) = 
                            *(ckt->CKTstate1 + here->MOS1vbs);
                    vbs = (1+xfact)* (*(ckt->CKTstate1 + here->MOS1vbs))
                            -(xfact * (*(ckt->CKTstate2 + here->MOS1vbs)));
                    *(ckt->CKTstate0 + here->MOS1vgs) = 
                            *(ckt->CKTstate1 + here->MOS1vgs);
                    vgs = (1+xfact)* (*(ckt->CKTstate1 + here->MOS1vgs))
                            -(xfact * (*(ckt->CKTstate2 + here->MOS1vgs)));
                    *(ckt->CKTstate0 + here->MOS1vds) = 
                            *(ckt->CKTstate1 + here->MOS1vds);
                    vds = (1+xfact)* (*(ckt->CKTstate1 + here->MOS1vds))
                            -(xfact * (*(ckt->CKTstate2 + here->MOS1vds)));
                    *(ckt->CKTstate0 + here->MOS1vbd) = 
                            *(ckt->CKTstate0 + here->MOS1vbs)-
                            *(ckt->CKTstate0 + here->MOS1vds);
                } else {
#endif /* PREDICTOR */

                    /* general iteration */

                    vbs = model->MOS1type * ( 
                        *(ckt->CKTrhsOld+here->MOS1bNode) -
                        *(ckt->CKTrhsOld+here->MOS1sNodePrime));
                    vgs = model->MOS1type * ( 
                        *(ckt->CKTrhsOld+here->MOS1gNode) -
                        *(ckt->CKTrhsOld+here->MOS1sNodePrime));
                    vds = model->MOS1type * ( 
                        *(ckt->CKTrhsOld+here->MOS1dNodePrime) -
                        *(ckt->CKTrhsOld+here->MOS1sNodePrime));
#ifndef PREDICTOR
                }
#endif /* PREDICTOR */

                /* now some common crunching for some more useful quantities */

                vbd=vbs-vds;
                vgd=vgs-vds;
                vgdo = *(ckt->CKTstate0 + here->MOS1vgs) - 
                        *(ckt->CKTstate0 + here->MOS1vds);
                delvbs = vbs - *(ckt->CKTstate0 + here->MOS1vbs);
                delvbd = vbd - *(ckt->CKTstate0 + here->MOS1vbd);
                delvgs = vgs - *(ckt->CKTstate0 + here->MOS1vgs);
                delvds = vds - *(ckt->CKTstate0 + here->MOS1vds);
                delvgd = vgd-vgdo;

                /* these are needed for convergence testing */

                if (here->MOS1mode >= 0) {
                    cdhat=
                        here->MOS1cd-
                        here->MOS1gbd * delvbd +
                        here->MOS1gmbs * delvbs +
                        here->MOS1gm * delvgs + 
                        here->MOS1gds * delvds ;
                } else {
                    cdhat=
                        here->MOS1cd -
                        ( here->MOS1gbd -
                        here->MOS1gmbs) * delvbd -
                        here->MOS1gm * delvgd + 
                        here->MOS1gds * delvds ;
                }
                cbhat=
                    here->MOS1cbs +
                    here->MOS1cbd +
                    here->MOS1gbd * delvbd +
                    here->MOS1gbs * delvbs ;
/*

*/

#ifdef DETAILPROF
asm("   .globl mosptb");
asm("mosptb:");
#endif /*DETAILPROF*/
#ifndef NOBYPASS
                /* now lets see if we can bypass (ugh) */
                /* the following mess should be one if statement, but
                 * many compilers can't handle it all at once, so it
                 * is split into several successive if statements
                 */
                tempv = MAX(FABS(cbhat),FABS(here->MOS1cbs
                        + here->MOS1cbd))+ckt->CKTabstol;
                if((!(ckt->CKTmode & (MODEINITPRED|MODEINITTRAN|MODEINITSMSIG)
                        )) && (ckt->CKTbypass) )
                if ( (FABS(cbhat-(here->MOS1cbs + 
                        here->MOS1cbd)) < ckt->CKTreltol * 
                        tempv)) 
                if( (FABS(delvbs) < (ckt->CKTreltol * MAX(FABS(vbs),
                        FABS(*(ckt->CKTstate0+here->MOS1vbs)))+
                        ckt->CKTvoltTol)))
                if ( (FABS(delvbd) < (ckt->CKTreltol * MAX(FABS(vbd),
                        FABS(*(ckt->CKTstate0+here->MOS1vbd)))+
                        ckt->CKTvoltTol)) )
                if( (FABS(delvgs) < (ckt->CKTreltol * MAX(FABS(vgs),
                        FABS(*(ckt->CKTstate0+here->MOS1vgs)))+
                        ckt->CKTvoltTol)))
                if ( (FABS(delvds) < (ckt->CKTreltol * MAX(FABS(vds),
                        FABS(*(ckt->CKTstate0+here->MOS1vds)))+
                        ckt->CKTvoltTol)) )
                if( (FABS(cdhat- here->MOS1cd) <
                        ckt->CKTreltol * MAX(FABS(cdhat),FABS(
                        here->MOS1cd)) + ckt->CKTabstol) ) {
                    /* bypass code *
                    /* nothing interesting has changed since last
                     * iteration on this device, so we just
                     * copy all the values computed last iteration out
                     * and keep going
                     */
                    vbs = *(ckt->CKTstate0 + here->MOS1vbs);
                    vbd = *(ckt->CKTstate0 + here->MOS1vbd);
                    vgs = *(ckt->CKTstate0 + here->MOS1vgs);
                    vds = *(ckt->CKTstate0 + here->MOS1vds);
                    vgd = vgs - vds;
                    vgb = vgs - vbs;
                    cdrain = here->MOS1mode * (here->MOS1cd + here->MOS1cbd);
                    if(ckt->CKTmode & (MODETRAN | MODETRANOP)) {
                        capgs = ( *(ckt->CKTstate0+here->MOS1capgs)+ 
                                  *(ckt->CKTstate1+here->MOS1capgs) +
                                  GateSourceOverlapCap );
                        capgd = ( *(ckt->CKTstate0+here->MOS1capgd)+ 
                                  *(ckt->CKTstate1+here->MOS1capgd) +
                                  GateDrainOverlapCap );
                        capgb = ( *(ckt->CKTstate0+here->MOS1capgb)+ 
                                  *(ckt->CKTstate1+here->MOS1capgb) +
                                  GateBulkOverlapCap );

                        if(ckt->CKTsenInfo){
                            here->MOS1cgs = capgs;
                            here->MOS1cgd = capgd;
                            here->MOS1cgb = capgb;
                        }
                    }
                    goto bypass;
                }
#endif /*NOBYPASS*/
/*

*/

#ifdef DETAILPROF
asm("   .globl mosptc");
asm("mosptc:");
#endif /*DETAILPROF*/
                /* ok - bypass is out, do it the hard way */

                von = model->MOS1type * here->MOS1von;

#ifndef NODELIMITING
                /* 
                 * limiting
                 *  we want to keep device voltages from changing
                 * so fast that the exponentials churn out overflows
                 * and similar rudeness
                 */

                if(*(ckt->CKTstate0 + here->MOS1vds) >=0) {
                    vgs = DEVfetlim(vgs,*(ckt->CKTstate0 + here->MOS1vgs)
                            ,von);
                    vds = vgs - vgd;
                    vds = DEVlimvds(vds,*(ckt->CKTstate0 + here->MOS1vds));
                    vgd = vgs - vds;
                } else {
                    vgd = DEVfetlim(vgd,vgdo,von);
                    vds = vgs - vgd;
                    if(!(ckt->CKTfixLimit)) {
                        vds = -DEVlimvds(-vds,-(*(ckt->CKTstate0 + 
                                here->MOS1vds)));
                    }
                    vgs = vgd + vds;
                }
                if(vds >= 0) {
                    vbs = DEVpnjlim(vbs,*(ckt->CKTstate0 + here->MOS1vbs),
                            vt,here->MOS1sourceVcrit,&Check);
                    vbd = vbs-vds;
                } else {
                    vbd = DEVpnjlim(vbd,*(ckt->CKTstate0 + here->MOS1vbd),
                            vt,here->MOS1drainVcrit,&Check);
                    vbs = vbd + vds;
                }
#endif /*NODELIMITING*/
/*

*/

#ifdef DETAILPROF
asm("   .globl mosptd");
asm("mosptd:");
#endif /*DETAILPROF*/
            } else {

                /* ok - not one of the simple cases, so we have to
                 * look at all of the possibilities for why we were
                 * called.  We still just initialize the three voltages
                 */

                if((ckt->CKTmode & MODEINITJCT) && !here->MOS1off) {
                    vds= model->MOS1type * here->MOS1icVDS;
                    vgs= model->MOS1type * here->MOS1icVGS;
                    vbs= model->MOS1type * here->MOS1icVBS;
                    if((vds==0) && (vgs==0) && (vbs==0) && 
                            ((ckt->CKTmode & 
                                (MODETRAN|MODEDCOP|MODEDCTRANCURVE)) ||
                             (!(ckt->CKTmode & MODEUIC)))) {
                        vbs = -1;
                        vgs = model->MOS1type * here->MOS1tVto;
                        vds = 0;
                    }
                } else {
                    vbs=vgs=vds=0;
                } 
            }
/*

*/

#ifdef DETAILPROF
asm("   .globl mospte");
asm("mospte:");
#endif /*DETAILPROF*/

            /*
             * now all the preliminaries are over - we can start doing the
             * real work
             */
            vbd = vbs - vds;
            vgd = vgs - vds;
            vgb = vgs - vbs;


            /*
             * bulk-source and bulk-drain diodes
             *   here we just evaluate the ideal diode current and the
             *   corresponding derivative (conductance).
             */
next1:      if(vbs <= 0) {
                here->MOS1gbs = SourceSatCur/vt;
                here->MOS1cbs = here->MOS1gbs*vbs;
                here->MOS1gbs += ckt->CKTgmin;
            } else {
                evbs = exp(MIN(MAX_EXP_ARG,vbs/vt));
                here->MOS1gbs = SourceSatCur*evbs/vt + ckt->CKTgmin;
                here->MOS1cbs = SourceSatCur * (evbs-1);
            }
            if(vbd <= 0) {
                here->MOS1gbd = DrainSatCur/vt;
                here->MOS1cbd = here->MOS1gbd *vbd;
                here->MOS1gbd += ckt->CKTgmin;
            } else {
                evbd = exp(MIN(MAX_EXP_ARG,vbd/vt));
                here->MOS1gbd = DrainSatCur*evbd/vt +ckt->CKTgmin;
                here->MOS1cbd = DrainSatCur *(evbd-1);
            }

            /* now to determine whether the user was able to correctly
             * identify the source and drain of his device
             */
            if(vds >= 0) {
                /* normal mode */
                here->MOS1mode = 1;
            } else {
                /* inverse mode */
                here->MOS1mode = -1;
            }
/*

*/

#ifdef DETAILPROF
asm("   .globl mosptf");
asm("mosptf:");
#endif /*DETAILPROF*/
            {
            /*
             *     this block of code evaluates the drain current and its 
             *     derivatives using the shichman-hodges model and the 
             *     charges associated with the gate, channel and bulk for 
             *     mosfets
             *
             */

            /* the following 4 variables are local to this code block until 
             * it is obvious that they can be made global 
             */
            double arg;
            double betap;
            double sarg;
            double vgst;

                if ((here->MOS1mode==1?vbs:vbd) <= 0 ) {
                    sarg=sqrt(here->MOS1tPhi-(here->MOS1mode==1?vbs:vbd));
                } else {
                    sarg=sqrt(here->MOS1tPhi);
                    sarg=sarg-(here->MOS1mode==1?vbs:vbd)/(sarg+sarg);
                    sarg=MAX(0,sarg);
                }
                von=(here->MOS1tVbi*model->MOS1type)+model->MOS1gamma*sarg;
                vgst=(here->MOS1mode==1?vgs:vgd)-von;
                vdsat=MAX(vgst,0);
                if (sarg <= 0) {
                    arg=0;
                } else {
                    arg=model->MOS1gamma/(sarg+sarg);
                }
                if (vgst <= 0) {
                    /*
                     *     cutoff region
                     */
                    cdrain=0;
                    here->MOS1gm=0;
                    here->MOS1gds=0;
                    here->MOS1gmbs=0;
                } else{
                    /*
                     *     saturation region
                     */
                    betap=Beta*(1+model->MOS1lambda*(vds*here->MOS1mode));
                    if (vgst <= (vds*here->MOS1mode)){
                        cdrain=betap*vgst*vgst*.5;
                        here->MOS1gm=betap*vgst;
                        here->MOS1gds=model->MOS1lambda*Beta*vgst*vgst*.5;
                        here->MOS1gmbs=here->MOS1gm*arg;
                    } else {
                    /*
                     *     linear region
                     */
                        cdrain=betap*(vds*here->MOS1mode)*
                            (vgst-.5*(vds*here->MOS1mode));
                        here->MOS1gm=betap*(vds*here->MOS1mode);
                        here->MOS1gds=betap*(vgst-(vds*here->MOS1mode))+
                                model->MOS1lambda*Beta*
                                (vds*here->MOS1mode)*
                                (vgst-.5*(vds*here->MOS1mode));
                        here->MOS1gmbs=here->MOS1gm*arg;
                    }
                }
                /*
                 *     finished
                 */
            }
/*

*/

#ifdef DETAILPROF
asm("   .globl mosptg");
asm("mosptg:");
#endif /*DETAILPROF*/

            /* now deal with n vs p polarity */

            here->MOS1von = model->MOS1type * von;
            here->MOS1vdsat = model->MOS1type * vdsat;
            /* line 490 */
            /*
             *  COMPUTE EQUIVALENT DRAIN CURRENT SOURCE
             */
            here->MOS1cd=here->MOS1mode * cdrain - here->MOS1cbd;

            if (ckt->CKTmode & (MODETRAN | MODETRANOP | MODEINITSMSIG)) {
                /* 
                 * now we do the hard part of the bulk-drain and bulk-source
                 * diode - we evaluate the non-linear capacitance and
                 * charge
                 *
                 * the basic equations are not hard, but the implementation
                 * is somewhat long in an attempt to avoid log/exponential
                 * evaluations
                 */
                /*
                 *  charge storage elements
                 *
                 *.. bulk-drain and bulk-source depletion capacitances
                 */
#ifdef CAPBYPASS
                if(((ckt->CKTmode & (MODEINITPRED | MODEINITTRAN) ) ||
                        FABS(delvbs) >= ckt->CKTreltol * MAX(FABS(vbs),
                        FABS(*(ckt->CKTstate0+here->MOS1vbs)))+
                        ckt->CKTvoltTol)|| senflag)
#endif /*CAPBYPASS*/
                {
                    /* can't bypass the diode capacitance calculations */
#ifdef CAPZEROBYPASS
                    if(here->MOS1Cbs != 0 || here->MOS1Cbssw != 0 ) {
#endif /*CAPZEROBYPASS*/
                    if (vbs < here->MOS1tDepCap){
                        arg=1-vbs/here->MOS1tBulkPot;
                        /*
                         * the following block looks somewhat long and messy,
                         * but since most users use the default grading
                         * coefficients of .5, and sqrt is MUCH faster than an
                         * exp(log()) we use this special case code to buy time.
                         * (as much as 10% of total job time!)
                         */
#ifndef NOSQRT
                        if(model->MOS1bulkJctBotGradingCoeff ==
                                model->MOS1bulkJctSideGradingCoeff) {
                            if(model->MOS1bulkJctBotGradingCoeff == .5) {
                                sarg = sargsw = 1/sqrt(arg);
                            } else {
                                sarg = sargsw =
                                        exp(-model->MOS1bulkJctBotGradingCoeff*
                                        log(arg));
                            }
                        } else {
                            if(model->MOS1bulkJctBotGradingCoeff == .5) {
                                sarg = 1/sqrt(arg);
                            } else {
#endif /*NOSQRT*/
                                sarg = exp(-model->MOS1bulkJctBotGradingCoeff*
                                        log(arg));
#ifndef NOSQRT
                            }
                            if(model->MOS1bulkJctSideGradingCoeff == .5) {
                                sargsw = 1/sqrt(arg);
                            } else {
#endif /*NOSQRT*/
                                sargsw =exp(-model->MOS1bulkJctSideGradingCoeff*
                                        log(arg));
#ifndef NOSQRT
                            }
                        }
#endif /*NOSQRT*/
                        *(ckt->CKTstate0 + here->MOS1qbs) =
                            here->MOS1tBulkPot*(here->MOS1Cbs*
                            (1-arg*sarg)/(1-model->MOS1bulkJctBotGradingCoeff)
                            +here->MOS1Cbssw*
                            (1-arg*sargsw)/
                            (1-model->MOS1bulkJctSideGradingCoeff));
                        here->MOS1capbs=here->MOS1Cbs*sarg+
                                here->MOS1Cbssw*sargsw;
                    } else {
                        *(ckt->CKTstate0 + here->MOS1qbs) = here->MOS1f4s +
                                vbs*(here->MOS1f2s+vbs*(here->MOS1f3s/2));
                        here->MOS1capbs=here->MOS1f2s+here->MOS1f3s*vbs;
                    }
#ifdef CAPZEROBYPASS
                    } else {
                        *(ckt->CKTstate0 + here->MOS1qbs) = 0;
                        here->MOS1capbs=0;
                    }
#endif /*CAPZEROBYPASS*/
                }
#ifdef CAPBYPASS
                if(((ckt->CKTmode & (MODEINITPRED | MODEINITTRAN) ) ||
                        FABS(delvbd) >= ckt->CKTreltol * MAX(FABS(vbd),
                        FABS(*(ckt->CKTstate0+here->MOS1vbd)))+
                        ckt->CKTvoltTol)|| senflag)
#endif /*CAPBYPASS*/
                    /* can't bypass the diode capacitance calculations */
                {
#ifdef CAPZEROBYPASS
                    if(here->MOS1Cbd != 0 || here->MOS1Cbdsw != 0 ) {
#endif /*CAPZEROBYPASS*/
                    if (vbd < here->MOS1tDepCap) {
                        arg=1-vbd/here->MOS1tBulkPot;
                        /*
                         * the following block looks somewhat long and messy,
                         * but since most users use the default grading
                         * coefficients of .5, and sqrt is MUCH faster than an
                         * exp(log()) we use this special case code to buy time.
                         * (as much as 10% of total job time!)
                         */
#ifndef NOSQRT
                        if(model->MOS1bulkJctBotGradingCoeff == .5 &&
                                model->MOS1bulkJctSideGradingCoeff == .5) {
                            sarg = sargsw = 1/sqrt(arg);
                        } else {
                            if(model->MOS1bulkJctBotGradingCoeff == .5) {
                                sarg = 1/sqrt(arg);
                            } else {
#endif /*NOSQRT*/
                                sarg = exp(-model->MOS1bulkJctBotGradingCoeff*
                                        log(arg));
#ifndef NOSQRT
                            }
                            if(model->MOS1bulkJctSideGradingCoeff == .5) {
                                sargsw = 1/sqrt(arg);
                            } else {
#endif /*NOSQRT*/
                                sargsw =exp(-model->MOS1bulkJctSideGradingCoeff*
                                        log(arg));
#ifndef NOSQRT
                            }
                        }
#endif /*NOSQRT*/
                        *(ckt->CKTstate0 + here->MOS1qbd) =
                            here->MOS1tBulkPot*(here->MOS1Cbd*
                            (1-arg*sarg)
                            /(1-model->MOS1bulkJctBotGradingCoeff)
                            +here->MOS1Cbdsw*
                            (1-arg*sargsw)
                            /(1-model->MOS1bulkJctSideGradingCoeff));
                        here->MOS1capbd=here->MOS1Cbd*sarg+
                                here->MOS1Cbdsw*sargsw;
                    } else {
                        *(ckt->CKTstate0 + here->MOS1qbd) = here->MOS1f4d +
                                vbd * (here->MOS1f2d + vbd * here->MOS1f3d/2);
                        here->MOS1capbd=here->MOS1f2d + vbd * here->MOS1f3d;
                    }
#ifdef CAPZEROBYPASS
                } else {
                    *(ckt->CKTstate0 + here->MOS1qbd) = 0;
                    here->MOS1capbd = 0;
                }
#endif /*CAPZEROBYPASS*/
                }
/*

*/

#ifdef DETAILPROF
asm("   .globl mospth");
asm("mospth:");
#endif /*DETAILPROF*/

                if(SenCond && (ckt->CKTsenInfo->SENmode==TRANSEN)) goto next2;

                if ( (ckt->CKTmode & MODETRAN) || ( (ckt->CKTmode&MODEINITTRAN)
						&& !(ckt->CKTmode&MODEUIC)) ) {
                    /* (above only excludes tranop, since we're only at this
                     * point if tran or tranop )
                     */

                    /*
                     *    calculate equivalent conductances and currents for
                     *    depletion capacitors
                     */

                    /* integrate the capacitors and save results */

                    error = NIintegrate(ckt,&geq,&ceq,here->MOS1capbd,
                            here->MOS1qbd);
                    if(error) return(error);
                    here->MOS1gbd += geq;
                    here->MOS1cbd += *(ckt->CKTstate0 + here->MOS1cqbd);
                    here->MOS1cd -= *(ckt->CKTstate0 + here->MOS1cqbd);
                    error = NIintegrate(ckt,&geq,&ceq,here->MOS1capbs,
                            here->MOS1qbs);
                    if(error) return(error);
                    here->MOS1gbs += geq;
                    here->MOS1cbs += *(ckt->CKTstate0 + here->MOS1cqbs);
                }
            }
/*

*/

#ifdef DETAILPROF
asm("   .globl mospti");
asm("mospti:");
#endif /*DETAILPROF*/

            if(SenCond) goto next2;


            /*
             *  check convergence
             */
            if ( (here->MOS1off == 0)  || 
                    (!(ckt->CKTmode & (MODEINITFIX|MODEINITSMSIG))) ){
                if (Check == 1) {
                    ckt->CKTnoncon++;
		    ckt->CKTtroubleElt = (GENinstance *) here;
#ifndef NEWCONV
                } else {
                    tol=ckt->CKTreltol*MAX(FABS(cdhat),
                            FABS(here->MOS1cd))+ckt->CKTabstol;
                    if (FABS(cdhat-here->MOS1cd) >= tol) { 
                        ckt->CKTnoncon++;
			ckt->CKTtroubleElt = (GENinstance *) here;
                    } else {
                        tol=ckt->CKTreltol*MAX(FABS(cbhat),
                                FABS(here->MOS1cbs+here->MOS1cbd))+
                                ckt->CKTabstol;
                        if (FABS(cbhat-(here->MOS1cbs+here->MOS1cbd)) > tol) {
                            ckt->CKTnoncon++;
			    ckt->CKTtroubleElt = (GENinstance *) here;
                        }
                    }
#endif /*NEWCONV*/
                }
            }
/*

*/

#ifdef DETAILPROF
asm("   .globl mosptj");
asm("mosptj:");
#endif /*DETAILPROF*/

            /* save things away for next time */

next2:      *(ckt->CKTstate0 + here->MOS1vbs) = vbs;
            *(ckt->CKTstate0 + here->MOS1vbd) = vbd;
            *(ckt->CKTstate0 + here->MOS1vgs) = vgs;
            *(ckt->CKTstate0 + here->MOS1vds) = vds;

/*

*/

#ifdef DETAILPROF
asm("   .globl mosptk");
asm("mosptk:");
#endif /*DETAILPROF*/
            /*
             *     meyer's capacitor model
             */
            if ( ckt->CKTmode & (MODETRAN | MODETRANOP | MODEINITSMSIG) ) {
                /*
                 *     calculate meyer's capacitors
                 */
                /* 
                 * new cmeyer - this just evaluates at the current time,
                 * expects you to remember values from previous time
                 * returns 1/2 of non-constant portion of capacitance
                 * you must add in the other half from previous time
                 * and the constant part
                 */
                if (here->MOS1mode > 0){
                    DEVqmeyer (vgs,vgd,vgb,von,vdsat,
                        (ckt->CKTstate0 + here->MOS1capgs),
                        (ckt->CKTstate0 + here->MOS1capgd),
                        (ckt->CKTstate0 + here->MOS1capgb),
                        here->MOS1tPhi,OxideCap);
                } else {
                    DEVqmeyer (vgd,vgs,vgb,von,vdsat,
                        (ckt->CKTstate0 + here->MOS1capgd),
                        (ckt->CKTstate0 + here->MOS1capgs),
                        (ckt->CKTstate0 + here->MOS1capgb),
                        here->MOS1tPhi,OxideCap);
                }
                vgs1 = *(ckt->CKTstate1 + here->MOS1vgs);
                vgd1 = vgs1 - *(ckt->CKTstate1 + here->MOS1vds);
                vgb1 = vgs1 - *(ckt->CKTstate1 + here->MOS1vbs);
                if(ckt->CKTmode & (MODETRANOP|MODEINITSMSIG)) {
                    capgs =  2 * *(ckt->CKTstate0+here->MOS1capgs)+ 
                              GateSourceOverlapCap ;
                    capgd =  2 * *(ckt->CKTstate0+here->MOS1capgd)+ 
                              GateDrainOverlapCap ;
                    capgb =  2 * *(ckt->CKTstate0+here->MOS1capgb)+ 
                              GateBulkOverlapCap ;
                } else {
                    capgs = ( *(ckt->CKTstate0+here->MOS1capgs)+ 
                              *(ckt->CKTstate1+here->MOS1capgs) +
                              GateSourceOverlapCap );
                    capgd = ( *(ckt->CKTstate0+here->MOS1capgd)+ 
                              *(ckt->CKTstate1+here->MOS1capgd) +
                              GateDrainOverlapCap );
                    capgb = ( *(ckt->CKTstate0+here->MOS1capgb)+ 
                              *(ckt->CKTstate1+here->MOS1capgb) +
                              GateBulkOverlapCap );
                }
                if(ckt->CKTsenInfo){
                    here->MOS1cgs = capgs;
                    here->MOS1cgd = capgd;
                    here->MOS1cgb = capgb;
                }
/*

*/

#ifdef DETAILPROF
asm("   .globl mosptl");
asm("mosptl:");
#endif /*DETAILPROF*/
                /*
                 *     store small-signal parameters (for meyer's model)
                 *  all parameters already stored, so done...
                 */
                if(SenCond){
                    if((ckt->CKTsenInfo->SENmode == DCSEN)||
                            (ckt->CKTsenInfo->SENmode == ACSEN)){
                        continue;
                    }
                }

#ifndef PREDICTOR
                if (ckt->CKTmode & (MODEINITPRED | MODEINITTRAN) ) {
                    *(ckt->CKTstate0 + here->MOS1qgs) =
                        (1+xfact) * *(ckt->CKTstate1 + here->MOS1qgs)
                        - xfact * *(ckt->CKTstate2 + here->MOS1qgs);
                    *(ckt->CKTstate0 + here->MOS1qgd) =
                        (1+xfact) * *(ckt->CKTstate1 + here->MOS1qgd)
                        - xfact * *(ckt->CKTstate2 + here->MOS1qgd);
                    *(ckt->CKTstate0 + here->MOS1qgb) =
                        (1+xfact) * *(ckt->CKTstate1 + here->MOS1qgb)
                        - xfact * *(ckt->CKTstate2 + here->MOS1qgb);
                } else {
#endif /*PREDICTOR*/
                    if(ckt->CKTmode & MODETRAN) {
                        *(ckt->CKTstate0 + here->MOS1qgs) = (vgs-vgs1)*capgs +
                            *(ckt->CKTstate1 + here->MOS1qgs) ;
                        *(ckt->CKTstate0 + here->MOS1qgd) = (vgd-vgd1)*capgd +
                            *(ckt->CKTstate1 + here->MOS1qgd) ;
                        *(ckt->CKTstate0 + here->MOS1qgb) = (vgb-vgb1)*capgb +
                            *(ckt->CKTstate1 + here->MOS1qgb) ;
                    } else {
                        /* TRANOP only */
                        *(ckt->CKTstate0 + here->MOS1qgs) = vgs*capgs;
                        *(ckt->CKTstate0 + here->MOS1qgd) = vgd*capgd;
                        *(ckt->CKTstate0 + here->MOS1qgb) = vgb*capgb;
                    }
#ifndef PREDICTOR
                }
#endif /*PREDICTOR*/
            }
bypass:
            if(SenCond) continue;

            if ( (ckt->CKTmode & (MODEINITTRAN)) || 
                    (! (ckt->CKTmode & (MODETRAN)) )  ) {
                /*
                 *  initialize to zero charge conductances 
                 *  and current
                 */
                gcgs=0;
                ceqgs=0;
                gcgd=0;
                ceqgd=0;
                gcgb=0;
                ceqgb=0;
            } else {
                if(capgs == 0) *(ckt->CKTstate0 + here->MOS1cqgs) =0;
                if(capgd == 0) *(ckt->CKTstate0 + here->MOS1cqgd) =0;
                if(capgb == 0) *(ckt->CKTstate0 + here->MOS1cqgb) =0;
                /*
                 *    calculate equivalent conductances and currents for
                 *    meyer"s capacitors
                 */
                error = NIintegrate(ckt,&gcgs,&ceqgs,capgs,here->MOS1qgs);
                if(error) return(error);
                error = NIintegrate(ckt,&gcgd,&ceqgd,capgd,here->MOS1qgd);
                if(error) return(error);
                error = NIintegrate(ckt,&gcgb,&ceqgb,capgb,here->MOS1qgb);
                if(error) return(error);
                ceqgs=ceqgs-gcgs*vgs+ckt->CKTag[0]* 
                        *(ckt->CKTstate0 + here->MOS1qgs);
                ceqgd=ceqgd-gcgd*vgd+ckt->CKTag[0]*
                        *(ckt->CKTstate0 + here->MOS1qgd);
                ceqgb=ceqgb-gcgb*vgb+ckt->CKTag[0]*
                        *(ckt->CKTstate0 + here->MOS1qgb);
            }
            /*
             *     store charge storage info for meyer's cap in lx table
             */

            /*
             *  load current vector
             */
            ceqbs = model->MOS1type * 
                    (here->MOS1cbs-(here->MOS1gbs-ckt->CKTgmin)*vbs);
            ceqbd = model->MOS1type * 
                    (here->MOS1cbd-(here->MOS1gbd-ckt->CKTgmin)*vbd);
            if (here->MOS1mode >= 0) {
                xnrm=1;
                xrev=0;
                cdreq=model->MOS1type*(cdrain-here->MOS1gds*vds-
                        here->MOS1gm*vgs-here->MOS1gmbs*vbs);
            } else {
                xnrm=0;
                xrev=1;
                cdreq = -(model->MOS1type)*(cdrain-here->MOS1gds*(-vds)-
                        here->MOS1gm*vgd-here->MOS1gmbs*vbd);
            }
            *(ckt->CKTrhs + here->MOS1gNode) -= 
                (model->MOS1type * (ceqgs + ceqgb + ceqgd));
                
            //sigma_I_add( here->MOS1gNode, -(model->MOS1type * (ceqgs + ceqgb + ceqgd)));
            
            *(ckt->CKTrhs + here->MOS1bNode) -=
                (ceqbs + ceqbd - model->MOS1type * ceqgb);
                
            //sigma_I_add( here->MOS1bNode, -(ceqbs + ceqbd - model->MOS1type * ceqgb));
                
            *(ckt->CKTrhs + here->MOS1dNodePrime) +=
                    (ceqbd - cdreq + model->MOS1type * ceqgd);
                    
            //sigma_I_add( here->MOS1dNode, (ceqbd - cdreq + model->MOS1type * ceqgd));
                    
            *(ckt->CKTrhs + here->MOS1sNodePrime) += 
                    cdreq + ceqbs + model->MOS1type * ceqgs;
                    
            //sigma_I_add( here->MOS1sNode, (cdreq + ceqbs + model->MOS1type * ceqgs));

                    
            //printf("Row 2: Isum incremented by %f\n",(model->MOS1type * (ceqgs + ceqgb + ceqgd)));
            
            //printf("Row 4: Isum incremented by %f\n",(ceqbs + ceqbd - model->MOS1type * ceqgb));
            
            //printf("Row 1: Isum incremented by %f\n",(ceqbd - cdreq + model->MOS1type * ceqgd));
            
            //printf("Row 3: Isum incremented by %f\n",(cdreq + ceqbs + model->MOS1type * ceqgs));

            //printf("\n");
            /*
             *  load y matrix
             */


            *(here->MOS1DdPtr) += (here->MOS1drainConductance);
            matfree_DD         -= (here->MOS1drainConductance);
            
            /*
                printf("Incrementing D pivot by %f\n",(here->MOS1drainConductance));
                printf("D pivot = %f\n",matfree_DD);
            */
            
            *(here->MOS1GgPtr) += ((gcgd+gcgs+gcgb));
            matfree_GG         -= ((gcgd+gcgs+gcgb));
            
            /*
                //printf("Incrementing G pivot by %f\n",((gcgd+gcgs+gcgb)));
                //printf("G pivot = %f\n",matfree_GG);
            */
            
            *(here->MOS1SsPtr) += (here->MOS1sourceConductance);
            matfree_SS         -= (here->MOS1sourceConductance);
            
            /*
                //printf("Incrementing S pivot by %f\n",(here->MOS1sourceConductance));
                //printf("S pivot = %f\n",matfree_SS);
            */
            
            *(here->MOS1BbPtr) += (here->MOS1gbd+here->MOS1gbs+gcgb);
            matfree_BB         -= (here->MOS1gbd+here->MOS1gbs+gcgb);
            
            /*
                //printf("Incrementing B pivot by %f\n",(here->MOS1gbd+here->MOS1gbs+gcgb));
                //printf("B pivot = %f\n",matfree_BB);
            */
            
            *(here->MOS1DPdpPtr) += 
                    (here->MOS1drainConductance+here->MOS1gds+
                    here->MOS1gbd+xrev*(here->MOS1gm+here->MOS1gmbs)+gcgd);
            matfree_DD           -= 
                    (here->MOS1drainConductance+here->MOS1gds+
                    here->MOS1gbd+xrev*(here->MOS1gm+here->MOS1gmbs)+gcgd);
            
            /*        
                //printf("Incrementing D pivot by %f\n",(here->MOS1drainConductance+here->MOS1gds+
                    here->MOS1gbd+xrev*(here->MOS1gm+here->MOS1gmbs)+gcgd));
                //printf("D pivot = %f\n",matfree_DD);
            */
                    
            *(here->MOS1SPspPtr) += 
                    (here->MOS1sourceConductance+here->MOS1gds+
                    here->MOS1gbs+xnrm*(here->MOS1gm+here->MOS1gmbs)+gcgs);
            matfree_SS           -= 
                    (here->MOS1sourceConductance+here->MOS1gds+
                    here->MOS1gbs+xnrm*(here->MOS1gm+here->MOS1gmbs)+gcgs);
            
            /*
                //printf("Incrementing S pivot by %f\n",(here->MOS1sourceConductance+here->MOS1gds+
                    here->MOS1gbs+xnrm*(here->MOS1gm+here->MOS1gmbs)+gcgs));
                //printf("S pivot = %f\n",matfree_SS);
            */
            
            *(here->MOS1DdpPtr) += (-here->MOS1drainConductance);
            matfree_DD          -= (-here->MOS1drainConductance);
            
            /*
                //printf("Incrementing D pivot by %f\n",(-here->MOS1drainConductance));
                //printf("D pivot = %f\n",matfree_DD);
            */
            
            *(here->MOS1GbPtr) -= gcgb;
            matfree_GB         += gcgb;
            
            /*
                //printf("Incrementing G off-pivot B by %f\n",(-gcgb));
                //printf("G off-pivot B = %f\n",matfree_GB);
            */
            
            *(here->MOS1GdpPtr) -= gcgd;
            matfree_GD          += gcgd;
            
            /*
                //printf("Incrementing G off-pivot D by %f\n",(gcgd));
                //printf("G off-pivot D = %f\n",matfree_GD);
            */
            
            *(here->MOS1GspPtr) -= gcgs;
            matfree_GS          += gcgs;
            
            /*
                //printf("Incrementing G off-pivot S by %f\n",(gcgs));
                //printf("G off-pivot S = %f\n",matfree_GS);
            */
            
            *(here->MOS1SspPtr) += (-here->MOS1sourceConductance);
            matfree_SS          -= (-here->MOS1sourceConductance);
            
            /*
                //printf("Incrementing S pivot by %f\n",(-here->MOS1sourceConductance));
                //printf("S pivot = %f\n",matfree_SS);
            */
            
            *(here->MOS1BgPtr) -= gcgb;
            matfree_BG         += gcgb;
            
            /*
                //printf("Incrementing B off-pivot G by %f\n",(gcgb));
                //printf("B off-pivot G = %f\n",matfree_BG);
            */
            
            *(here->MOS1BdpPtr) -= here->MOS1gbd;
            matfree_BD          += here->MOS1gbd;
            
            /*
                //printf("Incrementing B off-pivot D by %f\n",(here->MOS1gbd));
                //printf("B off-pivot D = %f\n",matfree_BD);
            */
            
            *(here->MOS1BspPtr) -= here->MOS1gbs;
            matfree_BS          += here->MOS1gbs;
            
            /*
                //printf("Incrementing B off-pivot S by %f\n",(here->MOS1gbs));
                //printf("B off-pivot S = %f\n",matfree_BS);
            */
            
            *(here->MOS1DPdPtr) += (-here->MOS1drainConductance);
            matfree_DD          -= (-here->MOS1drainConductance);
            
            /*
                //printf("Incrementing D pivot by %f\n",(-here->MOS1drainConductance));
                //printf("D pivot = %f\n",matfree_DD);
            */
            
            *(here->MOS1DPgPtr) += ((xnrm-xrev)*here->MOS1gm-gcgd);
            matfree_DG          -= ((xnrm-xrev)*here->MOS1gm-gcgd);
            
            /*
                //printf("Incrementing D off-pivot G by %f\n",((xnrm-xrev)*here->MOS1gm-gcgd));
                //printf("D off-pivot G = %f\n",matfree_DG);
            */
            
            *(here->MOS1DPbPtr) += (-here->MOS1gbd+(xnrm-xrev)*here->MOS1gmbs);
            matfree_DB          -= (-here->MOS1gbd+(xnrm-xrev)*here->MOS1gmbs);
            
            /*
                //printf("Incrementing D off-pivot B by %f\n",(-here->MOS1gbd+(xnrm-xrev)*here->MOS1gmbs));
                //printf("D off-pivot B = %f\n",matfree_DB);
            */
            
            *(here->MOS1DPspPtr) += (-here->MOS1gds-xnrm*
                    (here->MOS1gm+here->MOS1gmbs));
            matfree_DS           -= (-here->MOS1gds-xnrm*
                    (here->MOS1gm+here->MOS1gmbs));
            
            /*
                //printf("Incrementing D off-pivot S by %f\n",(-here->MOS1gds-xnrm*
                    (here->MOS1gm+here->MOS1gmbs)));
                //printf("D off-pivot S = %f\n",matfree_DS);
            */
            
            *(here->MOS1SPgPtr) += (-(xnrm-xrev)*here->MOS1gm-gcgs);
            matfree_SG          -= (-(xnrm-xrev)*here->MOS1gm-gcgs);
            
            /*
                //printf("Incrementing S off-pivot G by %f\n",(-(xnrm-xrev)*here->MOS1gm-gcgs));
                //printf("S off-pivot G = %f\n",matfree_SG);
            */
            
            *(here->MOS1SPsPtr) += (-here->MOS1sourceConductance);
            matfree_SS          -= (-here->MOS1sourceConductance);
            
            /*
                //printf("Incrementing S pivot by %f\n",(-here->MOS1sourceConductance));
                //printf("S pivot = %f\n",matfree_SS);
            */
            
            *(here->MOS1SPbPtr) += (-here->MOS1gbs-(xnrm-xrev)*here->MOS1gmbs);
            matfree_SB          -= (-here->MOS1gbs-(xnrm-xrev)*here->MOS1gmbs);
            
            /*
                //printf("Incrementing S off-pivot B by %f\n",(-here->MOS1gbs-(xnrm-xrev)*here->MOS1gmbs));
                //printf("S off-pivot = %f\n",matfree_SB);
            */
            
            *(here->MOS1SPdpPtr) += (-here->MOS1gds-xrev*
                    (here->MOS1gm+here->MOS1gmbs));
            matfree_SD           -= (-here->MOS1gds-xrev*
                    (here->MOS1gm+here->MOS1gmbs));
            
            /*
                //printf("Incrementing S off-pivot D by %f\n",(-here->MOS1gds-xrev*
                    (here->MOS1gm+here->MOS1gmbs)));
                //printf("S off-pivot D = %f\n",matfree_SD);
            */

//printf("D sum = %f\n", (matfree_DS + matfree_DB + matfree_DG - matfree_DD));
//printf("G sum = %f\n", (matfree_GD + matfree_GB + matfree_GS - matfree_GG));
//printf("S sum = %f\n", (matfree_SD + matfree_SB + matfree_SG - matfree_SS));
//printf("B sum = %f\n", (matfree_BD + matfree_BG + matfree_BS - matfree_BB));

//matfree_pause();

/*
if((matfree_SD + matfree_SB + matfree_SG - matfree_SS) > 0.000002){
  printf("S sum nonzero, aborting...\n");
  //abort();
}

if((matfree_DS + matfree_DB + matfree_DG - matfree_DD) > 0.000002){
  printf("D sum nonzero, aborting...\n");
  //abort();
}

if((matfree_GD + matfree_GB + matfree_GS - matfree_GG) > 0.000002){
  printf("G sum nonzero, aborting...\n");
  //abort();
}

if((matfree_BD + matfree_BG + matfree_BS - matfree_BB) > 0.000002){
  printf("B sum nonzero, aborting...\n");
  //abort();
}
*/

//begin MatFree stuff - added by Coryan
/*
            bool ground_adjacent = false;
            if(here->MOS1dNode == 0 || here->MOS1gNode == 0 || here->MOS1bNode == 0 || here->MOS1sNode == 0){
              ground_adjacent = true;
            }

            //set node info
            update_node_properties( here->MOS1dNode,
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false,
                                    
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false );

            update_node_properties( here->MOS1gNode,
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false,
                                    
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false );

            update_node_properties( here->MOS1bNode,
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false,
                                    
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false );
                                    
            update_node_properties( here->MOS1sNode,
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false,
                                    
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false );

            sigma_G_add(here->MOS1dNode, here->MOS1gNode, matfree_DG);
            sigma_G_add(here->MOS1dNode, here->MOS1sNode, matfree_DS);
            sigma_G_add(here->MOS1dNode, here->MOS1bNode, matfree_DB);
            
            sigma_G_add(here->MOS1gNode, here->MOS1dNode, matfree_GD);
            sigma_G_add(here->MOS1gNode, here->MOS1sNode, matfree_GS);
            sigma_G_add(here->MOS1gNode, here->MOS1bNode, matfree_GB);
            
            sigma_G_add(here->MOS1sNode, here->MOS1dNode, matfree_SD);
            sigma_G_add(here->MOS1sNode, here->MOS1gNode, matfree_SG);
            sigma_G_add(here->MOS1sNode, here->MOS1bNode, matfree_SB);
            
            sigma_G_add(here->MOS1bNode, here->MOS1dNode, matfree_BD);
            sigma_G_add(here->MOS1bNode, here->MOS1gNode, matfree_BG);
            sigma_G_add(here->MOS1bNode, here->MOS1sNode, matfree_BS);
*/

/*
            *(ckt->CKTrhs + here->MOS1gNode) = 0;
            *(ckt->CKTrhs + here->MOS1bNode) = 0;
            *(ckt->CKTrhs + here->MOS1dNodePrime) = 0;
            *(ckt->CKTrhs + here->MOS1sNodePrime) = 0;

            *(here->MOS1DdPtr) = 0;        //+drainconductance
            *(here->MOS1GgPtr) = 0;
            *(here->MOS1SsPtr) = 0;
            *(here->MOS1BbPtr) = 0;
            *(here->MOS1DPdpPtr) = 0;      //+drainconductance
            *(here->MOS1SPspPtr) = 0;
            *(here->MOS1DdpPtr) = 0;       //-drainconductance
            *(here->MOS1GbPtr) = 0;
            *(here->MOS1GdpPtr) = 0;
            *(here->MOS1GspPtr) = 0;
            *(here->MOS1SspPtr) = 0;
            *(here->MOS1BgPtr) = 0;
            *(here->MOS1BdpPtr) = 0;
            *(here->MOS1BspPtr) = 0;
            *(here->MOS1DPdPtr) = 0;       //-drainconductance
            *(here->MOS1DPgPtr) = 0;
            *(here->MOS1DPbPtr) = 0;
            *(here->MOS1DPspPtr) = 0;
            *(here->MOS1SPgPtr) = 0;
            *(here->MOS1SPsPtr) = 0;
            *(here->MOS1SPbPtr) = 0;
            *(here->MOS1SPdpPtr) = 0;
            */
            
            
            /*
            *(here->MOS1DdPtr) = 2; /*        row 1, pivot
            *(here->MOS1GgPtr) = 7; /*        row 2, pivot
            *(here->MOS1SsPtr) = 7; /*        row 3, pivot
            *(here->MOS1BbPtr) = 7; /*        row 4, pivot
            *(here->MOS1DPdpPtr) += 2; /*     row 1, pivot 
            *(here->MOS1SPspPtr) = 7; /*      row 3, pivot
            *(here->MOS1DdpPtr) += 2; /*      row 1, pivot
            *(here->MOS1GbPtr) = 7; /*      - row 2, off-pivot
            *(here->MOS1GdpPtr) = 8; /*     - row 2, off-pivot
            *(here->MOS1GspPtr) = 9; /*     - row 2, off-pivot
            *(here->MOS1SspPtr) = 7; /*       row 3, pivot
            *(here->MOS1BgPtr) = 7; /*      - row 4, off-pivot
            *(here->MOS1BdpPtr) = 8; /*     - row 4, off-pivot
            *(here->MOS1BspPtr) = 9; /*     - row 4, off-pivot
            *(here->MOS1DPdPtr) += 2; /*      row 1, pivot 
            *(here->MOS1DPgPtr) = 7; /*     - row 1, off-pivot 
            *(here->MOS1DPbPtr) = 8; /*     - row 1, off-pivot
            *(here->MOS1DPspPtr) = 9; /*    - row 1, off-pivot
            *(here->MOS1SPgPtr) = 7; /*     - row 3, off-pivot
            *(here->MOS1SPsPtr) = 6; /*       row 3, pivot
            *(here->MOS1SPbPtr) = 8; /*     - row 3, off-pivot
            *(here->MOS1SPdpPtr) = 9; /*    - row 3, off-pivot
            
            *(ckt->CKTrhs + here->MOS1gNode) = 7; /*            row 2, Ivector
            *(ckt->CKTrhs + here->MOS1bNode) = 7; /*            row 4, Ivector
            *(ckt->CKTrhs + here->MOS1dNodePrime) = 7; /*       row 1, Ivector
            *(ckt->CKTrhs + here->MOS1sNodePrime) = 7; /*       row 3, Ivector
             
            
            */
            
            /*
            if(*(here->MOS1DdPtr) == *(here->MOS1DPdpPtr) && *(here->MOS1DPdpPtr) == *(here->MOS1DdpPtr) && *(here->MOS1DdpPtr) == *(here->MOS1DPdPtr)){
              printf("ALL THE SAME.\n");
            }
            */
//end MatFree stuff


        }
    }
    return(OK);
}
