/**********
Copyright 1990 Regents of the University of California.  All rights reserved.
Author: 1985 Thomas L. Quarles
**********/
/*
 */

#include "spice.h"
#include <stdio.h>
#include "cktdefs.h"
#include "resdefs.h"
#include "sperror.h"
#include "suffix.h"

#include "matfree_interface.h"


/*ARGSUSED*/
int
RESload(inModel,ckt)
    GENmodel *inModel;
    CKTcircuit *ckt;
        /* actually load the current resistance value into the 
         * sparse matrix previously provided 
         */
{
    register RESmodel *model = (RESmodel *)inModel;
    register RESinstance *here;

    /*  loop through all the resistor models */
    for( ; model != NULL; model = model->RESnextModel ) {

        /* loop through all the instances of the model */
        for (here = model->RESinstances; here != NULL ;
                here=here->RESnextInstance) {

/*
            printf("Resistor: %lu",here->RESposPosptr);
            printf(" %lu",here->RESnegNegptr);// += here->RESconduct;
            printf(" %lu",here->RESposNegptr);// -= here->RESconduct;
            printf(" %lu\n",here->RESnegPosptr);// -= 
	          printf("cond=%g\n",here->RESconduct);
            printf("connected to nodes %d (+ve) and %d (-ve)\n\n",here->RESposNode, here->RESnegNode);
*/

//matfree stuff - added by Coryan
/*
            bool ground_adjacent = false;
            if(here->RESposNode == 0 || here->RESnegNode == 0){
              ground_adjacent = true;
            }

            //set node info
            update_node_properties( here->RESposNode,
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false,
                                    
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false );

            update_node_properties( here->RESnegNode,
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false,
                                    
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false );

            //load conductances into MatFree structure
            sigma_G_add( here->RESposNode, here->RESnegNode, here->RESconduct );
            sigma_G_add( here->RESnegNode, here->RESposNode, here->RESconduct );
*/
//end matfree stuff


            *(here->RESposPosptr) += here->RESconduct;
            *(here->RESnegNegptr) += here->RESconduct;
            *(here->RESposNegptr) -= here->RESconduct;
            *(here->RESnegPosptr) -= here->RESconduct;
        }
    }
    return(OK);
}
