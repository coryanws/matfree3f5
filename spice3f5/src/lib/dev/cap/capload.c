/**********
Copyright 1990 Regents of the University of California.  All rights reserved.
Author: 1985 Thomas L. Quarles
**********/

#include "spice.h"
#include <stdio.h>
#include "util.h"
#include "cktdefs.h"
#include "capdefs.h"
#include "trandefs.h"
#include "sperror.h"
#include "suffix.h"

#include "matfree_interface.h"

int
CAPload(inModel,ckt)

    GENmodel *inModel;
    register CKTcircuit *ckt;
        /* actually load the current capacitance value into the 
         * sparse matrix previously provided 
         */
{
    register CAPmodel *model = (CAPmodel*)inModel;
    register CAPinstance *here;
    register int cond1;
    double vcap=0;
    double geq=0;
    double ceq=0;
    int error;


//    printf("Start capacitance loading\n");

    /* check if capacitors are in the circuit or are open circuited */
    if(ckt->CKTmode & (MODETRAN|MODEAC|MODETRANOP) ) {
        /* evaluate device independent analysis conditions */
        cond1= 
            ( ( (ckt->CKTmode & MODEDC) && 
                (ckt->CKTmode & MODEINITJCT) )
            || ( ( ckt->CKTmode & MODEUIC) &&
                ( ckt->CKTmode & MODEINITTRAN) ) ) ;
        /*  loop through all the capacitor models */
        for( ; model != NULL; model = model->CAPnextModel ) {

            /* loop through all the instances of the model */
            for (here = model->CAPinstances; here != NULL ;
                    here=here->CAPnextInstance) {
/*
                if(ckt->CKTmode & MODEINITPRED) {printf(" initpred,");}
                if(ckt->CKTmode & MODEINITTRAN) {printf(" inittran,");}
                if(ckt->CKTmode & MODETRAN) {printf(" tran,");}
                if(ckt->CKTmode & MODETRANOP) {printf(" tranop,");}
                if(ckt->CKTmode & MODEUIC) {printf(" uic");}
		printf("\n");

		printf("cap=%g, order=%d, mode=%x, cktag0=%g, cktag1=%g pos=%g, neg=%g,",here->CAPcapac, ckt->CKTorder, ckt->CKTmode, ckt->CKTag[0], ckt->CKTag[1], *(ckt->CKTrhsOld+here->CAPposNode), *(ckt->CKTrhsOld+here->CAPnegNode));
		printf(" state0_ccap=%g state1_ccap=%g, state0_qcap=%g, state1_qcap=%g\n", *(ckt->CKTstate0+here->CAPccap), *(ckt->CKTstate1+here->CAPccap),*(ckt->CKTstate0+here->CAPqcap),*(ckt->CKTstate1+here->CAPqcap));
*/
                
                if(cond1) {
                    vcap = here->CAPinitCond;
                } else {
                    vcap = *(ckt->CKTrhsOld+here->CAPposNode) - 
                        *(ckt->CKTrhsOld+here->CAPnegNode) ;   
                }
                if(ckt->CKTmode & (MODETRAN | MODEAC)) {
#ifndef PREDICTOR
                    if(ckt->CKTmode & MODEINITPRED) {
                        *(ckt->CKTstate0+here->CAPqcap) = 
                            *(ckt->CKTstate1+here->CAPqcap);
                    } else { /* only const caps - no poly's */
#endif /* PREDICTOR */
                        *(ckt->CKTstate0+here->CAPqcap) = here->CAPcapac * vcap;
                        if((ckt->CKTmode & MODEINITTRAN)) {
                            *(ckt->CKTstate1+here->CAPqcap) = 
                                *(ckt->CKTstate0+here->CAPqcap);
			}
#ifndef PREDICTOR
                    }
#endif /* PREDICTOR */
		    //printf("Capacitance: cap=%g, s0qcap=%g\n",here->CAPcapac, *(ckt->CKTstate0+here->CAPqcap));
                    error = NIintegrate(ckt,&geq,&ceq,here->CAPcapac,
                            here->CAPqcap);
                    if(error) return(error);
                    if(ckt->CKTmode & MODEINITTRAN) {
                        *(ckt->CKTstate1+here->CAPccap) = 
                            *(ckt->CKTstate0+here->CAPccap);
                    }
                    *(here->CAPposPosptr) += geq;
                    *(here->CAPnegNegptr) += geq;
                    *(here->CAPposNegptr) -= geq;
                    *(here->CAPnegPosptr) -= geq;
                    *(ckt->CKTrhs+here->CAPposNode) -= ceq;
                    *(ckt->CKTrhs+here->CAPnegNode) += ceq;

/*
		    printf("Capacitor %lu %lu %lu %lu, %lu %lu\n",
		    	here->CAPposPosptr,
			here->CAPnegNegptr,
			here->CAPposNegptr,
			here->CAPnegPosptr,
			here->CAPposNode,
			here->CAPnegNode);
		    printf("Capacitance=%g STAMP=%g\n",here->CAPcapac, geq);
*/


//matfree stuff - added by Coryan

            //it looks like this is being modelled as
            //a resistor in series with an isource.
/*
            bool ground_adjacent = false;
            if(here->CAPposNode == 0 || here->CAPnegNode == 0){
              ground_adjacent = true;
            }

            //set node info
            update_node_properties( here->CAPposNode,
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false,
                                    
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false );

            update_node_properties( here->CAPnegNode,
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false,
                                    
                                    false,
                                    false,
                                    true,
                                    ground_adjacent,
                                    true,
                                    false );

            //load conductances into MatFree structure
            sigma_G_add( here->CAPposNode, here->CAPnegNode, geq );
            sigma_G_add( here->CAPnegNode, here->CAPposNode, geq );
            
            //load currents into MatFree structure
            sigma_I_add( here->CAPnegNode, ceq   );
            sigma_I_add( here->CAPposNode, (-ceq));
*/
//end matfree stuff




                } else {
		    *(ckt->CKTstate0+here->CAPqcap) = here->CAPcapac * vcap;
		}

//		printf("Output: ceq=%g, geq=%g, state0_ccap=%g state1_ccap=%g, state0_qcap=%g, state1_qcap=%g\n",ceq, geq, *(ckt->CKTstate0+here->CAPccap), *(ckt->CKTstate1+here->CAPccap),*(ckt->CKTstate0+here->CAPqcap),*(ckt->CKTstate1+here->CAPqcap));
            } // instances
        } // models
    }
    return(OK);
}

