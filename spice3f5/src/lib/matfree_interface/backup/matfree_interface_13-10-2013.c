/**********
Copyright 2013 Regents of the University of Pennsylvania.  All rights reserved.
Author: 2013 Coryan Wilson-Shah
**********/

#include "spice.h"
#include <stdio.h>
#include "trandefs.h"
#include "cktdefs.h"
#include "smpdefs.h"
#include "sperror.h"
#include "util.h"
#include "suffix.h"
#include "spdefs.h"
#include <math.h> //for log and round, used in "int depth_gauge()"

#include "matfree_interface.h"


#define MF_CONN 1
#define MF_G 0
#define MF_NC -1

#define CONV_TOL 0.00001
#define INDI_TOL 0.000005

#define NODE_TOL 0.000005 //how much a node needs to change for us to register that its neightbours must update.

static RealNumber * diopointer;

typedef struct node_jotter{
  bool supernode;             //part of a supernode?
  bool static_tree;           //part of a supernode that's a static tree?
  bool external;              //an external node of a supernode? (connected Vsources AND conductances/isources)
  bool ground_neighbour;      //is one of this node's neighbouring nodes ground?
  bool gi_inputs;             //any current sources or conductances attached to this node?
  bool v_inputs;              //any voltage sources or conductances attached to this node?
  bool neighbour_flux;        //this is true if any of the node's neighbours have changed by more than NODE_TOL.
  ElementPtr sigma_g_src;
  RealNumber my_sigma_G;
  RealNumber my_sigma_GV;
  RealNumber my_offset;
  RealNumber my_V;
  RealNumber my_V_old;

  RealNumber ** static_V_multipliers;  //used by the old V1 stuff.
  ElementPtr * floating_sigma_G;       //used by the old V1 stuff.
  bool internal;                       //used by the old V1 stuff.
  
  //supernode stuff.
  int cursor;
  int * neighbours;
  int * v_src;    //pointer to the location in b where we will find this value.
  ElementPtr * mult_src; //pointer to the location in MNA where we will find the premultiplier.
  RealNumber * hop_size;
  bool sn_built;
  
  int supernode_index;
  RealNumber sn_internals;
  RealNumber sn_grand_gsum;
  
  bool psn_built;
  int * p_neighbours;
  int p_cursor;
  
  //remembering connections stuff.
  int g_cursor;
  int * normal_neighbours;
  ElementPtr * g_src;      //pointer to the location in MNA where we will find the conductance val.
  RealNumber * link_size;
  bool con_built;
  
  //a late-night notion.
  int ghost_cursor;
  int * ghost_neighbours;
  RealNumber * ghost_size;
  
  //for storing Isource contributions.
  RealNumber my_I_sum;
  int i_src;
  
  //for some of the results from the premultiply calculation.
  RealNumber premult_offset;
  bool premult_supernode_resolved;
  
  //for remembering how much work I've done
  int my_adds;
  int my_subs;
  int my_mults;
  int my_divs;
  int my_total_ops;
  
} node_jotter;

/* These functions are used internally; should not be called from outside.
 * They're mostly just to neaten up the code.
 */
 
//prints booleans.
void print_bool( bool in );

//updates all the internal node voltages.
bool fillin_internals( int thisnode, int from, RealNumber Vtotal, int index );

//a function used to walk a supernode; link elements and apply a label.
void static_walk_sn(int home, bool * visited, int this_node, bool is_new, int label);

//a function to walk a supernode; sum Gs and sum internals to the home node. (returns depth.)
int sn_gather_recursive(int home, bool * visited, int this_node, RealNumber voltage, RealNumber * grand_sigma_g, bool summed);

//used by the premultiply function.
void premult_populate ( RealNumber * row, bool * used, int * num_used, RealNumber * offset, int node);

//A function used by the MNA interface for plugging values into the nodejotter.
void add_MNA_entry( int index, int neighbour, ElementPtr val_src, int mode );

//performance functions
void log_add ( int num );
void log_sub ( int num );
void log_mult( int num );
void log_div ( int num );

//personal performance functions
void log_my_add ( int index, int num );
void log_my_sub ( int index, int num );
void log_my_mult( int index, int num );
void log_my_div ( int index, int num );

//a function for measuring vector_diff over successive iterations.
void note_vdiff( int mode, RealNumber diff_in );



/////////////////////////////////////////////////
// V1 below this.
/////////////////////////////////////////////////

//returns a pointer to the element at (row,col)
ElementPtr position( int row, int col );
ElementPtr position_recol( int row, int col );

//sorts out supernodes, detects ground-neighbours, etc.
void build_node ( int index );

//fixes the voltages of all nodes in a static tree.
//void build_static_trees_old( void );

//builds maps for non-static trees (should be called after build_static_trees).
void build_floating_trees( void );

// used by fix_static_trees.
void static_treewalker( int thisrow, int prevrow );

// used by fix_floating_trees.
void floating_treewalker( int thisrow, int prevrow );

//returns the type of an element (connection, conductance, or nothing.)
int recol_type( int row, int col );

//for dealing with the external nodes of floating trees.
void collect_floating_terms( int row, int from, RealNumber Vtotal, bool * visited, int node );

//for dealing with the internal nodes of floating trees.
bool collect_internal_terms( int normalrow, int from, RealNumber Vtotal, int node, RealNumber * result);


/////////////////
// The below variables *are* used by V2.
/////////////////

static CKTcircuit * matfree_ckt;  //a pointer to the last ckt passed via readin_matrix
static MatrixPtr matfree_Matrix;  //a pointer to the matrix from the above.
static unsigned long last_ID = 0; //the id of the last matrix passed in (again, the above.)
static node_jotter * nodes;       //the working set of nodes
static int nodes_set = false;      //indicates whether or not the working set of nodes has been allocated yet
static int num_nodes_matfree;      //the number of rows in the matrix that are actually summing currents at a node.
                                    //i.e. the size of [G], and the number of elements in nodes.
static bool nodes_built = false;   //true once we've allocated nodes.

static int num_abstract_rows;     //number of rows in the matrix that aren't actually sums of conductances.

static int num_supernodes;        //total number of individual supernodes.

static int instance_number = -1;   //just for monitoring which timestep we're at, for the sake of printing it out.

static bool premultiplied = false; //to remember if we've premultiplied or not.


// For performance testing:
static int adds = 0;
static int subs = 0;
static int mults = 0;
static int divs = 0;
static int total_ops = 0;

//////////////////////////////////////


/* The matfree solver. Note that this is to be used instead of the
 * earlier "solve_matfree," which is now obsolete.
 * Returns true if converged, otherwise returns false.
 * The algorithm is slightly different depending on whether or not
 * we've done the premultiply - doing it means that we've already rounded
 * up terms at each supernode, so we don't need to do it again. Thus,
 * supernodes are treated differently, contingent on the status of the
 * variable "premultiplied."
 */

bool matfree_solve( void ){

  int next_node;
  int i, temp_index;
  int j; //can be deleted; only used for printing stuff out.
  bool retval = false;
  bool converged;
  
  RealNumber partials;  //for external node calculations.
  RealNumber vector_diff; //for the convergence check
  
  //note_vdiff(0,0); //starts monitoring iterations.
  
  RealNumber * row_diff = (RealNumber*)malloc(sizeof(RealNumber)*(num_nodes_matfree+1));
  
  for(next_node = 0; next_node<=num_nodes_matfree; next_node++){
    //solve for nodes that aren't internal nodes, and aren't in a
    //static tree (as both are solved elsewhere)

    //if it's a static tree or an internal node, do nothing (as both
    //are solved elsewhere.)
    if(nodes[next_node].static_tree || !nodes[next_node].external){}

    else{
      nodes[next_node].my_V = nodes[next_node].my_sigma_GV; 
    }
    
    //while we're iterating, we might as well set everyone's neighbour flux to "false."
    nodes[next_node].neighbour_flux = false;
  }

  //now check for convergence (and see which nodes we need to update next time.)
  vector_diff = 0;
  converged = true;
  
  for(i=1; i<=num_nodes_matfree; i++){ //for each node:
    row_diff[i] = nodes[i].my_V - nodes[i].my_V_old;
    
    if(row_diff[i] > 0){
      vector_diff = vector_diff + row_diff[i];
      if (row_diff[i] >= INDI_TOL){converged = false;}
      //check whether we should update this guy's neighbours or not
      
      if(row_diff[i] >= NODE_TOL){ // if this is true, tell all its neighbours that it's changed
                                   // (and that they should react accordingly.)
		for(j=0; j<nodes[i].ghost_cursor; j++){
		  temp_index = nodes[i].ghost_neighbours[j];
		  nodes[temp_index].neighbour_flux = true;
		}		  
	  }
      
    }
    else{ 
      vector_diff = vector_diff - row_diff[i];
      if (row_diff[i] <= (-INDI_TOL)){converged = false;}
      //check whether we should update this guy's neighbours or not
      
      if(row_diff[i] <= (-NODE_TOL)){ // if this is true, tell all its neighbours that it's changed
                                      // (and that they should react accordingly.)
		for(j=0; j<nodes[i].ghost_cursor; j++){
		  temp_index = nodes[i].ghost_neighbours[j];
		  nodes[temp_index].neighbour_flux = true;
		}		  
	  }
      
    }

    nodes[i].my_V_old = nodes[i].my_V;
    
    //log ops.
    log_sub(1);
    log_my_sub(i, 1);
  }
  
  //note_vdiff(1, vector_diff); //records vector diff.
  
  //printf("  Vector diff = %f\n", vector_diff);
  
  if(vector_diff < CONV_TOL) { //we've converged.
    retval = true;
    //printf("  Converged!\n");
  }
  else{
    retval = false;
    /* This doesn't work.
    for(i=1; i<=num_nodes_matfree; i++){ //for each node:
      nodes[i].my_V_old = nodes[i].my_V_old + row_diff[i];
      
      log_add(1);
      log_my_add(i, 1);
    }
    */
  }
  
  free(row_diff);
  
  return retval;
}


/* Prepares the node jotter. if nodes is already built, this function
 * initialises the node jotter.
 */

void build_node_jotter( CKTcircuit * inckt, char * inmatrix ){

  CKTnode * temp;
  int next_node; // for iterating over nodes as we initialise them.
  instance_number++;

  if(!nodes_built){
    
    matfree_ckt = inckt;
    matfree_Matrix = (MatrixPtr) inmatrix;
    last_ID = matfree_Matrix->ID;
    
    //Link rows.
    if(!matfree_Matrix->RowsLinked){
      spcLinkRows(matfree_Matrix);
    }
    
    //print_matfree();
    
    //get the number of nodes in the array.
    num_nodes_matfree = -1;
    num_abstract_rows = 0;
    temp = &(matfree_ckt->CKTnodes[0]);

    while(temp != NULL){
      if(temp->type == 3){ num_nodes_matfree++; }
      else if(temp->type == 4){ num_abstract_rows++; }
      temp = temp->next;
    }
    
    //printf( "detected %d nodes (not including ground.)\n", num_nodes_matfree);
    //printf( "and %d \"abstract\" rows.\n", num_abstract_rows);

    //now we can build the node jotter.
    nodes=malloc(sizeof(node_jotter)*(num_nodes_matfree+1));
    
    //this bit's important for malloc'ing the supernode arrays.
    for(next_node = 0; next_node<=num_nodes_matfree; next_node++){
      nodes[next_node].sn_built = false;
      nodes[next_node].cursor = 0;
      
      nodes[next_node].psn_built = false;
      nodes[next_node].p_cursor = 0;
      
      nodes[next_node].con_built = false;
      nodes[next_node].g_cursor = 0;
      
      //and this is because we only want to initialise voltages once,
      //NOT on every timestep.
      nodes[next_node].my_V = 0;
      nodes[next_node].my_V_old = 0;
      
      //clearly we don't want to zero performance counters at every iteration:
      nodes[next_node].my_adds = 0;
      nodes[next_node].my_subs = 0;
      nodes[next_node].my_mults = 0;
      nodes[next_node].my_divs = 0;
      nodes[next_node].my_total_ops = 0;
    }

//////////////////////

  for(next_node = 0; next_node<=num_nodes_matfree; next_node++){
      //printf( "initialising node %d... ", next_node);
      nodes[next_node].supernode = false;
      nodes[next_node].static_tree = false;
      nodes[next_node].external = false;
      nodes[next_node].ground_neighbour = false;
      nodes[next_node].gi_inputs = false;
      nodes[next_node].v_inputs = false;
      nodes[next_node].premult_supernode_resolved = false;
      nodes[next_node].neighbour_flux = true; //after each model solve, update everyone.
      nodes[next_node].supernode_index = -1;
      nodes[next_node].sn_internals = 0;
      nodes[next_node].sn_grand_gsum = 0;
      nodes[next_node].my_sigma_G = 0;
      nodes[next_node].my_sigma_GV = 0;
      nodes[next_node].my_I_sum = 0;
      nodes[next_node].my_offset = 0;
      nodes[next_node].premult_offset = 0;
      if(nodes[next_node].sn_built){
        free(nodes[next_node].neighbours);
        free(nodes[next_node].hop_size);
        nodes[next_node].cursor = 0;
        nodes[next_node].sn_built = false;
      }
      if(nodes[next_node].psn_built){
        free(nodes[next_node].p_neighbours);
        nodes[next_node].p_cursor = 0;
        nodes[next_node].psn_built = false;
      }
      if(nodes[next_node].con_built){
        free(nodes[next_node].normal_neighbours);
        free(nodes[next_node].link_size);
        nodes[next_node].g_cursor = 0;
        nodes[next_node].con_built = false;
      }
      //printf( "complete.\n");
      
  }


    //Remember that we've built the node jotter.
    nodes_built = true;

  }

  //if nodes is already built, this function just initialises the nodejotter.
  else{
    //printf( "MatFree node structure already built...\n");
    //printf( "(If you are attempting to simulate a second circuit, restart the program.)\n");
    //printf( "Re-initialising nodejotter.\n");
  }
  
  //This was here before pointer method; move back if needed.
  /*
  //initialise node jotter.
  for(next_node = 0; next_node<=num_nodes_matfree; next_node++){
      //printf( "initialising node %d... ", next_node);
      nodes[next_node].supernode = false;
      nodes[next_node].static_tree = false;
      nodes[next_node].external = false;
      nodes[next_node].ground_neighbour = false;
      nodes[next_node].gi_inputs = false;
      nodes[next_node].v_inputs = false;
      nodes[next_node].premult_supernode_resolved = false;
      nodes[next_node].neighbour_flux = true; //after each model solve, update everyone.
      nodes[next_node].supernode_index = -1;
      nodes[next_node].sn_internals = 0;
      nodes[next_node].sn_grand_gsum = 0;
      nodes[next_node].my_sigma_G = 0;
      nodes[next_node].my_sigma_GV = 0;
      nodes[next_node].my_I_sum = 0;
      nodes[next_node].my_offset = 0;
      nodes[next_node].premult_offset = 0;
      if(nodes[next_node].sn_built){
        free(nodes[next_node].neighbours);
        free(nodes[next_node].hop_size);
        nodes[next_node].cursor = 0;
        nodes[next_node].sn_built = false;
      }
      if(nodes[next_node].psn_built){
        free(nodes[next_node].p_neighbours);
        nodes[next_node].p_cursor = 0;
        nodes[next_node].psn_built = false;
      }
      if(nodes[next_node].con_built){
        free(nodes[next_node].normal_neighbours);
        free(nodes[next_node].link_size);
        nodes[next_node].g_cursor = 0;
        nodes[next_node].con_built = false;
      }
      //printf( "complete.\n");
      
  }
  */

  //printf( "nodejotter initialised.\n");

  return;
}

/* this function pulls the relevant values from the MNA matrix and stores
 * them in the nodejotter. Essentially removes the need to "add" new devices
 * to MatFree, as it just works with whatever matrix it is handed.
 * This needs a bit of work (it's not quite reading in Vsources in the right way - 
 * it relies unduly on the symmetry of their stamps) but should work for 
 * basic components.
 */

void MNA_interface( void ){

  int row;
  RealNumber check, checksum;
  ElementPtr element, temp_element;
  bool end_found;
  ElementPtr multiplier = NULL;
  static bool loaded = false;
  
  int v_test_tally=0, g_test_tally=0;
/*
    retval = matfree_Matrix->FirstInRow[row];
    while(retval != NULL){
      if (retval->Col == col){
        break;
      }
      else{
        retval = retval->NextInRow;
*/
	if(!loaded){

	  for(row=1; row<=num_nodes_matfree; row++){ //just look at "node" rows. (not vsource rows)
		  
		  v_test_tally=0; g_test_tally=0;
		  printf("-- row %d: --\n", row);
		  
		  checksum = 0; //zero this in preparation.
		  check = 0;    //this too.
		  element = matfree_Matrix->FirstInRow[row]; //get first entry in row.
		  
		  while (element != NULL){ //for each element in row;
		  
			printf("column = %d\n", element->Col);
		  
			if(element->Col == row){ //if it's the pivot, it's our sigma_G at the node.
				check = element->Real;
				printf("Type 2 (Gsum) @ row %d is %f\n", row, element->Real);
				add_MNA_entry( row, 0, (element), 2 );
				printf("  TESTING: (Gsum) @ row %d is %f\n", row, ((nodes[row].sigma_g_src)->Real));
			}
			
			else if(element->Col > num_nodes_matfree){ //if it's outside of the G submatrix, it's a vsource
			
			  temp_element = matfree_Matrix->FirstInCol[(element->Col)];
			  end_found = false;
			  //either other end exists (floating vsource) or it does not (grounded vsource)
			  while(temp_element != NULL && !end_found){
				  
				  if(temp_element != element){
					  end_found = true;
					  multiplier = temp_element;
				  }
				  else{
					  temp_element = temp_element->NextInCol;
				  }
			  }
			  
		  
			  //see what we found.
			  if(end_found){
				  printf("    connected to row %d by a vsource (value %f)\n", temp_element->Row, matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[element->Col]]);
				  printf("Type 4 (vsrc) @ row %d - connected to %d by voltage %f\n", row, temp_element->Row, (multiplier->Real) * (matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[element->Col]]));
				  add_MNA_entry(row, temp_element->Row, (multiplier), 4);
				  //matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[element->Col]] is the voltage.
				  //matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[multiplier->Col]] is also the voltage.
				  printf("  TESTING: (vsrc) @ row %d to row %d is %f\n",
				         row,
				         ((nodes[row].mult_src[v_test_tally])->Row),
				         (((nodes[row].mult_src[v_test_tally])->Real) * (matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[(nodes[row].v_src[v_test_tally])]]))
				         );
			  }
			  else{
				  printf("    connected to row 0 by a vsource (value %f)\n", matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[element->Col]]);
				  printf("Type 4 (vsrc) @ row %d - connected to 0 by voltage %f\n", row, -(matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[element->Col]]));
				  printf("Type 4 (vsrc) @ row 0 - connected to %d by voltage %f\n", row, (matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[element->Col]]));
				  //add_MNA_entry(row, 0, -(matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[element->Col]]), 4);
				  //add_MNA_entry(0, row, (matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[element->Col]]), 4);
				  add_MNA_entry(row, 0, element, 4);
				  add_MNA_entry(0, row, element, 4);
			  }
			  
			}
			
			else{ //otherwise, it's a plain old conductance.
			//remember, these are all stored negative in the MNA (hence why you're seeing
			//subtracts rather than adds.)
				printf("    connected to row %d by a conductance (value %f)\n", element->Col, (-element->Real));
				checksum -= element->Real;
				printf("Type 1 (gelm) @ row %d - connected to %d by conductance %f\n", row, element->Col, (-element->Real));
				//add_MNA_entry( row, element->Col, (-element->Real), 1 );
				add_MNA_entry( row, element->Col, (element), 1 );
			}
		  
		  
			element = element->NextInRow;
		  }
		  
		  //check checksum
		  if(checksum != check){
			  printf("    connected to row 0 by a conductance (value %f)\n", (check-checksum));
			  printf("Type 1 (gelm) @ row %d - connected to 0 by conductance %f\n", row, (check-checksum));
			  printf("Type 1 (gelm) @ row 0 - connected to %d by conductance %f\n", row, (check-checksum));
			  add_MNA_entry( row, 0, NULL, 1 );
			  add_MNA_entry( 0, row, NULL, 1 );
		  }

		  
		  //finally, add on the current contribution from the rhs vector.
		  printf("    Isum in = %f\n", matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[row]]);
		  printf("Type 3 (gelm) @ row %d - Isum in is %f\n", row, (matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[row]]));
		  //add_MNA_entry( row, 0, (matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[row]]), 3 );
		  add_MNA_entry( row, 0, (matfree_Matrix->FirstInRow[row]), 3 );
		  
	  }
	  
	  //once we've finished sourcing values, let's mark static nodes as such.
	  mark_static_trees(0,0,0);
	  
	  //remember that we've done this already.
	  loaded = true;
  }
  
  //printf("end.\n");

	//for each row in matrix
	//
	//if you are a row voltage:
	//  if entry in col (n) is (a !=0):
	//    voltage link at n is a*V
	//
	//if you are a row node:
	//  for each row entry:
	//    if col# == row#:
	//      save this for check.
	//    else if col# is a voltage row:
	//      voltage link at row is a*V
	//    else if row entry !=0
	//      conductance link at row is (entry)
	//      checksum += (entry)
	//lastly (but CRUCIALLY) Isum = b[row]
	//
	//Finally, check that checksum == check

  return;

}

/* This function is called by MNA_interface to slot entries from the MNA
 * matrix into the nodejotter. "mode" tells us what type of thing we're
 * adding in:
 *   1 is a conductance
 *   2 is a sigma_G (the diagonals, basically)
 *   3 is a current total
 *   4 is a vsource/something like that
 */
 
void add_MNA_entry( int index, int neighbour, ElementPtr val_src, int mode ){

  int my_cursor, i;
  int * temp_indices;
  ElementPtr * temp_src;
  int * temp_src_int; // for vsources.
  bool ground_adjacent = (neighbour == 0);
  RealNumber check_i_z;

  //sanitise inputs:
  if((index < 0) || (index > num_nodes_matfree)){
    printf("debug: a device has attempted to update node %d,", index);
    printf("debug: which does not exist. Aborting...");
    abort();
  }

  if((neighbour < 0) || (neighbour > num_nodes_matfree)){
    printf("debug: a device has attempted to connect node %d to node %d", index, neighbour);
    printf("debug: the latter, sadly, does not exist. Aborting...");
    abort();
  }
  
  //check mode: what sort of value are we reading in here?
  
  switch(mode){
    case(1): //conductance. "[index] is connected to [neighbour] by conductance [val_in]."
	  
	  //printf("A: neighbour = %d\n", neighbour);
	  
      //if links array isn't allocated yet, do so.
      if(!nodes[index].con_built){
    
        //printf("                    no conductance array built at node %d. now building...\n", index);
    
        nodes[index].normal_neighbours=(int*)malloc(sizeof(int)*(1)); 
        nodes[index].link_size=(RealNumber*)malloc(sizeof(RealNumber)*(1));
        nodes[index].g_src=(ElementPtr*)malloc(sizeof(ElementPtr)*(1));
        nodes[index].g_cursor = 1;
        nodes[index].con_built = true;
    
        nodes[index].normal_neighbours[0] = neighbour;
        nodes[index].link_size[0] = 0;
        nodes[index].g_src[0] = val_src;
        
      }
      
      //if links array is allocated, we need to tack this chap on the end - 
      //which is no small feat, but at least we only have to do it once.
      else{

		  my_cursor = nodes[index].g_cursor + 1; //new size of the arrays.
		  
		  //Allocate temporary arrays (We'll copy stuff into these,
		  //Add the new guys, free the existing arrays, reallocate
		  //them as the correct size, copy from temps to actual,
		  //then deallocate the temporary arrays. Inefficient,
		  //but again, we shouldn't need to do this more than once
		  //per node per row, and it's more simple to implement than
		  //a geometric scheme.
          temp_indices=(int*)malloc(sizeof(int)*(my_cursor)); 
          temp_src=(ElementPtr*)malloc(sizeof(ElementPtr)*(my_cursor));
          
          //copy existing stuff to temp
          for(i=0; i<(my_cursor-1); i++){
            temp_indices[i] = nodes[index].normal_neighbours[i];
            temp_src[i] = nodes[index].g_src[i];
          }
          
          //put in new stuff
          temp_indices[i] = neighbour;
          temp_src[i] = val_src;
          
          //free and re-allocate space
          free(nodes[index].normal_neighbours);
          free(nodes[index].link_size);
          free(nodes[index].g_src);
          nodes[index].normal_neighbours=(int*)malloc(sizeof(int)*(my_cursor)); 
          nodes[index].link_size=(RealNumber*)malloc(sizeof(RealNumber)*(my_cursor));
          nodes[index].g_src=(ElementPtr*)malloc(sizeof(ElementPtr)*(my_cursor));
          
          //copy stuff back
          for(i=0; i<my_cursor; i++){
            nodes[index].normal_neighbours[i] = temp_indices[i];
            nodes[index].link_size[i] = 0;
            nodes[index].g_src[i] = temp_src[i];
          }
          
          //save new cursor
          nodes[index].g_cursor = my_cursor;
          
          //deallocate temps
          free(temp_indices);
		  free(temp_src);
	  }


      
      for(i=0; i<nodes[index].g_cursor; i++){
		//printf ("  \n %d ", nodes[index].normal_neighbours[i]);
		if (nodes[index].normal_neighbours[i] == neighbour){
			//printf(" MATCH!\n");
			 //printf("  debug: node %d connected to node %d by conductance %f\n\n", index, nodes[index].normal_neighbours[i], nodes[index].link_size[i]);
		  break;
	    }
	  }
      if(i == nodes[index].g_cursor){
		//printf("\n weird... I couldn't find %d.\n", neighbour);
	  }
     
      if (ground_adjacent){
	    nodes[index].ground_neighbour = true;
      }
      nodes[index].gi_inputs = true;

	break;//end of conductance portion.
	
	case(2): //Gsum. "The sum of conductances at node [index] is [val_in]."
	
	  //We only need to do one thing:
	  nodes[index].sigma_g_src = val_src;
	  
	break;//end of Gsum portion. "The sum of currents flowing into node [index] is [val_in]."
	  
	case(3): //Isum.
	
	  check_i_z = (matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[val_src->Row]]);
	
	  if((check_i_z) != 0){
	    //printf("Node %d has gi inputs.", index);
	    nodes[index].gi_inputs = true;
	  }
	
	  //We only need to do one thing:
	  nodes[index].i_src = (val_src->Row);
	  
	break;//end of Isum portion.
	
	case(4): //vsource-inductor-transformer type weirdo thing. "[index] is connected to [neighbour] by voltage [val_in]." 
	  
	  //printf("  %d is connected to %d via a hop of %f\n", index, neighbour, val_in);
	  
      //if hops array isn't allocated yet, do so.
      if(!nodes[index].sn_built){
		  //printf("  building hop array at node %d\n", index);
        nodes[index].neighbours=(int*)malloc(sizeof(int)*(1));
        nodes[index].hop_size=(RealNumber*)malloc(sizeof(RealNumber)*(1));
        nodes[index].v_src=(int*)malloc(sizeof(int)*(1));
        nodes[index].mult_src=(ElementPtr*)malloc(sizeof(ElementPtr)*(1));
        nodes[index].cursor = 1;
        nodes[index].sn_built = true;

        nodes[index].neighbours[0] = neighbour;
        nodes[index].hop_size[0] = 0;
        nodes[index].v_src[0] = (val_src->Col);
        nodes[index].mult_src[0] = val_src;
            
      }
      
      //this should look remarkably similar to the steps for conductances.
      
      else{

		  my_cursor = nodes[index].cursor + 1; //new size of the arrays.
		  
		  //Allocate temporary arrays (We'll copy stuff into these,
		  //Add the new guys, free the existing arrays, reallocate
		  //them as the correct size, copy from temps to actual,
		  //then deallocate the temporary arrays. Inefficient,
		  //but again, we shouldn't need to do this more than once
		  //per node per row, and it's more simple to implement than
		  //a geometric scheme.
          temp_indices=(int*)malloc(sizeof(int)*(my_cursor)); 
          temp_src=(ElementPtr*)malloc(sizeof(ElementPtr)*(my_cursor));
          temp_src_int=(int*)malloc(sizeof(int)*(my_cursor));
          
          //copy existing stuff to temp
          for(i=0; i<(my_cursor-1); i++){
            temp_indices[i] = nodes[index].neighbours[i];
            temp_src[i] = nodes[index].mult_src[i];
            temp_src_int[i] = nodes[index].v_src[i];
          }
          
          //put in new stuff
          temp_indices[i] = neighbour;
          temp_src[i] = val_src;
          temp_src_int[i] = (val_src->Col);
          
          //free and re-allocate space
          free(nodes[index].neighbours);
          free(nodes[index].hop_size);
          free(nodes[index].v_src);
          free(nodes[index].mult_src);
          nodes[index].neighbours=(int*)malloc(sizeof(int)*(my_cursor)); 
          nodes[index].hop_size=(RealNumber*)malloc(sizeof(RealNumber)*(my_cursor));
          nodes[index].v_src=(int*)malloc(sizeof(int)*(my_cursor));
          nodes[index].mult_src=(ElementPtr*)malloc(sizeof(ElementPtr)*(my_cursor));
          
          //copy stuff back
          for(i=0; i<my_cursor; i++){
            nodes[index].neighbours[i] = temp_indices[i];
            nodes[index].hop_size[i] = 0;
            nodes[index].v_src[i] = temp_src_int[i];
            nodes[index].mult_src[i] = temp_src[i];
          }

          
          //save new cursor
          nodes[index].cursor = my_cursor;

          //deallocate temps
          free(temp_indices);
		  free(temp_src);
		  free(temp_src_int);
	  }
		  
	     
      for(i=0; i<nodes[index].cursor; i++){
		//printf ("  \n %d ", nodes[index].neighbours[i]);
		if (nodes[index].neighbours[i] == neighbour){
			//printf(" MATCH!\n");
			//printf("  debug: node %d connected to node %d by voltage %f\n\n", index, nodes[index].neighbours[i], nodes[index].hop_size[i]);
		  break;
	    }
	  }
      if(i == nodes[index].cursor){
		//printf("\n weird... I couldn't find %d...\n", neighbour);
	  }
     
	  nodes[index].supernode = true;
	  nodes[index].v_inputs = true;
      if (ground_adjacent){
	     nodes[index].ground_neighbour = true;
      }

	break;//end of Vsource portion.
  };
  
  if(nodes[index].v_inputs && nodes[index].gi_inputs){
	  nodes[index].external = true;
  }
  
  return;
}

/* A function to remove the ghosted versions of conductance from the 
 * node jotter. This allows them to be made afresh for each round of
 * the iteration.
 */

void ghost_buster( void ){
	
	int index;
	
	for(index=0; index<=num_nodes_matfree; index++){
		
		nodes[index].ghost_cursor=0;
		free(nodes[index].ghost_neighbours);
		free(nodes[index].ghost_size);
		
		//the supernode internals are, for all intents and purposes,
		//a part of this ghostly scheme.
		
		nodes[index].premult_offset = 0;
		nodes[index].premult_supernode_resolved = false;
	}
	
	return;
	
}

/* This function uses the links given to the nodejotter to populate it
 * with values from the MNA matrix.
 */
 
void matfree_cktload( void ){
	
	int n, index;
	int b_row;
	int zero_index;
	int far_end, i;
	ElementPtr this;

	RealNumber g_sum_accum;

	
	for(n=0; n<=num_nodes_matfree; n++){printf("--at node %d.--\n", n);
		
		//if there are g or i sources connected to this node, get their values.
		if (nodes[n].gi_inputs){printf("  gi_inputs detected.\n", n);
			
			g_sum_accum = 0;
			zero_index = -1;
			
			//load Isrc contribution
			b_row = nodes[n].i_src;
			nodes[n].my_I_sum = (matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[b_row]]);

			
			//load g contributions
			for(index=0; index < nodes[n].g_cursor; index++){

                printf("      checking out node %d.\n",nodes[n].normal_neighbours[index]);

				if(nodes[n].normal_neighbours[index] != 0){
					
					//for most nodes, we just do this:
					if(n != 0){
						this = (nodes[n].g_src[index]);
						printf("yep we're here.\n");
						if(this == NULL){printf("lol\n");abort();}
						nodes[n].link_size[index] = -(this->Real);
						printf("yep we're here.\n");
						g_sum_accum                += (this->Real);
						printf("    g to node %d is %f.\n", nodes[n].normal_neighbours[index], nodes[n].link_size[index]);
					}
					//for the ground node, we have to figure out the size of the conductance.
					else{
						far_end = nodes[n].normal_neighbours[index];
						this = nodes[far_end].sigma_g_src;
						nodes[n].link_size[index] = this->Real;
						
						for(i=0; i<nodes[far_end].g_cursor; i++){
							this=nodes[far_end].g_src[i];
							if(this != NULL){nodes[n].link_size[index] += this->Real;}
						}
						printf("    g to node %d is %f.\n", nodes[n].normal_neighbours[index], nodes[n].link_size[index]);
						
					}

				}
				else{
					if(n != 0){
						zero_index = index;
					}
					else{//zero connected to zero? Oh, now you're being ridiculous.
						nodes[n].link_size[index] = 0;
					}
				}
			}
			
			//printf("  loading sigma_g...\n");
			
			//load gsum.
			this = nodes[n].sigma_g_src;
			if(this != NULL){
				nodes[n].my_sigma_G = this->Real;
			}
			else{
				nodes[n].my_sigma_G = 0;
				for(i=0; i<nodes[n].g_cursor; i++){
					nodes[n].my_sigma_G += nodes[n].link_size[i];
				}
			}
			
			//printf("    it is %f.\n", nodes[n].my_sigma_G);

			
			//sort out the link to ground.
			if((zero_index != -1) && (nodes[n].ground_neighbour)){ //overkill, I know.
				nodes[n].link_size[zero_index] = nodes[n].my_sigma_G + g_sum_accum;
				//printf("    g to node 0 is %f.\n", nodes[n].link_size[zero_index]);
			}
			
		}//end gi grabbing section.
		
		//if there are vsources connected to this node, load their values too.
		if(nodes[n].v_inputs){
			
			printf("  v inputs are present.\n");
			
			for(index=0; index<nodes[n].cursor; index++){
				
				far_end = nodes[n].neighbours[index];
				
				if(far_end!=0){
					//printf("      far end is %d\n",far_end);
					b_row = nodes[n].v_src[index];
					//printf("      base voltage is %f.\n", (matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[b_row]]));
					
					this = (nodes[n].mult_src[index]);
					//printf("      multiplier is %f.\n", this->Real);
					
					//printf("      hop size is %f.\n", nodes[n].hop_size[index]);
					//printf("      product is %f.\n", (this->Real * (matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[b_row]])));
					
					nodes[n].hop_size[index] = (this->Real * (matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[b_row]]));
					
				}
				else{
					//printf("      far end is %d\n",0);
					b_row = nodes[n].v_src[index];
					this = (nodes[n].mult_src[index]);
					nodes[n].hop_size[index] = (-(this->Real)) * (matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[b_row]]);
				}
				
				//printf("    hop from %d to node %d is %f.\n",n,far_end,nodes[n].hop_size[index]);
				
			}
			
		}
		
	}

	return;
}

/* A testing function that can be safely deleted.
 */

void matfree_check( int node, int neighbour ){
	int i;
	for(i=0;i<nodes[node].g_cursor;i++){
		if (nodes[node].normal_neighbours[i] == neighbour){
			//check that the pointer between them is non-null
			if (nodes[node].g_src[i] == NULL){
				printf("matfree_check reporting a null pointer. failing...\n");
				abort();
			}
		}
	}
	
	printf("matfree check passed.\n");
	matfree_pause();
}

/* this function "links" supernodes; i.e. it gives supernodes the indices
 * of all the other nodes in their supernode.
 */
void link_sn( void ){
    
  int next_node, i;
  int index_cursor = 0;
  bool new_sn;
  bool * nodes_visited;
  
  //make array of indices.
  nodes_visited = (bool*)malloc(sizeof(bool)*(num_nodes_matfree+1));
  
  //iterate over all nodes, looking for nodes identified as being super.
  for(next_node = 0; next_node<=num_nodes_matfree; next_node++){
    
    if(nodes[next_node].supernode){
    
      //if this node's index is (-1), then its subgraph hasn't been analysed yet.
      if(nodes[next_node].supernode_index == -1){ new_sn = true; }
      else { new_sn = false; }

      //prepare storage.
      for(i = 0; i<=num_nodes_matfree; i++){
        nodes_visited[i] = false;
      }
      
      //call a function to walk the supernode subgraph, giving parent node
      //the indices of all nodes for which gi_inputs is true. Also, slap
      //the index_cursor on everyone if they don't have one already.
      static_walk_sn(next_node, nodes_visited, next_node, new_sn, index_cursor);
      
      //if this was a new supernode, increment cursor.
      if(new_sn){ index_cursor++; }

    }
    
  }

  //free memory.
  free(nodes_visited);

    //printf("supernodes linked.\n");
    //remember how many supernodes we found here.
    num_supernodes = index_cursor;
  
  return;
}

/* This function sets up static trees for us.
 * To set it in motion, call:
 *   build_static_trees( 0, 0, 0, 0)
 * which will start it from node 0 (ground).
 * (it assumes that vsources are arranged in trees, with no loops.)
 */

void build_static_trees( int here_node, int prev_node, RealNumber voltage, int step){

  int i;

    printf("  At node %d. \n", here_node);

  //if we've come *back* to ground, then abort.
  if(here_node == 0 && here_node != prev_node){
	
	//depth_new
	depth_gauge_new(1,3,0); //consolidate.
	 
	printf("  At node %d: It's ground again. Going back up. \n", here_node);
	 
    return;
  }

  //set this node's voltage to (voltage).
  nodes[here_node].my_V = voltage;
  
  //label this node as a static tree node.
  nodes[here_node].static_tree = true;
  
  
  //print neighbours
  for(i=0; i<nodes[here_node].cursor; i++){
    printf("  | %d |\n", nodes[here_node].neighbours[i]);
  }
  
  
  //depth_new
  bool consolidate = true;//assume that this node is the end of the line
  
  //for each node connected via a Vsource (node != prev_node)
  for(i=0; i<nodes[here_node].cursor; i++){
    if(nodes[here_node].neighbours[i] != prev_node){
		
		//depth_new
		consolidate = false;//learn that this node isn't the end of the line
		depth_gauge_new(1,1,0); //increment.
		
        //call
        //  build_static_trees( node, here_node, (voltage+hop))
        printf("  checking out node %d\n", nodes[here_node].neighbours[i]);
        build_static_trees(nodes[here_node].neighbours[i], here_node, (voltage + nodes[here_node].hop_size[i]), step+1);
        
        //log ops
        log_add(1);
        log_my_add(nodes[here_node].neighbours[i], 1);
        
        //depth_new
        depth_gauge_new(1,-1,0); //decrement.
    }
  }
  
  //depth_new
  if(consolidate){
	  depth_gauge_new(1,3,0);
  }

  return;
}

/* This function is a lot like build_static trees,
 * however it only marks nodes as being static - 
 * it does not actually calculate their values.
 */

void mark_static_trees( int here_node, int prev_node, int step){

  int i;

    printf("  At node %d. \n", here_node);

  //if we've come *back* to ground, then abort.
  if(here_node == 0 && here_node != prev_node){
	
	//depth_new
	depth_gauge_new(1,3,0); //consolidate.
	 
	printf("  At node %d: It's ground again. Going back up. \n", here_node);
	 
    return;
  }
  
  //label this node as a static tree node.
  nodes[here_node].static_tree = true;
  
  //print neighbours
  for(i=0; i<nodes[here_node].cursor; i++){
    printf("  | %d |\n", nodes[here_node].neighbours[i]);
  }

  //depth_new
  bool consolidate = true;//assume that this node is the end of the line
  
  //for each node connected via a Vsource (node != prev_node)
  for(i=0; i<nodes[here_node].cursor; i++){
    if(nodes[here_node].neighbours[i] != prev_node){
		
		//depth_new
		consolidate = false;//learn that this node isn't the end of the line
		depth_gauge_new(1,1,0); //increment.
		
        //call
        //  build_static_trees( node, here_node, (voltage+hop))
        printf("  checking out node %d\n", nodes[here_node].neighbours[i]);
        mark_static_trees(nodes[here_node].neighbours[i], here_node, step+1);
        
        //log ops
        log_add(1);
        log_my_add(nodes[here_node].neighbours[i], 1);
        
        //depth_new
        depth_gauge_new(1,-1,0); //decrement.
    }
  }
  
  //depth_new
  if(consolidate){
	  depth_gauge_new(1,3,0);
  }

  return;
}


/* This function deals with updating the voltage at a specified external
 * node.
 * (it assumes that vsources are arranged in trees, with no loops.)
 */

void handle_external( int thisnode, int from, RealNumber Vtotal, RealNumber * partials, RealNumber * conductances ){
  
  int i;
  
  if(nodes[thisnode].external){ //if we've reached an external node,
    *(partials) += nodes[thisnode].my_sigma_GV;
    *(partials) -= (nodes[thisnode].my_sigma_G * Vtotal);
    *(conductances) += nodes[thisnode].my_sigma_G;
    //log_add(2);
    //log_sub(1);
    //log_mult(1);
    //log_my_add(thisnode,2);
    //log_my_sub(thisnode,1);
    //log_my_mult(thisnode,1);
  }

  //for each node connected via a Vsource (node != prev_node)
  for(i=0; i<nodes[thisnode].cursor; i++){
    if(nodes[thisnode].neighbours[i] != from){
        //call
        handle_external(nodes[thisnode].neighbours[i], thisnode, (Vtotal + nodes[thisnode].hop_size[i]), partials, conductances);
        //log_add(1);
        //log_my_add(nodes[thisnode].neighbours[i],1);
    }
  }
  return;
}

/* This function calls fillin_internals on each of the internal nodes.
 */

void handle_internals ( void ){
  
  int next_node;
  
  for(next_node = 0; next_node<=num_nodes_matfree; next_node++){
    if(!nodes[next_node].external){
      fillin_internals(next_node, next_node, 0, next_node);
    }
  }
  return;
}

/* This function sorts out the voltage of an internal node, based on
 * whatever the current solution is. Best called once convergence of
 * the main section is found.
 * Should basically always return true, unless something is REALLY going
 * wrong.
 * (it assumes that vsources are arranged in trees, with no loops.)
 */

bool fillin_internals( int thisnode, int from, RealNumber Vtotal, int index ){

  int i;
  bool success = false;

  //if thisnode is an external node, then we're done.
  if(nodes[thisnode].external){
    nodes[index].my_V_old = nodes[thisnode].my_V_old - Vtotal;
    nodes[index].my_V = nodes[index].my_V_old;
    success = true;
    
    //log ops.
    log_sub(1);
    log_my_sub(thisnode, 1);
    
    //depth_new
    depth_gauge_new(9, 2, 0);
  }
  
  //if it's not, then keep going
  
  //for each node connected via a Vsource (node != prev_node)
  for(i=0; ((i<nodes[thisnode].cursor) && !success); i++){
    if(nodes[thisnode].neighbours[i] != from){
		
		//depth_new
		depth_gauge_new(9,1,1); //add one on.
		
        //call
        //  build_static_trees( node, here_node, (voltage+hop))
        success = fillin_internals(nodes[thisnode].neighbours[i], thisnode, (Vtotal + nodes[thisnode].hop_size[i]), index);
        
        //log ops.
        log_add(1);
        log_my_add(nodes[thisnode].neighbours[i],1);
        
		//depth_new
		depth_gauge_new(9,-1,1); //add one on.
    }
  }

  return success;
}

/* This function actually only computes the Sigma_GV terms at each node;
 * the Sigma_G terms are built by compute_static_sigmas().
 * (It's the main operation in the matfree loop)
 */

void compute_sigmas( void ){

  int next_node, i;
  int index;
  RealNumber g;

  for(next_node = 0; next_node<=num_nodes_matfree; next_node++){

	if(/*nodes[next_node].neighbour_flux*/ true){ // only recalculate nodes whose neighbours have changed.
		
		//depth_new.
		depth_gauge_new(7,1,nodes[next_node].g_cursor); 
		
		printf("Node %d:\n", next_node);
		//printf( "Sigma_GV @ node %d : Isum = %f\n", next_node, nodes[next_node].my_I_sum);
		//printf( "Sigma_G @ node %d : sigma_G = %f\n", next_node, nodes[next_node].my_sigma_G);

		//first, add in the Isource/internals contribution.
		nodes[next_node].my_sigma_GV = nodes[next_node].premult_offset;
		printf( "  premult_offset = %f\n", nodes[next_node].premult_offset);
		

		//add partial current contributions from neighbouring (ghost) nodes.
		for(i=0; i<nodes[next_node].ghost_cursor; i++){
		  index = nodes[next_node].ghost_neighbours[i];
		  g = nodes[next_node].ghost_size[i];
		  
		  printf( "  premult_offset = %f\n", nodes[next_node].premult_offset);
		  
		  //printf( "Sigma_GV @ node %d : adding contribution from node %d (V=%f) of size %f\n", next_node, index, nodes[index].my_V_old, (g * nodes[index].my_V_old));
		  nodes[next_node].my_sigma_GV += (g * nodes[index].my_V_old);
		  
		  printf( "  plus %f times the voltage at node %d\n", g, index);
		  
		  //log ops.
		  log_add(1);
		  log_mult(1);
		  log_my_add(next_node, 1);
		  log_my_mult(next_node, 1);

		  //printf( "Sigma_GV @ node %d : new value %f\n", next_node, nodes[next_node].my_sigma_GV);

		  //printf( "Sigma_GV @ node %d : voltage at node %d is %f\n", next_node, index, nodes[index].my_V_old);
		  //printf( "Sigma_GV @ node %d : adding %f to sigma_GV\n", next_node, (g * nodes[index].my_V_old));
		  //printf( "Sigma_GV @ node %d : new total is %f\n\n", next_node, nodes[next_node].my_sigma_GV );
		  //log_add(2);
		  //log_mult(1);
		  //log_my_add(next_node,2);
		  //log_my_mult(next_node,1);
		}

	}
	else{ // if none of this node's neighbours have updated, just whack in its old value.
	      // (saves us doing empty work, particularly at the beginning and end of the process.)
		nodes[next_node].my_sigma_GV = nodes[next_node].my_V_old;
	}
  }
  
  matfree_pause();
  
  return;  
}

/* This function computes sigma_G at each node. Separated it out so that
 * we don't have to do it at each iteration; can do it once per model
 * solve.
 */

void compute_static_sigmas( void ){
  
  int next_node, i;
  RealNumber g;

  //depth_new
  depth_gauge_new(2,0,0);
  
  for(next_node = 0; next_node<=num_nodes_matfree; next_node++){
    g = 0;
    for(i=0; i<nodes[next_node].g_cursor; i++){
      g += nodes[next_node].link_size[i];
      
    }
    //finally, store;
    nodes[next_node].my_sigma_G = g;
    
    //depth_new
    depth_gauge_new(2,1,nodes[next_node].g_cursor);
    
  }
  
  return;
}

/* This function sums internal partials for each node in a supernode.
 * It also sums the sigma_G's across all nodes in a supernode, and
 * broadcasts this value to all external nodes in the supernode.
 */
void supernode_gather_terms( void ){

  int next_node, i, this_sn, temp_index;
  RealNumber grand_sigma_g;
  bool * nodes_visited;
  bool * sn_handled;
  
  //depth_new
  depth_gauge_new(3,0,0);//zero locals
  

  //make array of indices.
  nodes_visited = (bool*)malloc(sizeof(bool)*(num_nodes_matfree+1));
  

  
  //make array of supernodes.
  sn_handled = (bool*)malloc(sizeof(bool)*(num_supernodes));
  for(i = 0; i<num_supernodes; i++){
    sn_handled[i] = false;
  }

  //iterate over all nodes, looking for nodes identified as being part of a supernode.
  for(next_node = 0; next_node<=num_nodes_matfree; next_node++){
    
    //if this is a supernode, let's handle it.
    if(nodes[next_node].supernode && !nodes[next_node].static_tree){
    
      this_sn = nodes[next_node].supernode_index;
      //printf("supernode index = %d\n", this_sn);
    
      //prepare storage.
      grand_sigma_g = 0;
      for(i = 0; i<=num_nodes_matfree; i++){
        nodes_visited[i] = false;
      }
      
      //zero sn_internals.
      nodes[next_node].sn_internals = 0;
      
      //call a function to walk the supernode subgraph, summing sigma_gs and
      //summing internals.
      
      sn_gather_recursive(next_node, nodes_visited, next_node, 0, &grand_sigma_g, sn_handled[this_sn]);
      
      //depth_new
      depth_gauge_new(3,3,0);
      
      
      //printf("finished the recursive walk to sum sigma_g's.\n");
      
      //if this was a new supernode, allocate grand_sigma_g to everyone.
      if(!sn_handled[this_sn]){
        
        //printf("haven't been to this node before; allocating sigma_g to everyone.\n");

        //throw grand_sigma_g at everyone.
        for(i=0; i<nodes[next_node].p_cursor; i++){
          
          //printf("  getting node\n");
          
          temp_index = nodes[next_node].p_neighbours[i];
          
          //printf("  node is %d\n", temp_index);
          
          nodes[temp_index].sn_grand_gsum = grand_sigma_g;
          
          //printf("  node %d gets it...\n", temp_index);
          
        }
        //including ourself.
        nodes[next_node].sn_grand_gsum = grand_sigma_g;
        
        //printf("  I get it...\n", temp_index);
        
      }
      sn_handled[this_sn] = true;
      //printf("  finished throwing sigma_g at everyone.\n");
    }
    
  }
  
  //printf("complete. Deallocating...\n");
  
  //free allocated memory.
  free(nodes_visited);
  free(sn_handled);
  
  //printf("deallocated.\n");
  
  return;

}

/* divides all G, I terms by the sigma_G at a node (or its grand_sigma_G
 * if the node is a supernode.) Essentially saves having to do this division
 * every time we iterate, and makes the premultiply a bit more straightforward.
 */
void invert_sigmas( void ){
  
  int this_node, i;
  
  print_nodes();
  
  //depth_new
  depth_gauge_new(4,0,0); //zero local vars.
  depth_gauge_new(4,1,0); //Add one (unit depth.)
  depth_gauge_new(4,3,0); //commit this one as max.
  
  for(this_node = 0; this_node<=num_nodes_matfree; this_node++){
	  
	  //printf("**now at node %d.\n", this_node);
    
    //if node is internal or static, don't worry about it.
    if((!nodes[this_node].external && nodes[this_node].supernode) || nodes[this_node].static_tree){}
    
    //else, if it's an external node:
    else if(nodes[this_node].supernode){
		
		//printf("**  external node.\n");

      for(i=0; i<nodes[this_node].g_cursor; i++){
		  
		  //printf("**  link to %d is of size %f\n", nodes[this_node].normal_neighbours[i], nodes[this_node].link_size[i]);
		  //printf("**  gsum is of size %f\n", nodes[this_node].sn_grand_gsum);
		  
        nodes[this_node].link_size[i] = nodes[this_node].link_size[i] / nodes[this_node].sn_grand_gsum;
        
		  //printf("**  new link is of size %f\n\n", nodes[this_node].link_size[i]);
        
        log_div(1);
        log_my_div( this_node, 1 );
        
        
      }

      nodes[this_node].my_I_sum = nodes[this_node].my_I_sum / nodes[this_node].sn_grand_gsum;
      nodes[this_node].sn_internals = nodes[this_node].sn_internals / nodes[this_node].sn_grand_gsum;
      log_div(2);
      log_my_div( this_node, 2 );

    }
    
    //else, if it's a normal node:
    else{

      for(i=0; i<nodes[this_node].g_cursor; i++){
		  
		  //printf("**  link to %d is of size %f\n", nodes[this_node].normal_neighbours[i], nodes[this_node].link_size[i]);
		  //printf("**  gsum is of size %f\n", nodes[this_node].my_sigma_G);
		  
        nodes[this_node].link_size[i] = nodes[this_node].link_size[i] / nodes[this_node].my_sigma_G;
        
		  //printf("**  new link is of size %f\n\n", nodes[this_node].link_size[i]);
        
        log_div(1);
        log_my_div( this_node, 1 );
        
      }

      nodes[this_node].my_I_sum = nodes[this_node].my_I_sum / nodes[this_node].my_sigma_G;
      log_div(1);
      log_my_div( this_node, 1 );

    }
    
  }
  
  return;
}

/* Allocates my_offset at each node (essentially Isum - internals.)
 * Also resolves supernodes: for each supernode, this function rounds
 * up all the important factors and stores them in the g array.
 * After this function has been called, the voltage at a node (be it
 * external or normal) should be simply the sum of all its G'Vs, plus
 * its offset value.
 */

void resolve_equations( void ){
  
  int this_node, i, j, temp_cursor, neighbour;
  
  //depth_new
  depth_gauge_new(5,5,(num_nodes_matfree+1)); //create temporary storage.
  
  //arrays for intermediate storage.
  bool * links_need_updating = (bool*)malloc(sizeof(bool)*(num_nodes_matfree+1));
  int ** indices        = (int**) malloc(sizeof(int*)*(num_nodes_matfree+1));
  RealNumber ** hop_sizes  = (RealNumber**)malloc(sizeof(RealNumber*)*(num_nodes_matfree+1));
  int * cursors           = (int*) malloc(sizeof(int)*(num_nodes_matfree+1));
  RealNumber * offsets  = (RealNumber*)malloc(sizeof(RealNumber)*(num_nodes_matfree+1));
  
  RealNumber * temp_row = (RealNumber*)malloc(sizeof(RealNumber)*(num_nodes_matfree+1));
  bool * used = (bool*)malloc(sizeof(bool)*(num_nodes_matfree+1));
  
  //initialise.
  for(this_node=0; this_node<=num_nodes_matfree; this_node++){
    links_need_updating[this_node] = false;
    cursors[this_node] = 0;
    offsets[this_node] = 0;
  }
  
  //walk through nodes.
  for(this_node=0; this_node<=num_nodes_matfree; this_node++){
    
    //for normal nodes, all we need to do is save my_I_sum as the new offset:
    if(!nodes[this_node].supernode){
      offsets[this_node] = nodes[this_node].my_I_sum;
      
      //but let's tidy up the nodes for the sake of neatness:
      
      //initialise the temporary storage vector.
      for(i=0; i<=num_nodes_matfree; i++){
        used[i] = false;
        temp_row[i] = 0;
      }

      //read in g values at this node.
      for(i=0; i<nodes[this_node].g_cursor; i++){
        used[nodes[this_node].normal_neighbours[i]] = true;
        temp_row[nodes[this_node].normal_neighbours[i]] = nodes[this_node].link_size[i];
      }
      
      //save them in the intermediate storage - in the correct order.
      hop_sizes[this_node] = (RealNumber*)malloc(sizeof(RealNumber) * nodes[this_node].g_cursor);
      indices[this_node] = (int*) malloc(sizeof(int) * nodes[this_node].g_cursor);
      temp_cursor = 0;
      for(i=0; i<=num_nodes_matfree; i++){
        if(used[i]){
          hop_sizes[this_node][temp_cursor] = temp_row[i];
          indices[this_node][temp_cursor] = i;
          temp_cursor++;
        }
      }
      
      //a light check.
      if(temp_cursor != nodes[this_node].g_cursor){ printf("oh dear...\n"); abort(); }
      
      //store the cursor.
      cursors[this_node] = temp_cursor;
      
      //remind ourselves that this node needs updating.
      links_need_updating[this_node] = true;
      
      //and we're done for normal nodes.
    }
    
    //for internal and static nodes, we do plenty of nothing.
    else if (nodes[this_node].static_tree || !nodes[this_node].external){}
    
    //for external nodes, we do a round-robin of all the external nodes
    //in the supernode (this one included) and assemble the node equation.
    //we also build the offset, which is of course the Isum at each external
    //node in the sn minus this node's internals.
    else{
      
      //depth_new
      depth_gauge_new(5,0,0); //initialise the storage vector
      depth_gauge_new(5,1,(nodes[this_node].p_cursor+2)); //this is all the internals we'll ever need to add.
      
      //first, initialise the temporary storage vector.
      for(i=0; i<=num_nodes_matfree; i++){
        used[i] = false;
        temp_row[i] = 0;
      }

      //deal with the g, I, internal contributions at this node.
      offsets[this_node] = nodes[this_node].my_I_sum - nodes[this_node].sn_internals;
      
      //log ops.
      log_sub(1);
      log_my_sub(this_node, 1);
      
      for(i=0; i<nodes[this_node].g_cursor; i++){
        used[nodes[this_node].normal_neighbours[i]] = true;
        temp_row[nodes[this_node].normal_neighbours[i]] += nodes[this_node].link_size[i];
        
        //log ops.
        log_add(1);
        log_my_add(this_node, 1);
        
        //depth_new
        depth_gauge_new(5,2,(nodes[this_node].normal_neighbours[i])); //look for collisions.
        
      }

      //then for every other node in the sn,
      //add in its g, I contributions.
      for(i=0; i<nodes[this_node].p_cursor; i++){

        neighbour = nodes[this_node].p_neighbours[i];
        
        offsets[this_node] += nodes[neighbour].my_I_sum;
        
        //log ops.
        log_add(1);
        log_my_add(this_node, 1);

        for(j=0; j<nodes[neighbour].g_cursor; j++){
          temp_row[nodes[neighbour].normal_neighbours[j]] += nodes[neighbour].link_size[j];
          used[nodes[neighbour].normal_neighbours[j]] = true;
          
          //depth_new
          depth_gauge_new(5,2,(nodes[neighbour].normal_neighbours[j])); //look for collisions.
        }
      }
      
      //figure out the new size of the node's g array.
      for(i=0; i<=num_nodes_matfree; i++){
        if(used[i]){
          cursors[this_node]++;
        }
      }
      
      //copy new stuff into intermediate storage.
      hop_sizes[this_node] = (RealNumber*)malloc(sizeof(RealNumber) * cursors[this_node]);
      indices[this_node] = (int*) malloc(sizeof(int) * cursors[this_node]);
      temp_cursor = 0;
      for(i=0; i<=num_nodes_matfree; i++){
        if(used[i]){
          hop_sizes[this_node][temp_cursor] = temp_row[i];
          indices[this_node][temp_cursor] = i;
          temp_cursor++;
        }
      }
      
      //remind ourselves that this node needs updating.
      links_need_updating[this_node] = true;
      
      //depth_new
      depth_gauge_new(5,3,(num_nodes_matfree+1));//compare and reduce.
      
      //and we're done for external nodes.
    }
    
  }
  
  //now save this as GHOST connections in the node jotter.
  for(this_node=0; this_node<=num_nodes_matfree; this_node++){
	  
    if(links_need_updating[this_node] == true){
      
      nodes[this_node].ghost_cursor = cursors[this_node];
      nodes[this_node].premult_offset = offsets[this_node];
      
      //ghosts should not be built at this point, so
      //can allocate them.
      //free(nodes[this_node].normal_neighbours);
      //free(nodes[this_node].link_size);
      
      //reallocate them, at the correct size.
      nodes[this_node].ghost_neighbours  = (int*)        malloc(sizeof(int)*nodes[this_node].ghost_cursor);
      nodes[this_node].ghost_size        = (RealNumber*) malloc(sizeof(RealNumber)*nodes[this_node].ghost_cursor);
      
      //copy stuff across.
      for(i=0; i<nodes[this_node].ghost_cursor; i++){
        nodes[this_node].ghost_neighbours[i] = indices[this_node][i];
        nodes[this_node].ghost_size[i]       = hop_sizes[this_node][i];
      }
      
      //deallocate these parts of the intermediate storage.
      free(indices[this_node]);
      free(hop_sizes[this_node]);
      
    }
    else{//ghosts are easy.
      nodes[this_node].ghost_cursor = nodes[this_node].g_cursor;
      nodes[this_node].premult_offset = nodes[this_node].my_I_sum;
      
      nodes[this_node].ghost_neighbours  = (int*)        malloc(sizeof(int)*nodes[this_node].ghost_cursor);
      nodes[this_node].ghost_size        = (RealNumber*) malloc(sizeof(RealNumber)*nodes[this_node].ghost_cursor);
      
      for(i=0; i<nodes[this_node].ghost_cursor; i++){
        nodes[this_node].ghost_neighbours[i] = nodes[this_node].normal_neighbours[i];
        nodes[this_node].ghost_size[i]       = nodes[this_node].link_size[i];
      }
	}
  }
  
  //now that everything is safely copied: deallocate the storage.
  free(links_need_updating);
  free(indices);
  free(hop_sizes);
  free(cursors);
  free(offsets);
  free(temp_row);
  free(used);
  
  //depth_new
  depth_gauge_new(5, -5,0); // free storage.
  
  //to check: print the ghosts.
  for(this_node=0; this_node<=num_nodes_matfree; this_node++){
	  printf("  node %d has the following ghost connections:\n", this_node);
	  for(i=0; i<nodes[this_node].ghost_cursor; i++){
		  printf("    %d via a conductance of %f\n", nodes[this_node].ghost_neighbours[i], nodes[this_node].ghost_size[i]);
		  printf("    (compare with %f)\n", nodes[this_node].link_size[i]);
	  }
	  printf("    offset of %f:\n", nodes[this_node].premult_offset);
	  
  }
  
  //matfree_pause();
  
  return;
}
      

/* Performs a "premultiply" - essentially spreading out the neighbourhood
 * of nodes we're looking at to get voltage information. Note that this
 * function powers the matrix to a power (2n+1); the idea being that it's
 * as much work to compute ((G^2)^2) as it is to compute (G*G*G) so we
 * might as well just power it.
 * The +1 term is so that the power is always odd for all n. Odd means
 * that neighbouring nodes aren't artificially prevented from knowing
 * about each other; may or may not actually be necessary in practice
 * (though it appears to be a good idea on paper.)
 */

void matfree_premult( int n ){

  int this_node, i,j,power;
  int test; //delete once we've finished printf-ing.

  //arrays.
  RealNumber ** hop_vals;
  int ** hop_indices;
  RealNumber * offsets;
  int * cursors;
  
  RealNumber * temp_hops;
  bool * used;

  RealNumber temp_offset;
  int temp_cursor;
  int num_used;

    //allocate a bunch of memory.
    hop_vals      = (RealNumber**)malloc(sizeof(RealNumber*)*(num_nodes_matfree+1));
    hop_indices   = (int**)       malloc(sizeof(int*)       *(num_nodes_matfree+1));
    offsets       = (RealNumber*) malloc(sizeof(RealNumber) *(num_nodes_matfree+1));
    cursors       = (int*)        malloc(sizeof(int)        *(num_nodes_matfree+1));
    
    temp_hops = (RealNumber*) malloc(sizeof(RealNumber) *(num_nodes_matfree+1));
    used      = (bool*)       malloc(sizeof(bool)       *(num_nodes_matfree+1));

    //every time around this loop, we square the "matrix."
    for(power = 0; power < n; power++){

    printf("  power = %d\n", power);

      //we don't actually care about the ground node, so start at 1.
      for(this_node = 1; this_node<=num_nodes_matfree; this_node++){

        //printf("  this node is node %d\n", this_node);

        //do nothing for internal nodes or static trees, as they aren't
        //updated by the matrix multiplication.
        if((nodes[this_node].supernode && !nodes[this_node].external) || nodes[this_node].static_tree){ }
        else{

          //prepare the temporary storage.
          for(i=0; i<=num_nodes_matfree; i++){
            temp_hops[i] = 0;
            used[i] = false;
          }
          temp_offset = nodes[this_node].premult_offset;
          num_used = 0;
          
          //build the "new" row:
          premult_populate(temp_hops, used, &num_used, &temp_offset, this_node);
          
          //now that we've populated a row, store it in our allocated memory.
          
          //copying into memory...
          //printf("\n  Copying into memory...\n");
          
          temp_cursor = 0;
          cursors[this_node] = num_used;
          offsets[this_node] = temp_offset;
          hop_vals[this_node]      = (RealNumber*)malloc(sizeof(RealNumber)*(num_used));
          hop_indices[this_node]   = (int*)       malloc(sizeof(int)       *(num_used));
          for(i=0; i<=num_nodes_matfree; i++){
            if(used[i]){
              hop_vals[this_node][temp_cursor]    = temp_hops[i];
              hop_indices[this_node][temp_cursor] = i;
              temp_cursor++;
            }
          }

          //a quick check.
          if(temp_cursor != num_used){ printf("oh dear.\n\n\n\n"); abort(); }

        }

      }

      //once all nodes have been filled in, copy into node jotter.
      
      printf("  All nodes filled in. Copying into node jotter...\n");
      
      for(this_node = 1; this_node<=num_nodes_matfree; this_node++){

        //printf("  this_node = %d\n", this_node);

        //do nothing for internal nodes or static trees, as they aren't
        //updated by the matrix multiplication.
        if(!nodes[this_node].external || nodes[this_node].static_tree){}
        else{
          nodes[this_node].g_cursor = cursors[this_node];
          nodes[this_node].premult_offset = offsets[this_node];
          
          free(nodes[this_node].normal_neighbours);
          free(nodes[this_node].link_size);

          nodes[this_node].normal_neighbours = (int*)       malloc(sizeof(int)*cursors[this_node]);
          nodes[this_node].link_size         = (RealNumber*)malloc(sizeof(RealNumber)*cursors[this_node]);
          
          for(i=0; i<cursors[this_node]; i++){
            nodes[this_node].normal_neighbours[i] = hop_indices[this_node][i];
            nodes[this_node].link_size[i]         = hop_vals[this_node][i];
          }
          
          //if it's a supernode, mark that we've resolved it (i.e. walked all externals)
          if(nodes[this_node].supernode){
            nodes[this_node].premult_supernode_resolved = true;
          }
          
          //free memory as we go:
          free(hop_indices[this_node]);
          free(hop_vals[this_node]);
          
        }
        
      }
      
      //repeat as many times as is necessary.

    }
    
    //free memory.
    free(hop_vals);
    free(hop_indices);
    free(offsets);
    free(cursors);
    free(temp_hops);
    free(used);
    
//  }
  
  return;
}

/* a subfunction used by matfree_premult().
 */

void premult_populate ( RealNumber * row, bool * used, int * num_used, RealNumber * offset, int node){

  node_jotter * this_node = &(nodes[node]);
  node_jotter * this_neighbour;
  node_jotter * next_external;
  int neighbour_index, i, j;

/*
printf("starting to populate node %d. Initial offset = %f\n", node, (*offset));

printf("connected to these neighbours:");
  for(neighbour_index=0; neighbour_index<this_node->g_cursor; neighbour_index++){
    printf(" %d", this_node->normal_neighbours[neighbour_index]);
  }
  printf("\n");
  */

  //for every neighbour who features in this node's voltage equation,
  for(neighbour_index=0; neighbour_index<this_node->g_cursor; neighbour_index++){ //step through this_node->normal_neighbours[neighbour_index].
    
    //printf("connected to neighbour %d\n", this_node->normal_neighbours[neighbour_index]);
    
    this_neighbour = &(nodes[this_node->normal_neighbours[neighbour_index]]);
    
    //if it's a fixed node, do nothing.
    if(this_neighbour->static_tree){
      
      //printf("node %d is a fixed node.\n", this_node->normal_neighbours[neighbour_index]);
      
      row[this_node->normal_neighbours[neighbour_index]] += this_node->link_size[neighbour_index];
      
      //log ops.
      log_add(1);
      log_my_add(node, 1);
      
      //depth_new
      depth_gauge_new(6,1,0); //we're going to add on one to everyone; this'll be covered.
      
      if(!used[this_node->normal_neighbours[neighbour_index]]){
        used[this_node->normal_neighbours[neighbour_index]] = true;
        (*num_used)++;
      }
    }
    
    //otherwise, proceed thus:
    else{
      
      //depth_new
      depth_gauge_new(6,1, this_neighbour->g_cursor); //all the adds that we need to do.
      
      
      //printf("node %d is a non-static node.\n", this_node->normal_neighbours[neighbour_index]);
      
      for(j=0; j<this_neighbour->g_cursor; j++){

        row[this_neighbour->normal_neighbours[j]] += this_neighbour->link_size[j] * this_node->link_size[neighbour_index];
        
        //log ops.
        log_add(1);
        log_mult(1);
        log_my_add(node, 1);
        log_my_mult(node, 1);
        
        if(!used[this_neighbour->normal_neighbours[j]]){
          used[this_neighbour->normal_neighbours[j]] = true;
          (*num_used)++;
        }
      }

      //carry over this node's offset.
      (*offset) += this_node->link_size[neighbour_index] * this_neighbour->premult_offset;
      
      //log ops.
      log_add(1);
      log_mult(1);
      log_my_add(node, 1);
      log_my_mult(node, 1);


    }
    
    //depth_new
    depth_gauge_new(6,2,0); //consolidate.

  }

  return;

}

int sn_gather_recursive(int home, bool * visited, int this_node, RealNumber voltage, RealNumber * grand_sigma_g, bool summed){

  int i, my_cursor;
  int maxdepth;
  
  //printf("@ node %d:\n", this_node);
  
  if(visited[this_node]){ }//do nothing.
  else{
  
    //mark as visited.
    visited[this_node] = true;

    //if gi_inputs is true:
    if(nodes[this_node].gi_inputs){
      
      //printf("  grand sigma g = %f\n", (*grand_sigma_g));
      
      if(!summed){
        (*grand_sigma_g) += nodes[this_node].my_sigma_G;
        //printf("  grand sigma g + %f = %f \n",nodes[this_node].my_sigma_G, (*grand_sigma_g));
        
        //log ops.
        log_add(1);
        log_my_add(this_node, 1);
        
      }
      nodes[home].sn_internals += voltage * nodes[this_node].my_sigma_G;
      
      //log ops.
      log_add(1);
      log_mult(1);
      log_my_add(this_node, 1);
      log_my_mult(this_node, 1);
      
      //depth_new
      depth_gauge_new(3,1,0);//we'll add 1 on later for the multiply step.
      
    }
    
    //then throw sn_gather_recursive at every node it's connected to VIA A VSOURCE.
    //(it'll bounce back from visited nodes.)
    
    my_cursor = nodes[this_node].cursor;
    
    for(i=0; i<my_cursor; i++){      
      sn_gather_recursive(home, visited, nodes[this_node].neighbours[i], (voltage+nodes[this_node].hop_size[i]), grand_sigma_g, summed);
      
      //log ops.
      log_add(1);
      log_my_add(nodes[this_node].neighbours[i], 1);
      
    }
  }
  
  return 0;
  
}


void static_walk_sn(int home, bool * visited, int this_node, bool is_new, int label){
  
  int i, my_cursor;
  int * temp_array;
  
  if(visited[this_node]){ }//do nothing.
  else{
  
    //mark as visited.
    visited[this_node] = true;
    
    //apply the label, if appropriate:
    if(is_new){
      nodes[this_node].supernode_index = label;

    }
    
    //if gi_inputs is true:
    if(nodes[this_node].gi_inputs && this_node != home){

      //let the home node know that this_node is a connected external node in its supernode:
      //if array isn't allocated yet, do so.
      if(!nodes[home].psn_built){
        nodes[home].p_neighbours=(int*)malloc(sizeof(int)*(1)); 
        nodes[home].p_cursor = 1;
        nodes[home].psn_built = true;
        
        nodes[home].p_neighbours[0] = this_node;

      }
      
      //if array *is* allocated, we will need to re-make it with the new node.
      
      else{
        my_cursor = nodes[home].p_cursor;
        
        //first, check if this chap has already been linked in.
        for(i=0; i<my_cursor; i++){
          if(nodes[home].p_neighbours[i] == this_node){
            break;
          }
        }
        
        //if we haven't found it...
        if(i == my_cursor){

          //increment cursor.
          my_cursor++;

          //allocate temporary storage.
          temp_array = (int*)malloc(sizeof(int) * my_cursor);
          
          //copy existing stuff to temp
          for(i=0; i<(my_cursor-1); i++){
            temp_array[i] = nodes[home].p_neighbours[i];
          }
          
          //add in new guy
          temp_array[i] = this_node;
          
          //free and re-allocate space
          free(nodes[home].p_neighbours);
          nodes[home].p_neighbours=(int*)malloc(sizeof(int)*(my_cursor));
          
          //copy back into p_neighbours.
          for(i=0; i<(my_cursor); i++){
            nodes[home].p_neighbours[i] = temp_array[i];
          }
          
          //save new cursor.
          nodes[home].p_cursor = my_cursor;
          
          //deallocate temps
          free(temp_array);
          
        }

      }
      
    }
    
    //then throw static_walk_sn at every node it's connected to VIA A VSOURCE.
    //(it'll bounce back from visited nodes.)
    
    my_cursor = nodes[this_node].cursor;
    
    for(i=0; i<my_cursor; i++){
      static_walk_sn(home, visited, nodes[this_node].neighbours[i], is_new, label);
    }
  }
  
  return;
  
}



/* This function is to be called within the matfree iteration loop;
 * it zeroes the sigma_GV entries.
 */

void zero_sigmas( void ){
  
  int next_node;
  
  for(next_node = 0; next_node<=num_nodes_matfree; next_node++){
    nodes[next_node].my_sigma_GV = 0;
  }
  
  return;
}

/* this function actually does more than zero the sigmas: it also zeroes
 * the links between a node and each of its neighbours, ready for the
 * next model-eval'd circuit. Sigma_G, Sigma_I, and Sigma_GV are also zeroed.
 */

void clear_jotter( void ){
  
  int next_node, i;
  
  for(next_node = 0; next_node<=num_nodes_matfree; next_node++){
    nodes[next_node].my_I_sum = 0;
    nodes[next_node].my_sigma_G = 0;
    nodes[next_node].my_sigma_GV = 0;
    nodes[next_node].sn_internals = 0;
    nodes[next_node].sn_grand_gsum = 0;
    nodes[next_node].premult_offset = 0;
    nodes[next_node].my_offset = 0;
    nodes[next_node].premult_supernode_resolved = false;

    for(i=0; i<nodes[next_node].cursor; i++){
      nodes[next_node].hop_size[i] = 0;
    }
    for(i=0; i<nodes[next_node].g_cursor; i++){
      nodes[next_node].link_size[i] = 0;
    }
    
      if(nodes[next_node].con_built){
        free(nodes[next_node].normal_neighbours);
        free(nodes[next_node].link_size);
        nodes[next_node].g_cursor = 0;
        nodes[next_node].con_built = false;
      }

  }
  
  return;  
}

/* This function is used by the various *devload.c functions.
 * Whenever a device is loaded in, we can change properties of
 * the node.
 * The first five booleans tell the function which properties we
 * want to switch,
 * and the second set of five booleans are their new values.
 */

void update_node_properties( int index,
                           bool update_supernode,
                           bool update_static_tree,
                           bool update_external,
                           bool update_ground_neighbour,
                           bool update_gi_inputs,
                           bool update_v_inputs,
                           
                           bool is_supernode,
                           bool is_static_tree,
                           bool is_external,
                           bool is_ground_neighbour,
                           bool has_gi_inputs,
                           bool has_v_inputs ){

  if((index < 0) || (index > num_nodes_matfree)){
    printf("debug: a device has attempted to update node %d,", index);
    printf("debug: which does not exist. Aborting...");
    abort();
  }

  else{
    if(update_supernode){
      nodes[index].supernode = is_supernode;
    }
    if(update_static_tree){
      nodes[index].static_tree = is_static_tree;
    }
    if(update_external){
      nodes[index].external = is_external;
    }
    if(update_ground_neighbour){
      nodes[index].ground_neighbour = is_ground_neighbour;
    }
    if(update_gi_inputs){
      nodes[index].gi_inputs = has_gi_inputs;
    }
    if(update_v_inputs){
      nodes[index].v_inputs = has_v_inputs;
    }
  }
  return;
}

/* This function is used by the various *devload.c functions.
 * Whenever a device is loaded in, we can - where appropriate -
 * add its value to the sigma_G at each node it is connected to.
 */

void sigma_G_add( int index, int neighbour, RealNumber G_in ){

  //rintf("  Sigma_G_add():: node %d is connected to node %d by conductance %f\n", index, neighbour, G_in);

  int i, my_cursor;
  int * temp_indices;
  RealNumber * temp_links;

  if((index < 0) || (index > num_nodes_matfree)){
    printf("debug: a device has attempted to update node %d,", index);
    printf("debug: which does not exist. Aborting...");
    abort();
  }
  
  if((neighbour < 0) || (neighbour > num_nodes_matfree)){
    printf("debug: a device has attempted to connect node %d to node %d", index, neighbour);
    printf("debug: the latter, sadly, does not exist. Aborting...");
    abort();
  }

  //if links array isn't allocated yet, do so.
  if(!nodes[index].con_built){
    
    //printf("                    no conductance array built at node %d. now building...\n", index);
    
    nodes[index].normal_neighbours=(int*)malloc(sizeof(int)*(1)); 
    nodes[index].link_size=(RealNumber*)malloc(sizeof(RealNumber)*(1));
    nodes[index].g_cursor = 1;
    nodes[index].con_built = true;
    
    nodes[index].normal_neighbours[0] = neighbour;
    nodes[index].link_size[0] = 0;
  }
  
  //search the list for neighbour.
  my_cursor = nodes[index].g_cursor;
  
  //printf("                    searching con array (%d elements) for node %d\n", my_cursor, neighbour);
  
  for(i=0; i<my_cursor; i++){
    
    //if we've found it, update it and break the loop.
    if(nodes[index].normal_neighbours[i] == neighbour){
      //printf("                    found node %d!\n", neighbour);
      //printf("                    existing linksize = %f\n", nodes[index].link_size[i]);
      nodes[index].link_size[i] += G_in;
      //printf("                    new linksize to node %d is %f\n", neighbour, nodes[index].link_size[i]);
      break;
    }

  }
  
  //if we didn't find it, time to make it.
  if(i==my_cursor){
    
    temp_indices=(int*)malloc(sizeof(int)*(my_cursor)); 
    temp_links=(RealNumber*)malloc(sizeof(RealNumber)*(my_cursor));
    
    //copy existing stuff to temp
    for(i=0; i<my_cursor; i++){
      temp_indices[i] = nodes[index].normal_neighbours[i];
      temp_links[i] = nodes[index].link_size[i];
    }
    
    //free and re-allocate space
    free(nodes[index].normal_neighbours);
    free(nodes[index].link_size);
    nodes[index].normal_neighbours=(int*)malloc(sizeof(int)*(my_cursor+1)); 
    nodes[index].link_size=(RealNumber*)malloc(sizeof(RealNumber)*(my_cursor+1));
    
    //copy stuff back
    for(i=0; i<my_cursor; i++){
      nodes[index].normal_neighbours[i] = temp_indices[i];
      nodes[index].link_size[i] = temp_links[i];
    }
    
    //add in new guy
    nodes[index].normal_neighbours[my_cursor] = neighbour;
    nodes[index].link_size[my_cursor] = G_in;
    
    //deallocate temps
    free(temp_indices);
    free(temp_links);
    
    //increment cursor
    nodes[index].g_cursor = my_cursor+1;
  }

  return;
}

void sigma_GV_add( int index, int neighbour, RealNumber G_in ){

  if((index < 0) || (index > num_nodes_matfree)){
    printf("debug: a device has attempted to update node %d,", index);
    printf("debug: which does not exist. Aborting...");
    abort();
  }
  
  if((neighbour < 0) || (neighbour > num_nodes_matfree)){
    printf("debug: a device has attempted to connect node %d to node %d", index, neighbour);
    printf("debug: the latter, sadly, does not exist. Aborting...");
    abort();
  }



  else{
    nodes[index].my_sigma_GV += (G_in * nodes[neighbour].my_V_old);
    //log_mult(1);
    //log_add(1);
  }

  return;
}

/* This function is used by the various *devload.c functions.
 * Whenever a device is loaded in, we can - where appropriate -
 * add its (current) value to the sigma_GV at each node it is
 * connected to.
 */

void sigma_I_add( int index, RealNumber I_in ){

  if((index < 0) || (index > num_nodes_matfree)){
    printf("debug: a device has attempted to update node %d,", index);
    printf("debug: which does not exist. Aborting...");
    abort();
  }

  else{
    nodes[index].my_I_sum += (I_in);
    //log_add(1);
    //log_my_add(index,1);
  }

  return;
}


/* This function is used by the various *devload.c functions.
 * Whenever a device is loaded in, we can - where appropriate -
 * add its value to the sigma_GV at each node it is connected to.
 */

void supernode_add( int index, int neighbour, RealNumber V_in ){

  int i, my_cursor;
  int * temp_indices;
  RealNumber * temp_hops;

  if((index < 0) || (index > num_nodes_matfree)){
    printf("debug: a device has attempted to update node %d,", index);
    printf("debug: which does not exist. Aborting...");
    abort();
  }

  if((neighbour < 0) || (neighbour > num_nodes_matfree)){
    printf("debug: a device has attempted to connect node %d to node %d", index, neighbour);
    printf("debug: the latter, sadly, does not exist. Aborting...");
    abort();
  }

  //if hops array isn't allocated yet, do so.
  if(!nodes[index].sn_built){
    nodes[index].neighbours=(int*)malloc(sizeof(int)*(1));
    nodes[index].hop_size=(RealNumber*)malloc(sizeof(RealNumber)*(1));
    nodes[index].cursor = 1;
    nodes[index].sn_built = true;

    nodes[index].neighbours[0] = neighbour;
  }


  //search the list for neighbour.
  my_cursor = nodes[index].cursor;
  for(i=0; i<my_cursor; i++){

    //if we've found it, update it and break the loop.
    if(nodes[index].neighbours[i] == neighbour){
      nodes[index].hop_size[i] = V_in;
      break;
    }

  }

  //if we didn't find it, time to make it.
  if(i==my_cursor){

    temp_indices=(int*)malloc(sizeof(int)*(my_cursor)); 
    temp_hops=(RealNumber*)malloc(sizeof(RealNumber)*(my_cursor));
    
    //copy existing stuff to temp
    for(i=0; i<my_cursor; i++){
      temp_indices[i] = nodes[index].neighbours[i];
      temp_hops[i] = nodes[index].hop_size[i];
    }
    
    //free and re-allocate space
    free(nodes[index].neighbours);
    free(nodes[index].hop_size);
    nodes[index].neighbours=(int*)malloc(sizeof(int)*(my_cursor+1)); 
    nodes[index].hop_size=(RealNumber*)malloc(sizeof(RealNumber)*(my_cursor+1));
    
    //copy stuff back
    for(i=0; i<my_cursor; i++){
      nodes[index].neighbours[i] = temp_indices[i];
      nodes[index].hop_size[i] = temp_hops[i];
    }
    
    //add in new guy
    nodes[index].neighbours[my_cursor] = neighbour;
    nodes[index].hop_size[my_cursor] = V_in;
    
    //deallocate temps
    free(temp_indices);
    free(temp_hops);
    
    //increment cursor
    nodes[index].cursor = my_cursor+1;
  }

    //printf("debug: node %d connected to node %d by voltage %f\n\n", index, nodes[index].neighbours[my_cursor], nodes[index].hop_size[my_cursor]);

  return;

}

/* The various performance measuring functions.
 */

void log_add( int num ){
  adds += num;
  total_ops += num;
  return;
}

void log_sub( int num ){
  subs += num;
  total_ops += num;
  return;
}

void log_mult( int num ){
  mults += num;
  total_ops += num;
  return;
}

void log_div( int num ){
  divs += num;
  total_ops += num;
  return;
}



void log_my_add( int index, int num ){
  nodes[index].my_adds += num;
  nodes[index].my_total_ops += num;
  return;
}

void log_my_sub( int index, int num ){
  nodes[index].my_subs += num;
  nodes[index].my_total_ops += num;
  return;
}

void log_my_mult( int index, int num ){
  nodes[index].my_mults += num;
  nodes[index].my_total_ops += num;
  return;
}

void log_my_div( int index, int num ){
  nodes[index].my_divs += num;
  nodes[index].my_total_ops += num;
  return;
}


//function to print personal performace
void print_personal( int iters ){
  
  FILE * fp;
  int sum, i;
  static int prev_step = -2;
  static int substep=1;
  static bool titled = false;
  static bool titled2 = false;
  
  int busiest_node;
  
  static bool tracker_built = false;
  static unsigned long long int * tracker_ops_per_node;
  static int total_tracker_ops_per_node;

  if(!tracker_built){
    tracker_ops_per_node = (int*) malloc(sizeof(int)*(num_nodes_matfree+1));
    tracker_built = true;
    
    for(i=1; i<=num_nodes_matfree; i++){
      tracker_ops_per_node[i]=0;
    }
    
  }
  
  if(prev_step != instance_number){
    prev_step = instance_number;
    substep = 1;
  }
  
  fp = fopen ("stats.csv","a");
  if(!titled){
    fprintf (fp, "step");
    for(i=0; i<=num_nodes_matfree; i++){
      fprintf (fp, ", node %d", matfree_Matrix->IntToExtRowMap[i]);
    }
    fprintf (fp, ", total\n");
    titled = true;
  }

  fprintf (fp, "%d.%d", prev_step, substep);
  
  for(i=0; i<=num_nodes_matfree; i++){
    tracker_ops_per_node[i] = nodes[i].my_total_ops - tracker_ops_per_node[i];
    fprintf (fp, ", %d", (int)tracker_ops_per_node[i]);
  }
  
  total_tracker_ops_per_node = total_ops - total_tracker_ops_per_node;
  
  fprintf (fp, ", %d", total_tracker_ops_per_node);
  
  fprintf (fp, "\n");
  
  fclose(fp);
  
  //now print out "critical path":
  
  //print step
  //then iterate over all nodes:
  //  -> whose personal adds have incremented the most since the last time we called this function?
  //  --> print that person's number of adds to the file.
  //do the same for subs, mults, divs.
  //add these four up, and print as a total number of operations.
  
  fp = fopen ("stats_by_op.csv","a");
  if(!titled2){
    fprintf (fp, "step, node, adds, subs, mults, divs, total\n");
    titled2 = true;
  }
  
  fprintf (fp, "%d.%d", prev_step, substep);
  
  busiest_node = 0;
  
  for(i=1; i<=num_nodes_matfree; i++){
    if(tracker_ops_per_node[i] > tracker_ops_per_node[busiest_node]){
      busiest_node = i;
    }
  }

  printf(", %d", matfree_Matrix->IntToExtRowMap[busiest_node] );
  printf(", %d", nodes[busiest_node].my_adds );
  printf(", %d", nodes[busiest_node].my_subs );
  printf(", %d", nodes[busiest_node].my_mults );
  printf(", %d", nodes[busiest_node].my_divs );
  printf(", %d", nodes[busiest_node].my_total_ops );
  
  printf("\n");
  fclose(fp);
  
  //ready for next time.
  substep++;
  return;
  
}

//A different slant on the whole print-out-stuff function. This one doesn't
//allocate any memory; instead, it resets the counters each time they are read.
//function to print personal performace
void print_personal_( int iters ){


  FILE * fp;
  int sum, i;
  static int prev_step = -2;
  static int substep=1;
  static bool titled = false;
  static bool titled2 = false;
  static bool titled3 = false;
  static bool titled4 = false;
  static bool titled5 = false;
  static bool t_ops_dt_comp = false;
  static bool t_ops_dt_indi = false;
  static bool t_ops_it_comp = false;
  static bool t_ops_it_indi = false;
  static bool t_depth_it_indi = false;
  static bool t_depth_it_comp = false;
  static bool c_ops_all = false;
  
  static int num_rows = 0;
  static RealNumber avg_sparsity_z = 0;
  static RealNumber avg_sparsity_nz = 0;
  static int largest_fanout = 0;
  static int largest_fanout_node = 0;
  static int largest_fanout_nz = 0;
  static int largest_fanout_node_nz = 0;

  int busiest_node;
  int stackheight;
  int next_node; //for iterating.

  static int iters_acc = 0;
  static int adds_acc = 0;
  static int subs_acc = 0;
  static int mults_acc = 0;
  static int divs_acc = 0;
  static int total_acc = 0;

  static int depth_1 = 0;
  static int depth_2 = 0;
  static int depth_3 = 0;
  static int depth_4 = 0;
  static int depth_5 = 0;
  static int depth_6 = 0;
  static int depth_7 = 0;
  static int depth_8 = 0;
  static int depth_9 = 0;
  static int depth_total = 0;

  static timestep = 0;
  static mod_mat_loop = 0;

  bool new_timestep = false;

  //update accumulators.
  iters_acc += iters;
  adds_acc += adds;
  subs_acc += subs;
  divs_acc += divs;
  mults_acc += mults;
  total_acc += total_ops;

  if(prev_step != instance_number){
    prev_step = instance_number;
    substep = 1;
    new_timestep = true;
    timestep++;
  }
  mod_mat_loop++;

  if(new_timestep){ //print per-timestep things.
    fp = fopen ("./../graphs/raw/ops_dt_comp.csv","a");
    if(!t_ops_dt_comp){
      fprintf (fp, "timestep, iters, adds, subs, mults, divs, total\n");
      t_ops_dt_comp = true;
    }
    
    fprintf (fp, "%d", timestep);
    fprintf (fp, ", %d, %d, %d, %d, %d, %d\n", iters_acc, adds_acc, subs_acc, mults_acc, divs_acc, total_acc);
    fclose(fp);

    fp = fopen ("./../graphs/raw/ops_dt_indi.csv","a");
    if(!t_ops_dt_indi){
      fprintf (fp, "timestep, iters, adds, subs, mults, divs, total\n");
      t_ops_dt_indi = true;
    }
    
    fprintf (fp, "%d", timestep);
    fprintf (fp, ", %d, %d, %d, %d, %d, %d\n", iters, adds, subs, mults, divs, total_ops);
    fclose(fp);
  }
  
  //print per-iter things.

  fp = fopen ("./../graphs/raw/ops_it_comp.csv","a");
  if(!t_ops_it_comp){
    fprintf (fp, "innerloop, iters, adds, subs, mults, divs, total\n");
    t_ops_it_comp = true;
  }
  
  fprintf (fp, "%d", mod_mat_loop);
  fprintf (fp, ", %d, %d, %d, %d, %d, %d\n", iters_acc, adds_acc, subs_acc, mults_acc, divs_acc, total_acc);
  fclose(fp);

  fp = fopen ("./../graphs/raw/ops_it_indi.csv","a");
  if(!t_ops_it_indi){
    fprintf (fp, "innerloop, iters, adds, subs, mults, divs, total\n");
    t_ops_it_indi = true;
  }
  
  fprintf (fp, "%d", mod_mat_loop);
  fprintf (fp, ", %d, %d, %d, %d, %d, %d\n", iters, adds, subs, mults, divs, total_ops);
  fclose(fp);
  
  fp = fopen ("./../graphs/raw/depth_it_indi.csv","a");
  if(!t_depth_it_indi){
    fprintf (fp, "innerloop,iters,build_static_trees,compute_static_sigmas,supernode_gather_terms,invert_sigmas,resolve_equations,matfree_premult,compute_sigmas (total),matfree_solve (total),handle_internals,total\n");
    t_depth_it_indi = true;
  }
  
  fprintf (fp, "%d", mod_mat_loop);
  fprintf (fp, ", %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n", iters,
           depth_gauge_new(1,-8,0),
           depth_gauge_new(2,-8,0),
           depth_gauge_new(3,-8,0),
           depth_gauge_new(4,-8,0),
           depth_gauge_new(5,-8,0),
           depth_gauge_new(6,-8,0),
           depth_gauge_new(7,-8,0),
           depth_gauge_new(8,-8,0),
           depth_gauge_new(9,-8,0),
           depth_gauge_new(10,-8,0)
           );
  fclose(fp);
  
  fp = fopen ("./../graphs/raw/depth_it_comp.csv","a");
  if(!t_depth_it_comp){
    fprintf (fp, "innerloop,iters,build_static_trees,compute_static_sigmas,supernode_gather_terms,invert_sigmas,resolve_equations,matfree_premult,compute_sigmas (total),matfree_solve (total),handle_internals,total\n");
    t_depth_it_comp = true;
  }
  
  depth_1 += depth_gauge_new(1,-8,0);
  depth_2 += depth_gauge_new(2,-8,0);
  depth_3 += depth_gauge_new(3,-8,0);
  depth_4 += depth_gauge_new(4,-8,0);
  depth_5 += depth_gauge_new(5,-8,0);
  depth_6 += depth_gauge_new(6,-8,0);
  depth_7 += depth_gauge_new(7,-8,0);
  depth_8 += depth_gauge_new(8,-8,0);
  depth_9 += depth_gauge_new(9,-8,0);
  depth_total += depth_gauge_new(10,-8,0);
  
  fprintf (fp, "%d", mod_mat_loop);
  fprintf (fp, ", %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n", iters,
           depth_1,
           depth_2,
           depth_3,
           depth_4,
           depth_5,
           depth_6,
           depth_7,
           depth_8,
           depth_9,
           depth_total
           );
  fclose(fp);

  
  //print overall stats.
  if(!c_ops_all){
    num_rows = num_nodes_matfree;

    //printf("Now finding sparsity and fanout.\n\n");

    for(next_node = 1; next_node<=num_nodes_matfree; next_node++){
      avg_sparsity_z += (RealNumber)nodes[next_node].g_cursor;
      avg_sparsity_nz += (RealNumber)nodes[next_node].g_cursor;
      //printf("  Node %d has a fanout of %d.\n", next_node, nodes[next_node].g_cursor);
      if(nodes[next_node].g_cursor > largest_fanout){
        largest_fanout = nodes[next_node].g_cursor;
        largest_fanout_node = next_node;
        //printf("  ...which is the new highest recorded fanout_z.\n    Setting largest_fanout to %d (from node %d)\n", nodes[next_node].g_cursor, next_node);
      }
      if(next_node !=0 && nodes[next_node].g_cursor > largest_fanout_nz){
          largest_fanout_nz = nodes[next_node].g_cursor;
          largest_fanout_node_nz = next_node;
          //printf("  ...which is the new highest recorded fanout_nz.\n    Setting largest_fanout_nz to %d (from node %d)\n", nodes[next_node].g_cursor, next_node);
      }

    }
    avg_sparsity_z += nodes[0].g_cursor;
    //printf("  Node 0 has a fanout of %d.\n", nodes[0].g_cursor);

    avg_sparsity_z = avg_sparsity_z/((RealNumber)num_nodes_matfree);
    avg_sparsity_nz = avg_sparsity_nz/((RealNumber)(num_nodes_matfree-1));

/*
    printf("\n  Final breakdown:\n\n");
    printf("  num_rows = %d\n", num_rows);
    printf("  avg_sparsity_z = %f\n", avg_sparsity_z);
    printf("  avg_sparsity_nz = %f\n", avg_sparsity_nz);
    printf("  largest fanout = %d (node %d)\n", largest_fanout, largest_fanout_node);
    printf("  largest fanout nz = %d (node %d)\n", largest_fanout_nz, largest_fanout_node_nz);

    print_nodes();

    matfree_pause();
*/

    c_ops_all = true;
  }
  
  fp = fopen ("./../graphs/raw/ops_overall.csv","w");

  fprintf (fp, "innerloop, iters, adds, subs, mults, divs, total, num_nodes, average sparsity (with 0), average sparsity (without 0), largest fanout (including 0), at node, largest fanout (not including 0), at node");
  fprintf (fp, ", build_static_trees_lin,build_static_trees_log,compute_static_sigmas_lin,compute_static_sigmas_log,supernode_gather_terms_lin,supernode_gather_terms_log,invert_sigmas_lin,invert_sigmas_log,resolve_equations_lin,resolve_equations_log,matfree_premult_lin,matfree_premult_log,compute_sigmas_lin,compute_sigmas_log,matfree_solve_lin,matfree_solve_log,handle_internals_lin,handle_internals_log");
  fprintf (fp, ", \n");
  
  fprintf (fp, "%d", mod_mat_loop);
  fprintf (fp, ", %d, %d, %d, %d, %d, %d, %d, %f, %f, %d, %d, %d, %d",
           iters_acc,
           adds_acc,
           subs_acc, 
           mults_acc, 
           divs_acc, 
           total_acc, 
           num_rows, 
           avg_sparsity_z, 
           avg_sparsity_nz,
           largest_fanout,
           largest_fanout_node,
           largest_fanout_nz,
           largest_fanout_node_nz
           );
           
  //now print depth stuff.
  fprintf (fp, ", %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d",
           (depth_gauge(4,1)),
           (depth_gauge(4,2)),
           (depth_gauge(5,1)),
           (depth_gauge(5,2)),
           (depth_gauge(6,1)),
           (depth_gauge(6,2)),
           (depth_gauge(7,1)),
           (depth_gauge(7,2)),
           (depth_gauge(8,1)),
           (depth_gauge(8,2)),
           (depth_gauge(9,1)),
           (depth_gauge(9,2)),
           (depth_gauge(10,1)),
           (depth_gauge(10,2)),
           (depth_gauge(11,1)),
           (depth_gauge(11,2)),
           (depth_gauge(12,1)),
           (depth_gauge(12,2))
           );
  
  fprintf (fp, "\n");
  fclose(fp);


///////////////////////////////
/*
  //printf("attempting to write out...\n");
  
  fp = fopen ("./../graphs/raw/stats.csv","a");
  //fp = fopen ("stats.csv","a");
  if(fp == NULL){
    printf("oh dear.\n"); abort();
  }
  
  if(!titled){
    fprintf (fp, "step");
    for(i=0; i<=num_nodes_matfree; i++){
      fprintf (fp, ", node %d", matfree_Matrix->IntToExtRowMap[i]);
    }
    fprintf (fp, ", total\n");
    titled = true;
  }

  fprintf (fp, "%d.%d", prev_step, substep);
  
  for(i=0; i<=num_nodes_matfree; i++){
    fprintf (fp, ", %d", nodes[i].my_total_ops);
  }
  
  fprintf (fp, ", %d", total_ops);
  
  fprintf (fp, "\n");
  
  fclose(fp);
  
  //now print out "critical path":
  
  //print step
  //then iterate over all nodes:
  //  -> whose personal adds have incremented the most since the last time we called this function?
  //  --> print that person's number of adds to the file.
  //do the same for subs, mults, divs.
  //add these four up, and print as a total number of operations.
  
  fp = fopen ("./../graphs/raw/stats_by_op.csv","a");
  //fp = fopen ("stats_by_op.csv","a");
  if(!titled2){
    fprintf (fp, "step, node, adds, subs, mults, divs, total\n");
    titled2 = true;
  }
  
  fprintf (fp, "%d.%d", prev_step, substep);
  
  busiest_node = 0;
  
  for(i=1; i<=num_nodes_matfree; i++){
    if(nodes[i].my_total_ops > nodes[busiest_node].my_total_ops){
      busiest_node = i;
    }
  }

  fprintf(fp, ", %d", matfree_Matrix->IntToExtRowMap[busiest_node] );
  fprintf(fp, ", %d", nodes[busiest_node].my_adds );
  fprintf(fp, ", %d", nodes[busiest_node].my_subs );
  fprintf(fp, ", %d", nodes[busiest_node].my_mults );
  fprintf(fp, ", %d", nodes[busiest_node].my_divs );
  fprintf(fp, ", %d", nodes[busiest_node].my_total_ops );
  
  fprintf(fp, "\n");
  fclose(fp);
  
  //now print out stacked graph.
  //
  //
  //
  
  fp = fopen ("./../graphs/raw/stacked_stats.csv","a");
  //fp = fopen ("stacked_stats.csv","a");
  if(!titled3){
    fprintf (fp, "step, node, divs, mults, subs, adds\n");
    titled3 = true;
  }
  
  fprintf (fp, "%d.%d", prev_step, substep);
  
  fprintf(fp, ", %d", matfree_Matrix->IntToExtRowMap[busiest_node] );
  stackheight = nodes[busiest_node].my_divs;
  fprintf(fp, ", %d", stackheight );
  stackheight += nodes[busiest_node].my_mults;
  fprintf(fp, ", %d", stackheight );
  stackheight += nodes[busiest_node].my_subs;
  fprintf(fp, ", %d", stackheight );
  stackheight += nodes[busiest_node].my_adds;
  fprintf(fp, ", %d", stackheight );
  
  
  if(stackheight != nodes[busiest_node].my_total_ops){ printf("\n\n  Something doesn't add up. Aborting...\n\n"); abort(); }
  
  fprintf(fp, "\n");
  fclose(fp);
  
  //this bit is printed out for the sake of monitoring the effect of premultiplication
  //on total operations over the duration of a run.


  fp = fopen ("./../graphs/raw/premult_effect.csv","a");
  //fp = fopen ("premult_effect.csv","a");
  if(!titled4){
    fprintf (fp, "step, node, adds, subs, mults, divs, total\n");
    titled4 = true;
  }
  
  fprintf (fp, "%d.%d", prev_step, substep);
  
  fprintf (fp, ", %d, %d, %d, %d, %d, %d", matfree_Matrix->IntToExtRowMap[busiest_node], adds, subs, mults, divs, total_ops);
  
  fprintf(fp, "\n");
  fclose(fp);

  //iters per outer iter:
  //
  //
  //
  //
  
  fp = fopen ("./../graphs/raw/big_totals.csv","a");
  //fp = fopen ("premult_effect.csv","a");
  if(!titled5){
    fprintf (fp, "step, iters, adds, subs, mults, divs, total\n");
    titled5 = true;
  }
  
  fprintf (fp, "%d.%d", prev_step, substep);
  
  fprintf (fp, ", %d, %d, %d, %d, %d, %d", iters, adds, subs, mults, divs, total_ops);
  
  fprintf(fp, "\n");
  fclose(fp);
  
*/

  //ready for next time.


  
  for(i=0; i<=num_nodes_matfree; i++){
    nodes[i].my_adds = 0;
    nodes[i].my_subs = 0;
    nodes[i].my_mults = 0;
    nodes[i].my_divs = 0;
    nodes[i].my_total_ops = 0;
  }
  adds = 0;
  subs = 0;
  divs = 0;
  mults = 0;
  total_ops = 0;

  substep++;



  return;

}





/* prints out node information.
 */

void print_nodes( void ){
  int i,j;
  
  for(i=0; i<=num_nodes_matfree; i++){
    printf("node %d:\n",i);
    printf("  V (old) = %f\n", nodes[i].my_V_old);
    printf("  sigma_G  = %f\n", nodes[i].my_sigma_G);
    printf("  sigma_GV = %f\n", nodes[i].my_sigma_GV);
    printf("  I_sum = %f\n", nodes[i].my_I_sum);
    printf("  is supernode: "); print_bool(nodes[i].supernode);
    if(nodes[i].supernode){
      printf(" (supernode number %d)", nodes[i].supernode_index);
      printf("\n    also in this supernode: ");
      for(j=0; j<nodes[i].p_cursor; j++){
        printf("%d ", nodes[i].p_neighbours[j]);
      }
      printf("\n    supernode sigma_g = %f", nodes[i].sn_grand_gsum);
      printf("\n    supernode internals = %f", nodes[i].sn_internals);
    }
    printf("\n  is static tree: "); print_bool(nodes[i].static_tree);
    printf("\n  is external: "); print_bool(nodes[i].external);
    printf("\n  is ground neighbour: "); print_bool(nodes[i].ground_neighbour);
    printf("\n  has gi inputs: "); print_bool(nodes[i].gi_inputs);
    printf("\n  has v inputs: "); print_bool(nodes[i].v_inputs);
    printf("\n");
    if(nodes[i].v_inputs){
      for(j=0; j<nodes[i].cursor; j++){
        printf("    connected to node %d by a hop of %f\n", nodes[i].neighbours[j], nodes[i].hop_size[j]);
      }
    }
      for(j=0; j<nodes[i].g_cursor; j++){
        printf("    connected to node %d by a g of size %f\n", nodes[i].normal_neighbours[j], nodes[i].link_size[j]);
      }
      printf("  offset = %f\n", nodes[i].premult_offset);
    printf("\n");
  }
  return;
}

/*prints booleans.*/
void print_bool( bool in ){
  if(in){printf("true");}
  else{printf("false");}
  return;
}

/*prints stats*/
void matfree_stat( void ){
  printf("adds: %d\n", adds);
  printf("subs: %d\n", subs);
  printf("mults: %d\n", mults);
  printf("divs: %d\n", divs);
}

void matfree_pause( void ){
  char fake[20];
  fgets(fake, 20, stdin);
  return;
}

/////////////////////////////////////////////////////////////////////////////////////
//  Hopefully, none of the below is necessary.
/////////////////////////////////////////////////////////////////////////////////////

/* This function takes the circuit structure, and checks if it's the
 * same one we were working on last time. If it's not (or it's new)
 * then we read it in and prepare the CSR arrays.
 * Returns 0 if it didn't do anything, or (size) if it actually had cause to
 * do some work.
 */

int readin_matrix( CKTcircuit * inckt , char * inmatrix ){
  
  int retval = 0;
  
  if(((MatrixPtr)inmatrix)->ID != last_ID){
    matfree_ckt = inckt;
    matfree_Matrix = (MatrixPtr) inmatrix;
    last_ID = matfree_Matrix->ID;
    int size = matfree_Matrix->Size;
    retval = size;
    
    ElementPtr pElement;    //used to temporarily store pointers to elements.
    int row=0;             //used to index the Ap array
    int index= 0;          //used to index the Ai array
    
    //Link rows.
    if(!matfree_Matrix->RowsLinked){
      spcLinkRows(matfree_Matrix);
    }
/*
    //allocate space for the CSR storage.
    matfree_ckt->Ap_matfree=malloc(sizeof(int)*(size+1));
    matfree_ckt->Ai_matfree=malloc(sizeof(int)*(matfree_Matrix->Elements));
    matfree_ckt->NonZeroElements_matfree=malloc(sizeof(ElementPtr)*(matfree_Matrix->Elements));
    matfree_ckt->NonZeroValues_matfree=malloc(sizeof(double)*(matfree_Matrix->Elements));

    //The first Ap value should point to the first Ai value, but we're not using Ap[0]...
    matfree_ckt->Ap_matfree[0] = 0;
    printf("\tAp[0] = 0\n");

    for(row=1;row<=size;row++) { // foreach row 1..N ordering in Sp1.3
      pElement = matfree_Matrix->FirstInRow[row];
      printf("debug : looking at row %d\n", row);
      matfree_ckt->Ap_matfree[row] = index;
      printf("debug : Ap[%d] = %d\n", row, index);

      while(pElement != NULL){
	      printf("debug : NextInRow is  @ (%d, %d)\n", pElement->Row, pElement->Col);
	      matfree_ckt->Ai_matfree[index] = pElement->Col;
	      matfree_ckt->NonZeroElements_matfree[index]=pElement;
	      printf("\tAi[%d] = %d\n", index, matfree_ckt->Ai_matfree[index]);
	      printf("\tData[%d] = (%d,%d)\n", index, matfree_ckt->NonZeroElements_matfree[index]->Row, matfree_ckt->NonZeroElements_matfree[index]->Col);

        if (position(5,4) == NULL){ printf("debug : (5,4) still null.\n"); }
        else{ printf("debug : (5,4) NOT NULL.\n"); }

	      index++;
	      pElement = pElement->NextInRow;
	    }
    }

*/

    printf("debug : finished the readin.\n");

    //Thus, a call to element (row, column) should search for (column) between Ai[Ap[row]] and Ai[Ap[row+1]].
    
    //Now get the size of the [G] submatrix.
    //Previously, we did this by finding the number of zero entries in b.
    //However, this is a very bad solution, as (any) isources and zero-value vsources will screw it up.
    //instead, we find the number of nodes read into the circuit, and subtract 1 (no row for node 0)
    //which is a much more reliable way of doing things.

    CKTnode * temp = &(matfree_ckt->CKTnodes[0]);
  
    printf("debug : temp found.\n");
  
    //normal rows seem to have type 3, vsource terms seem to have type 4.
    //yes, it's a horrible dirty hack.
/*
    while(temp->type != 4){ 
      //printf(" node number %d of type %d\n", temp->number, temp->type);
      temp = temp->next;    
    }
*/

    num_nodes_matfree = 0;
    
    while(temp != NULL){
      if(temp->type == 3){ num_nodes_matfree++; }
      temp = temp->next;
    }


    //num_nodes_matfree = (temp->number)-1;
    
    printf("debug : approximately %d nodes.\n", num_nodes_matfree);
    
    //build the node jotter.
    nodes=malloc(sizeof(node_jotter)*(num_nodes_matfree+1));

    printf("debug : malloc'd the node jotter.\n");

    //initialise and populate node jotter.
    for(row=1; row<=matfree_Matrix->Size; row++){
      //printf("debug : now building row %d.\n", row);
      build_node(row);
    }

    //for(row=1; row<=num_nodes_matfree; row++){
      //printf("   row %d:\n", row);
      //printf("           supernode: "); if(nodes[row].supernode == true){printf("true\n");} else{printf("false\n");}
      //printf("           internal: "); if(nodes[row].internal == true){printf("true\n");} else{printf("false\n");}
      //printf("           static tree: "); if(nodes[row].static_tree == true){printf("true\n");} else{printf("false\n");}
      //printf("           ground neighbour: "); if(nodes[row].ground_neighbour == true){printf("true\n");} else{printf("false\n");}
    //}
    
    //sort out static trees.
    //build_static_trees_old();

  }
  
  else{
    retval = 0;
  }
  
  printf("debug : returning %d...\n", retval);

  return retval;

}

/* this is our dimensionless solve,
 * hurrah!
 * returns 0 if converged, or -1 otherwise.
 */

int solve_matfree( void ){
  int i = 0;
  int weirdrow;
  int col;
  ElementPtr dummy;
  int retval = -1;
  bool * collisions;
  bool collisions_allocd = false;
  RealNumber vector_diff;
  RealNumber internal_voltage;
  
  for(i=1; i<=num_nodes_matfree; i++){ //for each node:
    //ascertain type: normal, fixed, or floating?
    weirdrow =  matfree_Matrix->ExtToIntRowMap[i];
    
    if (!nodes[i].supernode){ //normal.
      printf("  solve : row %d is normal. (node %d)\n",weirdrow, i);
      //printf("  solve : node %d is normal.\n", i);
      //printf("  solve : > row %d of weird matrix.\n", matfree_Matrix->ExtToIntRowMap[i]);
      //printf("  solve : > row %d is normal.\n",weirdrow);
      
      nodes[i].my_V = 0;
      nodes[i].my_sigma_G = 0;
      nodes[i].my_sigma_GV = 0;

      //printf("  solve : Sum: \n");
      for(col = 1; col <= matfree_Matrix->Size; col++){
        dummy = position_recol(weirdrow, col);
        if(dummy != NULL){
          if(dummy->Row == dummy->Col){
            nodes[i].my_sigma_G = dummy->Real;
          }
          else{
            //printf("  solve : > multiplying %.5f by %.0f .\n",dummy->Real, nodes[matfree_Matrix->IntToExtColMap[col]].my_V_old);
            //printf("  solve : ( %f * %f )  (voltage from row %d, node %d)\n", dummy->Real, nodes[matfree_Matrix->IntToExtColMap[dummy->Col]].my_V_old, dummy->Col, matfree_Matrix->IntToExtColMap[dummy->Col] );
            nodes[i].my_sigma_GV = nodes[i].my_sigma_GV + (dummy->Real * nodes[matfree_Matrix->IntToExtColMap[dummy->Col]].my_V_old);
          }
        }
      }
      //account for inverted polarity of nondiagonals.
      nodes[i].my_sigma_GV = nodes[i].my_sigma_GV * (-1);
      
      //new node voltage
      nodes[i].my_V = nodes[i].my_sigma_GV / nodes[i].my_sigma_G;
      
    }
    else if(nodes[i].static_tree){ //static tree.
      printf("  solve : row %d is part of a static tree (node %d)\n",weirdrow, i);
      //printf("  solve : node %d is static.\n", i);
      //printf("  solve : > row %d of weird matrix.\n", matfree_Matrix->ExtToIntRowMap[i]);
      //printf("  solve : > row %d is static.\n",weirdrow);
      nodes[i].my_V = 0;
      for(col = 0; col < (matfree_Matrix->Size - num_nodes_matfree); col++){
        nodes[i].my_V = nodes[i].my_V + (nodes[i].static_V_multipliers[col][0] * matfree_ckt->CKTrhs[col+num_nodes_matfree+1]);
      }
      //printf("  solve : > its voltage is %.0f.\n", nodes[i].my_V);
      
    }
    else if (!nodes[i].internal){//floating external node.
      printf("  solve : row %d is part of a floating tree (node %d)\n",weirdrow, i);
      printf("  solve : (it's also an external node.)\n",weirdrow);
      //printf("  solve : node %d is floating.\n", i);
      //printf("  solve : > row %d of weird matrix.\n", matfree_Matrix->ExtToIntRowMap[i]);
      //printf("  solve : > row %d is floating.\n",weirdrow);
      nodes[i].my_V = 0;
      nodes[i].my_sigma_G = 0;
      nodes[i].my_sigma_GV = 0;

      //printf("  solve : node %d is normal.\n", i);
      //printf("  solve : > row %d of weird matrix.\n", matfree_Matrix->ExtToIntRowMap[i]);

      if(collisions_allocd){
        free(collisions);
        collisions_allocd = false;
      }
      
      collisions = malloc(sizeof(bool)*(matfree_Matrix->Size+1));
      collisions_allocd = true;
      int tick;
      
      for(tick = 0; tick <=matfree_Matrix->Size; tick++){ collisions[tick] = false; }
      
      collect_floating_terms (weirdrow, 0, 0, collisions, i);
      
      //account for inverted polarity of nondiagonals.
      nodes[i].my_sigma_GV = nodes[i].my_sigma_GV * (-1);
      
      //new node voltage
      nodes[i].my_V = nodes[i].my_sigma_GV / nodes[i].my_sigma_G;
      printf("  solve : V = %f / %f\ = %f\n",nodes[i].my_sigma_GV, nodes[i].my_sigma_G,nodes[i].my_V);

    }
    else{//floating internal node.
      printf("  solve : row %d is part of a floating tree (node %d)\n",weirdrow, i);
      printf("  solve : (it's also an internal node.)\n",weirdrow);
    
      //new node voltage
      if(collect_internal_terms (weirdrow, 0, 0, i, &internal_voltage)){
        printf("  voltage found to be %f\n", internal_voltage);
        nodes[i].my_V = internal_voltage;
      }
      else{
        printf("  Oh dear, could not resolve internal nodes in floating tree...\n  aborting...");
        abort();
      }
    }
  }
  
  //cleanup.
  if(collisions_allocd){
    free(collisions);
    collisions_allocd = false;
  }
  
  //convergence check.
  vector_diff = 0;
  
  for(i=1; i<=num_nodes_matfree; i++){ //for each node:
    if(nodes[i].my_V > nodes[i].my_V_old){ vector_diff = vector_diff + nodes[i].my_V - nodes[i].my_V_old; }
    else{ vector_diff = vector_diff - nodes[i].my_V + nodes[i].my_V_old; }
    nodes[i].my_V_old = nodes[i].my_V;
  }
  
  if(vector_diff < CONV_TOL) { //we've converged.
    retval = 0;
  }
  
  return retval;
  
}

/* This function prints out the matrix.
 */

void print_matfree(void){
  int i,j;
  ElementPtr thisone;
  CKTnode * print_temp;
  int hop;

  printf(" MNA matrix (G): \n");
  
  for(i=1; i<=matfree_Matrix->Size; i++){
    printf(" | ");
    
    for(j=1; j<=matfree_Matrix->Size; j++){
      thisone = position(i,j);
      if(thisone == NULL){
        printf(" ⋅ ");
      }
      else{
        if (thisone->Real < 0){
          printf("%.0f ", thisone->Real);
        }
        else{
          printf(" %.0f ", thisone->Real);
        }
        
      }
    }
    
    printf("|\n");
  }
  
  printf(" reordered MNA matrix (G): \n");
  
  int newi, newj;
  for(newi=1; newi<=matfree_Matrix->Size; newi++){
    printf(" | ");
    i = matfree_Matrix->IntToExtRowMap[newi];
    
    for(newj=1; newj<=matfree_Matrix->Size; newj++){
      j = matfree_Matrix->IntToExtColMap[newj];
      thisone = position(i,j);
      if(thisone == NULL){
        printf(" ⋅ ");
      }
      else{
        if (thisone->Real < 0){
          printf("%.0f ", thisone->Real);
        }
        else{
          printf(" %.0f ", thisone->Real);
        }
        
      }
    }
        
    printf("|\n");
  }
    
  printf(" col-reordered MNA matrix (G): \n");
  
  
  for(i=1; i<=matfree_Matrix->Size; i++){
    printf(" | ");
    
    for(newj=1; newj<=matfree_Matrix->Size; newj++){
      j = matfree_Matrix->IntToExtColMap[newj];
      thisone = position(i,j);
      if(thisone == NULL){
        printf(" ⋅ ");
      }
      else{
        if (thisone->Real < 0){
          printf("%.0f ", thisone->Real);
        }
        else{
          printf(" %.0f ", thisone->Real);
        }
        
      }
    }

    print_temp = &(matfree_ckt->CKTnodes[0]);

    newi = matfree_Matrix->IntToExtRowMap[i];

    for(hop = 1; hop <= newi; hop++){
      print_temp = print_temp->next;
    }

    if(print_temp->type == 3){
      printf("|  (currents)\n");
    }
    else if(print_temp->type == 4){
      printf("|  (voltages)\n");
    }
  }
  
    printf(" col-reordered MNA matrix (G): \n");
  
  
  for(i=1; i<=matfree_Matrix->Size; i++){
    printf(" | ");
    
    for(j=1; j<=matfree_Matrix->Size; j++){
      thisone = position_recol(i,j);
      if(thisone == NULL){
        printf(" ⋅ ");
      }
      else{
        if (thisone->Real < 0){
          printf("%.6f ", thisone->Real);
        }
        else{
          printf(" %.6f ", thisone->Real);
        }
        
      }
    }

    print_temp = &(matfree_ckt->CKTnodes[0]);

    newi = matfree_Matrix->IntToExtRowMap[i];

    for(hop = 1; hop <= newi; hop++){
      print_temp = print_temp->next;
    }

    if(print_temp->type == 3){
      printf("|  (currents)\n");
    }
    else if(print_temp->type == 4){
      printf("|  (voltages)\n");
    }
  }
  
      printf(" symbolic MNA matrix (G): \n");
  
  int MNA_symbol;
  for(i=1; i<=matfree_Matrix->Size; i++){
    printf(" | ");
    
    for(j=1; j<=matfree_Matrix->Size; j++){
      MNA_symbol = recol_type(i,j);
      switch(MNA_symbol){
        case (MF_CONN): //it's a link
          printf("  + ");
          break;
        case (MF_G): //it's a conductance
          printf("  G ");
          break;
        case (MF_NC): //it's nothing
          printf("  ⋅ ");
          break;
      }
    }

    print_temp = &(matfree_ckt->CKTnodes[0]);

    newi = matfree_Matrix->IntToExtRowMap[i];

    for(hop = 1; hop <= newi; hop++){
      print_temp = print_temp->next;
    }

    if(print_temp->type == 3){
      printf("| \n");
    }
    else if(print_temp->type == 4){
      printf("|  (V)\n");
    }
  }

 
  
  ////now print the old b.
  //printf("\n");
  //printf(" old RHS vector (b): \n");
    
  //for(i=1; i<=matfree_Matrix->Size; i++){
    //if (matfree_ckt->CKTrhsOld[i] < 0){
      //printf(" | %.0f |\n", matfree_ckt->CKTrhsOld[i]);
    //}
    //else{
      //printf(" |  %.0f |\n", matfree_ckt->CKTrhsOld[i]);
    //}
  //}
  
  //now print the new b.
  printf("\n");
  printf(" new RHS vector (b): \n");
    
  for(i=1; i<=matfree_Matrix->Size; i++){
    if (matfree_ckt->CKTrhs[i] < 0){
      printf(" | %.0f |\n", matfree_ckt->CKTrhs[i]);
    }
    else{
      printf(" |  %.0f |\n", matfree_ckt->CKTrhs[i]);
    }
  }
  
  //print the reordered b
  RealNumber dummy;
  printf("\n");
  printf(" reordered RHS vector (b): \n");
  for(i=1;i<=matfree_Matrix->Size; i++) {
		dummy = matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[i]];
    if (dummy < 0){
      printf(" | %.6f |\n", dummy);
    }
    else{
      printf(" |  %.6f |\n", dummy);
    }
  }



  //Print out number of nodes.
  printf("\n");
  printf(" number of nodes: %d\n", matfree_ckt->CKTlastNode[0].number);
  
  CKTnode * temp = &(matfree_ckt->CKTnodes[0]);
  while(temp != NULL){
    printf(" node number %d of type %d (number %d)\n", temp->number, temp->type);
    temp = temp->next;    
  }


  return;
}

/* This function returns a pointer to the element at (row, column). If
 * you're after the value at this position, just use retval->Real.
 */

ElementPtr position( int row, int col ){

  ElementPtr retval = NULL;

    retval = matfree_Matrix->FirstInRow[row];
    while(retval != NULL){
      if (retval->Col == col){
        break;
      }
      else{
        retval = retval->NextInRow;
      }
    }

  return retval;

}

/* This function returns a pointer to the element at (row, column);
 * assuming we're using the column-reordered matrix.
 */

ElementPtr position_recol( int row, int col ){

  ElementPtr retval = NULL;
  int newcol = matfree_Matrix->IntToExtColMap[col];

    retval = matfree_Matrix->FirstInRow[row];
    while(retval != NULL){
      if (retval->Col == newcol){
        break;
      }
      else{
        retval = retval->NextInRow;
      }
    }

  return retval;
}

/* This function informs you as to whether the element at position (i,j)
 * in the reordered-column matrix is a conductance, a +-1, or null.
 * It returns 1 if the element is a connection, 0 if the element is a
 * conductance, and -1 if the element is not filled.
 */

int recol_type( int inrow, int incol ){

  ElementPtr dummy = position_recol(inrow, incol);
  int retval;
  
  if(dummy == NULL){
    retval = MF_NC; //-1 indicates nothing here [.]
  }
  else{
    if(dummy->Row <= dummy->Col){
      dummy = position(dummy->Col, dummy->Col);
    }
    else{
      dummy = position(dummy->Row, dummy->Row);
    }
    
    if(dummy == NULL){
      retval = MF_CONN; //1 indicates that this is a connection [+-1]
    }
    else{
      retval = MF_G; //0 indicates that this is a conductance.
    }
    
  }
  return retval;
}


/* This function is only used by readin_matrix; it identifies and builds
 * supernodes, but does not detect static trees.
 */

void build_node ( int index_in ){

  int this_type;
  CKTnode * build_temp = &(matfree_ckt->CKTnodes[0]);
  int col, row;
  RealNumber sum_col;
  ElementPtr dummy;
  int num_conns = 0, num_gs = 0, num_total = 0; //for gathering row statistics
  int index = matfree_Matrix->IntToExtRowMap[index_in];

  //initialise everything
  nodes[index].supernode = false;
  nodes[index].static_tree = false;
  nodes[index].internal = false;
  nodes[index].ground_neighbour = false;
  nodes[index].gi_inputs = false;
  nodes[index].v_inputs = false;
  nodes[index].my_V_old = 0;
  nodes[index].supernode_index = -1;

  //first: what type of row is this?
  //is it a sum of currents, or of voltages?
  for(col = 1; col <= index; col++){
    build_temp = build_temp->next;
  }
  this_type = build_temp->type;
  
  if(this_type == 4){ // voltage node.
    printf("  building a Voltage node on row %d \n", index);
  }

  else if(this_type == 3){ // current node.
    printf("  building a Current node on row %d - saving at node[%d] \n", index_in, index);

    //or, if no connections, then node is a neighbour to ground if its row doesn't sum to 0.
    //collect row stats:
    for(col = 1; col <= matfree_Matrix->Size; col++){
      switch(recol_type(index_in,col))
      {
        case(MF_CONN): //connection
        printf("    %dth entry is a connection \n", col);
          num_conns = num_conns + 1;
          num_total = num_total + 1;
          //if any CONNECTION is singular in its row, then this node is a neighbour to ground.
          sum_col = 0;
          printf("    summing the following entries in column %d : ", col);
          for(row = 1; row <= matfree_Matrix->Size; row++){
            dummy = position_recol(row, col);
            if(dummy != NULL){
              printf("%d ", row);
              sum_col = sum_col + dummy->Real;
            }
          }
          printf(" \n", row);
          if(sum_col != 0) { nodes[index].ground_neighbour = true; printf("detected ground neighbour! \n ", row);}
          break;
          
        case(MF_G): //conductance
        printf("    %dth entry is a g \n", col);
          num_gs = num_gs + 1;
          num_total = num_total + 1;
          break;
          
        case(MF_NC): //no element
        printf("    %dth entry is empty \n", col);
          break;

        default:
          printf("  undefined element type %d. Aborting... \n", recol_type(index,col));
          break;
      }
    }
    
    //finally, check if there's any current sources attached.
    
    //if any entries on the row are CONNECTIONS, then it's a supernode.
    //we actually allocate more memory here than we need - come back and change this.
    //(static trees only use column [0] of this square array)
    //(floating supernodes all share the same matrix)
    if(num_conns > 0) { 
      nodes[index].supernode = true;
      nodes[index].static_V_multipliers = (RealNumber**)malloc(sizeof(RealNumber*)*(matfree_Matrix->Size - num_nodes_matfree));
      for(col = 0; col < (matfree_Matrix->Size - num_nodes_matfree); col++){
        nodes[index].static_V_multipliers[col] = (RealNumber*)malloc(sizeof(RealNumber)*(matfree_Matrix->Size - num_nodes_matfree));
      }
      //n vsources will never make supernodes of more than n+1 nodes.
      nodes[index].floating_sigma_G = (ElementPtr*)malloc(sizeof(ElementPtr)*(matfree_Matrix->Size - num_nodes_matfree));
    }
    
    //if ALL entries on the row are CONNECTIONS, then it's an internal node.
    if(num_conns == num_total) { nodes[index].internal = true; }
    
    //we could also have a bash at determining whether non-internal nodes are connected to GND
    //by summing all elements on their row and checking whether or not the total is zero.
    //however, this doesn't serve any useful purpose, and is a bit temperamental due to changing
    //g values, so forget it for now.
    
      printf("   row %d (node %d):\n", index_in, index);
      printf("           supernode: "); if(nodes[index].supernode == true){printf("true\n");} else{printf("false\n");}
      printf("           internal: "); if(nodes[index].internal == true){printf("true\n");} else{printf("false\n");}
      printf("           static tree: "); if(nodes[index].static_tree == true){printf("true\n");} else{printf("false\n");}
      printf("           ground neighbour: "); if(nodes[index].ground_neighbour == true){printf("true\n");} else{printf("false\n");}
    
  }

  return;
}

/* This function is only used by readin_matrix; it uses the information
 * in build_node to build the static trees.
 * now depreciated.
 */
/*
void build_static_trees_old( void ){
  
  int noderow;        //the node we're looking at.
  int row;            //its corresponding row.
  int col;            //column iterator.
  int temprow;        //for temporarily looking at different rows.
  RealNumber sum_col; //for summing column entries.
  ElementPtr dummy;

  
  for(row = 1; row <=matfree_Matrix->Size; row++){
    noderow = matfree_Matrix->IntToExtRowMap[row];
    if(noderow > num_nodes_matfree){ } //skip.

    else{ // for current nodes:
      
      if(nodes[noderow].ground_neighbour == true && nodes[noderow].supernode == true){ //for every node with a grounded neighbour
        printf("debug : row %d is a neighbour to ground.\n", row);
        
        //initialise static_V information.
        nodes[noderow].static_tree = true;
        for(col = 0; col < (matfree_Matrix->Size - num_nodes_matfree); col++){
          nodes[noderow].static_V_multipliers[col][0] = 0;
        }
        
        static_treewalker(row, 0);

      }
      
    }

  }
  return;
}
*/

/* This function is only used by fix_static_trees; it walks through
 * a tree, fixing node voltages as it goes.
 */

void static_treewalker( int thisrow, int prevrow ){
  
  int noderow = matfree_Matrix->IntToExtRowMap[thisrow];        //the node we're looking at.
  int row = thisrow;  //its corresponding row.
  int col;            //column iterator.
  int temprow;        //for temporarily looking at different rows.
  RealNumber sum_col; //for summing column entries.
  ElementPtr dummy;
  bool prev_node_found;
  int me_row, me_col;
  
  printf("debug : now considering row %d \n", thisrow);
  
  //print row.
  printf("  | ");
  for(col = 1; col <= matfree_Matrix->Size; col++){
    if(col == me_col){
      printf("  X ");
    }
    else if(recol_type(row,col) == MF_CONN){
      printf("  1 ");
    }
    else if(recol_type(row,col) == MF_G){
      printf("  G ");
    }
    else{
      printf("  . ");
    }
  }
  printf("  |\n");
  
  if(prevrow == 0){ printf("debug : grounded node.\n");
  //find the grounded connection.

    for(col = 1; col <=matfree_Matrix->Size; col++){
      
      if(recol_type(row,col) == MF_CONN){
        sum_col = 0;
        for(temprow=1; temprow<=matfree_Matrix->Size; temprow++){
          if(recol_type(temprow,col) == MF_CONN){
            sum_col = sum_col + (position_recol(temprow, col))->Real;
          }
        }
        if(sum_col != 0){
          //connection to ground identified at (row,col)
          printf("debug : %d is the column connecting it to ground.\n", col);
          break;
        }
      }
    }

  }
  else{ printf("debug : floating node.\n"); //non-grounded node.
  //copy node information.
  //initialise static_V information.
    nodes[noderow].static_tree = true;
    for(col = 0; col < (matfree_Matrix->Size - num_nodes_matfree); col++){
      nodes[noderow].static_V_multipliers[col][0] = nodes[matfree_Matrix->IntToExtRowMap[prevrow]].static_V_multipliers[col][0];
    }
    
    //find the connection to the previous row

    prev_node_found = false;
    
    for(col = 1; ((col <=matfree_Matrix->Size) && !prev_node_found); col++){
      
      printf("debug : now looking at (%d,%d) : %d \n", row, col, recol_type(row,col));
      
      if(recol_type(row,col) == MF_CONN){
        for(temprow=1; temprow<=matfree_Matrix->Size; temprow++){
          if(recol_type(temprow,col) == MF_CONN && temprow == prevrow){
            prev_node_found = true;
            printf("debug : %d is the column connecting it to previous.\n", col);
            break;
          }
          
        }
      }
    }
    if(prev_node_found){col = col-1;} //loop overshoots

  }
  
  //Now that we have found the connection to previous @ (row, col),
  //we look at the (voltage) element @ (col, row):
  printf("debug : link exists @ (%d,%d) in weird matrix\n", row, col);
  dummy = position_recol(row,col);
  if (dummy == NULL){ printf("debug : NULL\n"); }
  printf("debug : link exists @ (%d,%d) in original matrix\n", dummy->Row, dummy->Col);
  
  //now note down value of voltage hop.
  //if connection found at row j, then voltage value should be in b[j] (new b)
  printf("debug : connected to prev by voltage value %f\n", matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[dummy->Col]]);
  printf("debug : on line %d in the original matrix \n", matfree_Matrix->IntToExtRowMap[dummy->Col]);
  printf("debug : and thus on line %d in the array of voltages \n", matfree_Matrix->IntToExtRowMap[dummy->Col] - num_nodes_matfree - 1);
  //and multiplier = (j,i) (new matrix)
  printf("debug : multiplier of magnitude %f\n", position_recol(row,col)->Real);
  
  nodes[noderow].static_V_multipliers[(matfree_Matrix->IntToExtRowMap[dummy->Col] - num_nodes_matfree - 1)][0] = position_recol(row,col)->Real;
  
  me_row = row;
  me_col = col;
  
  //print row voltage multipliers.
  printf(" [ ");
  for(col = 0; col < (matfree_Matrix->Size - num_nodes_matfree); col++){
      printf(" %.0f ", nodes[noderow].static_V_multipliers[col][0]);
  }
  printf(" ]\n");

  
  if(nodes[noderow].internal){
    for(col = 1; col <= matfree_Matrix->Size; col++){
      if(col != me_col && recol_type(row,col) == MF_CONN){
        
        printf("debug : found connection at col %d \n",col);
        
        //find the other end of the connection
        for(temprow = 1; temprow <= matfree_Matrix->Size; temprow++){
          if(temprow != me_row && recol_type(temprow,col) == MF_CONN){
            printf("debug : now working on node %d \n\n",temprow);
            static_treewalker( temprow, me_row );
          }
        }

      }
    }
  }
  
  return;
}


/* This function is only used by readin_matrix; it uses the information
 * in build_node to build the floating trees.
 */

void build_floating_trees( void ){

  int noderow;          //the node we're looking at.
  int row;              //its corresponding row.
  int col;              //column iterator.
  int temprow;          //for temporarily looking at different rows.
  RealNumber sum_col;   //for summing column entries.
  ElementPtr dummy;


  for(row = 1; row <=matfree_Matrix->Size; row++){
    noderow = matfree_Matrix->IntToExtRowMap[row];
    if(noderow > num_nodes_matfree){ /*skip */ }

    else{ // for current nodes:
      
      if((!nodes[noderow].static_tree) && (nodes[noderow].supernode)){ //for every node in a floating tree
        printf("debug : row %d is a floating node.\n", row);
        
        //initialise static_V information.
        nodes[noderow].static_tree = true;
        for(col = 0; col <= (matfree_Matrix->Size - num_nodes_matfree); col++){
          for(temprow = 0; temprow <= (matfree_Matrix->Size - num_nodes_matfree); temprow++){
            nodes[noderow].static_V_multipliers[col][temprow] = 0;
            nodes[noderow].floating_sigma_G[col] = NULL;
          }
        }

        floating_treewalker(row, 0);

      }
      
    }

  }
  return;
}

/* this guy builds floating trees a.k.a. supernodes. */

void floating_treewalker( int thisrow, int prevrow ){
  
  int noderow = matfree_Matrix->IntToExtRowMap[thisrow];        //the node we're looking at.
  int row = thisrow;  //its corresponding row.
  int col;            //column iterator.
  int temprow;        //for temporarily looking at different rows.
  RealNumber sum_col; //for summing column entries.
  ElementPtr dummy;
  bool prev_node_found;
  int me_row, me_col;
  
  printf("debug : now considering row %d \n", thisrow);
  
  //print row.
  printf("  | ");
  for(col = 1; col <= matfree_Matrix->Size; col++){
    if(col == me_col){
      printf("  X ");
    }
    else if(recol_type(row,col) == MF_CONN){
      printf("  1 ");
    }
    else if(recol_type(row,col) == MF_G){
      printf("  G ");
    }
    else{
      printf("  . ");
    }
  }
  printf("  |\n");
  
  if(prevrow == 0){ printf("debug : grounded node.\n");
  //find the grounded connection.

    for(col = 1; col <=matfree_Matrix->Size; col++){
      
      if(recol_type(row,col) == MF_CONN){
        sum_col = 0;
        for(temprow=1; temprow<=matfree_Matrix->Size; temprow++){
          if(recol_type(temprow,col) == MF_CONN){
            sum_col = sum_col + (position_recol(temprow, col))->Real;
          }
        }
        if(sum_col != 0){
          //connection to ground identified at (row,col)
          printf("debug : %d is the column connecting it to ground.\n", col);
          break;
        }
      }
    }

  }
  else{ printf("debug : floating node.\n"); //non-grounded node.
  //copy node information.
  //initialise static_V information.
    nodes[noderow].static_tree = true;
    for(col = 0; col < (matfree_Matrix->Size - num_nodes_matfree); col++){
      nodes[noderow].static_V_multipliers[col][0] = nodes[matfree_Matrix->IntToExtRowMap[prevrow]].static_V_multipliers[col][0];
    }
    
    //find the connection to the previous row

    prev_node_found = false;
    
    for(col = 1; ((col <=matfree_Matrix->Size) && !prev_node_found); col++){
      
      printf("debug : now looking at (%d,%d) : %d \n", row, col, recol_type(row,col));
      
      if(recol_type(row,col) == MF_CONN){
        for(temprow=1; temprow<=matfree_Matrix->Size; temprow++){
          if(recol_type(temprow,col) == MF_CONN && temprow == prevrow){
            prev_node_found = true;
            printf("debug : %d is the column connecting it to previous.\n", col);
            break;
          }
          
        }
      }
    }
    if(prev_node_found){col = col-1;} //loop overshoots

  }
  
  //Now that we have found the connection to previous @ (row, col),
  //we look at the (voltage) element @ (col, row):
  printf("debug : link exists @ (%d,%d) in weird matrix\n", row, col);
  dummy = position_recol(row,col);
  if (dummy == NULL){ printf("debug : NULL\n"); }
  printf("debug : link exists @ (%d,%d) in original matrix\n", dummy->Row, dummy->Col);
  
  //now note down value of voltage hop.
  //if connection found at row j, then voltage value should be in b[j] (new b)
  printf("debug : connected to prev by voltage value %f\n", matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[dummy->Col]]);
  printf("debug : on line %d in the original matrix \n", matfree_Matrix->IntToExtRowMap[dummy->Col]);
  printf("debug : and thus on line %d in the array of voltages \n", matfree_Matrix->IntToExtRowMap[dummy->Col] - num_nodes_matfree - 1);
  //and multiplier = (j,i) (new matrix)
  printf("debug : multiplier of magnitude %f\n", position_recol(row,col)->Real);
  
  nodes[noderow].static_V_multipliers[(matfree_Matrix->IntToExtRowMap[dummy->Col] - num_nodes_matfree - 1)][0] = position_recol(row,col)->Real;
  
  me_row = row;
  me_col = col;
  
  //print row voltage multipliers.
  printf(" [ ");
  for(col = 0; col < (matfree_Matrix->Size - num_nodes_matfree); col++){
      printf(" %.0f ", nodes[noderow].static_V_multipliers[col][0]);
  }
  printf(" ]\n");

  
  if(nodes[noderow].internal){
    for(col = 1; col <= matfree_Matrix->Size; col++){
      if(col != me_col && recol_type(row,col) == MF_CONN){
        
        printf("debug : found connection at col %d \n",col);
        
        //find the other end of the connection
        for(temprow = 1; temprow <= matfree_Matrix->Size; temprow++){
          if(temprow != me_row && recol_type(temprow,col) == MF_CONN){
            printf("debug : now working on node %d \n\n",temprow);
            static_treewalker( temprow, me_row );
          }
        }

      }
    }
  }
  
  return;
}

//used on line 268.

void collect_floating_terms( int normalrow, int from, RealNumber Vtotal, bool * visited, int node ){
  
  ElementPtr dummy;
  int col;
  int row;
  RealNumber voltagesum;
  
  int weirdrow = matfree_Matrix->ExtToIntColMap[normalrow];
  //printf("the diagonal in this row is in column %d (originally) and %d (weird)\n", normalrow, weirdrow);


  //do nothing if we've already been here.
  if(visited[normalrow]){ /*printf("Visited this row already - doing nothing. \n", normalrow );*/ }
  else{
    visited[normalrow] = true;
    
    
    
      //print row.
  printf("  | ");
  for(col = 1; col <= matfree_Matrix->Size; col++){
    if(recol_type(normalrow,col) == MF_CONN){
      printf("  1 ");
    }
    else if(recol_type(normalrow,col) == MF_G){
      printf("  G ");
    }
    else{
      printf("  . ");
    }
  }
  printf("  |\n");
    
    
    
    
    //if it's an external node, we'll need to add it's partials and conductances
    //to the sum of each at the supernode.
    if(!nodes[matfree_Matrix->IntToExtRowMap[normalrow]].internal){
    printf("  this row is row %d (node %d), and it is external.\n", normalrow, matfree_Matrix->IntToExtRowMap[normalrow]);
    printf("  voltage sum at this node: %f\n", Vtotal);
    
      //add SigmaG to the SigmaG for the supernode, and add all the partials too.
      for(col = 1; col <= matfree_Matrix->Size; col++){
        if(recol_type(normalrow,col) == MF_G){    //if this element is a G,
          dummy = position_recol(normalrow, col); //grab it!
          if(dummy->Row == dummy->Col){
            //add G to SigmaG for the supernode
            printf("          > SigmaG += %.5f \n",dummy->Real);
            nodes[node].my_sigma_G = nodes[node].my_sigma_G + dummy->Real;
            
            //subtract internals from SigmaVG
            printf("          > SigmaGV += %.5f * %.5f (internals)\n",dummy->Real, Vtotal);
            nodes[node].my_sigma_GV = nodes[node].my_sigma_GV +(dummy->Real * Vtotal);
          }
          else{
            printf("          > SigmaGV += %.5f * %.5f (externals)\n",dummy->Real, nodes[matfree_Matrix->IntToExtColMap[dummy->Col]].my_V_old);
            nodes[node].my_sigma_GV = nodes[node].my_sigma_GV + (dummy->Real * nodes[matfree_Matrix->IntToExtColMap[dummy->Col]].my_V_old);
          }
        }
      }
      
      //We should also add on currents at this point...

    }
    
    //be  external or internal, we will want to subtract its internals from sigma_VG.
    //dummy = position(normalrow,normalrow);
    //if(dummy != NULL){
      //printf("          > SigmaGV -= %.5f * %.0f \n",dummy->Real, Vtotal);
      //nodes[node].my_sigma_GV = nodes[node].my_sigma_GV -(dummy->Real * Vtotal);
    //}
    
    //now figure out where we're going next. (Could possibly integrate this into the above
    //to prevent having to iterate over the row twice - above, we hunt for G's; here, we hunt
    //for conns.)

    for(col = 1; col <= matfree_Matrix->Size; col++){
      if(recol_type(normalrow,col) == MF_CONN){    //if this element is a connection,
        //find the row that its other end lives on.
        for(row = 1; row <= matfree_Matrix->Size; row++){
          if((recol_type(row,col) == MF_CONN) && (row != normalrow)){  //the other end,

            dummy = position_recol(row,col);
            voltagesum = Vtotal + (dummy->Real * matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[col]]); 
            printf("          >> connected to row %d by voltage  %.0f \n",row, (dummy->Real * matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[col]]));
            collect_floating_terms(row, normalrow, voltagesum, visited, node);
            
          }
        }
      }
    }
    
    
  }
  return; 
}

//this only works for the assumption that supernodes are TREES. No cycles, please.

bool collect_internal_terms( int normalrow, int from, RealNumber Vtotal, int node, RealNumber * result){
  int col;
  int row;
  bool external_found = false; //indicates that we've reached the edge of the supernode somewhere.
  ElementPtr dummy;
  
  //print row.
      //printf("  | ");
      //for(col = 1; col <= matfree_Matrix->Size; col++){
        //if(recol_type(normalrow,col) == MF_CONN){
          //printf("  1 ");
        //}
        //else if(recol_type(normalrow,col) == MF_G){
          //printf("  G ");
        //}
        //else{
          //printf("  . ");
        //}
      //}
      //printf("  |\n");
  
  //if we have reached an external node, awesome.
  if(!nodes[matfree_Matrix->IntToExtRowMap[normalrow]].internal){
    printf("  found an external node for this supernode: node %d (row %d)\n", matfree_Matrix->IntToExtRowMap[normalrow], normalrow);
    printf("  the resulting voltage is %f + %f\n", Vtotal, nodes[matfree_Matrix->IntToExtRowMap[normalrow]].my_V_old);
    *result = Vtotal + nodes[matfree_Matrix->IntToExtRowMap[normalrow]].my_V_old;
    external_found = true;
  }
  
  //if not, check each connected row.
  else{
    //printf("  found an internal node for this supernode: node %d (row %d)\n", matfree_Matrix->IntToExtRowMap[normalrow], normalrow);
    for(col = 1; (col <= matfree_Matrix->Size), !external_found; col++){
      //printf("  looking in col %d\n", col);
      //for each entry in the row that's a connection;
      if(recol_type(normalrow,col) == MF_CONN){//printf("  it's a connection.\n");
        //find the other end
        for(row = 1; (row <= matfree_Matrix->Size) && (!external_found); row++){
          //printf("  looking in row %d of %d\n", row, matfree_Matrix->Size);
          //if this element is the other end,
          if(recol_type(row,col) == MF_CONN && row != normalrow && row != from){

            dummy = position_recol(normalrow,col);
            
            if(dummy == NULL ){ printf("  well there's your problem...\n"); abort(); }
            
            //printf("  checking node %d (row %d)\n", matfree_Matrix->IntToExtRowMap[row], row);
            
            external_found = collect_internal_terms( row, normalrow, (Vtotal + (dummy->Real * matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[dummy->Col]])), node, result);

          }
        }
      }
    }
  }
  
  return external_found;

}

void matfree_print_sol(void){
  
  int i;

  
  for(i=1; i<=num_nodes_matfree; i++){
    if (nodes[i].my_V_old < 0){
      printf(" | %.5f  |   (row %d)\n", nodes[i].my_V_old, matfree_Matrix -> ExtToIntColMap[i]);
    }
    else{
      printf(" |  %.5f  |   (row %d)\n", nodes[i].my_V_old, matfree_Matrix -> ExtToIntColMap[i]);
    }
  }
}

/* This function is for testing the status of the g connections at a particular node at
 * any given instant. It's really only for debugging, and can be deleted and removed
 * once the premultiply is working.
 */
/*
void check_neighbours( int node ){
  
  int i;
  
  if (nodes[node].con_built){
    printf(" g values @ node %d:\n", node);
    for(i=0; i<nodes[node].g_cursor; i++){
      printf(" link to node %d = %f\n", nodes[node].normal_neighbours[i], nodes[node].link_size[i]);
    }
  }
  else{
    printf(" Node %d has no g allocated at this point. \n", node);
  }
  
  matfree_pause();
  
  return;
}
*/

/*
    int number;
    double ic;
    double nodeset;
    double *ptr;
    struct sCKTnode *next;
    unsigned int icGiven:1;
    unsigned int nsGiven:1;
*/


    ////////////////////////////////////////
    
    
  ////add the conductances here to your tally.
  //if(!visited[normalrow]){ printf("Haven't been to this node yet.\n");
    //visited[normalrow] = true;
    
    //for(col = 0; col <= matfree_Matrix->Size; col++){
      //if(!visited[col]){ printf(" -\n"); }
      //else { printf(" X\n"); }
    //}
    
    //if(!nodes[matfree_Matrix->IntToExtRowMap[normalrow]].internal){ printf(" it's not an internal node.\n");
      ////dummy = position_recol(normalrow, weirdrow);
      ////printf("adding %.5f\n", dummy->Real);
      ////nodes[node].my_sigma_G = nodes[node].my_sigma_G + dummy->Real;
      
      ////additionally, we also need to add its partials to nodes[node].my_sigma_G
    
      //for(col = 1; col <= matfree_Matrix->Size; col++){
        //dummy = position_recol(weirdrow, col);
        //if(dummy != NULL){ printf("entry at column %d\n", col);
          //if(dummy->Row == dummy->Col){
            //nodes[node].my_sigma_G = nodes[node].my_sigma_G + dummy->Real;
          //}
          //else{
            //printf("  solve : > multiplying %.5f by %.0f .\n",dummy->Real, nodes[matfree_Matrix->IntToExtColMap[col]].my_V_old);
            //nodes[node].my_sigma_GV = nodes[node].my_sigma_GV + (dummy->Real * nodes[matfree_Matrix->IntToExtColMap[col]].my_V_old);
          //}
        //}
      //}



      ////...
    
      ////plus currents
      
      ////...
      
    //}
    //else{ printf("it's an internal node.\n"); }
  //}

  ////update the Vtally.
  //printf("came here from %d\n", from);
    //if(from == 0) {} //no need to do anything at this point if we haven't come from anywhere.
    //else{
      //for(col = 1; col <=matfree_Matrix->Size; col++){
        //if(recol_type(normalrow,col) == MF_CONN){
          //for(row = 1; row <=matfree_Matrix->Size; row++){
            //if(recol_type(row,col) == MF_CONN){
              //if(row == from){
                //dummy = position_recol(normalrow,col);
                //Vtally = Vtally + (dummy->Real * matfree_ckt->CKTrhs[matfree_Matrix->IntToExtRowMap[col]]);
                //printf("Vtally = %f\n", Vtally);
              //} 
            //}
          //}
        //}
      //}
    //}


  ////if this is an external node, we need to subtract its internals from nodes[i].my_sigma_GV
  //if(!nodes[matfree_Matrix->IntToExtRowMap[normalrow]].internal){
    //dummy = position_recol(normalrow, weirdrow);
    //nodes[node].my_sigma_GV = nodes[node].my_sigma_GV - (Vtally * dummy->Real); 
  //}
  
  

  ////do the same for every connected node.
  //for(col = 1; col <=matfree_Matrix->Size; col++){
    //if(recol_type(normalrow,col) == MF_CONN){
      //for(row = 1; row <=matfree_Matrix->Size; row++){
        //if((recol_type(row,col) == MF_CONN) && (row != from) && (row != normalrow)){
          //printf("Visiting row %d (from row %d) \n", row, normalrow);
          //collect_floating_terms(row, normalrow, Vtally, visited, node);
        //}
      //}
    //}
  //}
  
  ////collect_floating_terms( ___, normalrow, Vtally, visited, node)
  
  //return;
//}



////For reference: the old way I implemented this:
//matfree_solver::matfree_solver( CKTcircuit * inckt ){
	
  //myckt = inckt;
  //eMatrix = (MatrixPtr)inckt->CKTmatrix;
	
  ////allocate space for the CSR storage.
  //myckt->Ap_matfree=malloc(sizeof(int)*(size+1));
  //myckt->Ai_matfree=malloc(sizeof(int)*(eMatrix->Elements));
  //myckt->NonZeroElements_matfree=malloc(sizeof(ElementPtr)*(eMatrix->Elements));
  //myckt->NonZeroValues_matfree=malloc(sizeof(double)*(eMatrix->Elements));

  ////Link rows.
  //if(!eMatrix->RowsLinked){
    //spcLinkRows(eMatrix);
  //}

  //ElementPtr pElement;    //used to temporarily store pointers to elements.
  //int row=0;             //used to index the Ap array
  //int index= 0;          //used to index the Ai array

  ////The first Ap value should point to the first Ai value, but we're not using Ap[0]...
  //myckt->Ap_matfree[0] = 0;
  //printf("\tAp[0] = 0\n");

  //for(row=1;row<=size;row++) { // foreach row 1..N ordering in Sp1.3
    //pElement = eMatrix->FirstInRow[row];
    //printf("debug : looking at row %d\n", row);
    //myckt->Ap_matfree[row] = index;
    //printf("debug : Ap[%d] = %d\n", row, index);

    //while(pElement != NULL){
		//printf("debug : NextInRow is  @ (%d, %d)\n", pElement->Row, pElement->Col);
		//myckt->Ai_matfree[index] = pElement->Col;
		//myckt->NonZeroElements_matfree[index]=pElement;
		//printf("\tAi[%d] = %d\n", index, pElement->Col);
		//printf("\tData[%d] = (%d,%d)\n", index, pElement->Row, pElement->Col);

		//index++;
		//pElement = pElement->NextInRow;
	//}
  //}

  ////Thus, a call to element (row, column) should search for (column) between Ai[Ap[row]] and Ai[Ap[row+1]].
//}


////////////////////////

//void build_grids(CKTcircuit* ckt){

////first, build compressed-sparse-row format representation
////note that this is CSR, not CSC as is done in KLU.
////(for symmetric matrices, they should be identical though.)
////much of this is borrowed from Nachiket's KLU wrappers.

  //MatrixPtr eMatrix = (MatrixPtr)ckt->CKTmatrix;
  //int size=eMatrix->Size;
  //printf("MatFree debug : size = %d\n", size);
  
  //ElementPtr pElement;

////Allocate space.
  //ckt->Ap_matfree=malloc(sizeof(int)*(size+1));
  //ckt->Ai_matfree=malloc(sizeof(int)*(eMatrix->Elements));
  //ckt->NonZeroElements_matfree=malloc(sizeof(ElementPtr)*(eMatrix->Elements));
  //ckt->NonZeroValues_matfree=malloc(sizeof(double)*(eMatrix->Elements));
  //printf("MatFree debug : allocated space.\n");

////Link rows.
  //spcLinkRows(eMatrix);


    //////for testing that everything is linked properly:
    ////int Col;
    ////for (Col = 1; Col <= eMatrix->Size; Col++){
      ////pElement = eMatrix->FirstInRow[Col];
      ////printf("buildgrids debug : FirstInRow[%d] is  @ (%d, %d)\n", Col, eMatrix->FirstInRow[Col]->Row,  eMatrix->FirstInRow[Col]->Col);
      ////pElement = pElement->NextInRow;
      
      ////while(pElement != NULL){
        ////printf("buildgrids debug : followed by (%d, %d)\n", pElement->Row,  pElement->Col);
        ////pElement = pElement->NextInRow;
      ////}
    ////}


  //int row=0;    //used to index the Ap array
  //int index= 0; //used to index the Ai array

////The first Ap value should point to the first Ai value, but we're not using Ap[0]...
  //ckt->Ap_matfree[0] = 0;
  //printf("\tAp[0] = 0\n");

  //for(row=1;row<=size;row++) { // foreach row 1..N ordering in Sp1.3
    //pElement = eMatrix->FirstInRow[row];
    //printf("debug : looking at row %d\n", row);
    //ckt->Ap_matfree[row] = index;
    //printf("debug : Ap[%d] = %d\n", row, index);

    //while(pElement != NULL){
		//printf("debug : NextInRow is  @ (%d, %d)\n", pElement->Row, pElement->Col);
		//ckt->Ai_matfree[index] = pElement->Col;
		//ckt->NonZeroElements_matfree[index]=pElement;
		//printf("\tAi[%d] = %d\n", index, pElement->Col);
		//printf("\tData[%d] = (%d,%d)\n", index, pElement->Row, pElement->Col);

		//index++;
		//pElement = pElement->NextInRow;
		
	//}
  //}
  
  ////Thus, a call to element (row, column) should search for (column) between Ai[Ap[row]] and Ai[Ap[row+1]].



//printf("MatFree debug:\n\tAp:\n");
//for(row=0;row<=size;row++){
	//printf("%d\n", ckt->Ap_matfree[row]);
//}

//printf("Ai:\n");
//for(row=0;row<eMatrix->Elements;row++){
	//printf("%d\n", ckt->Ai_matfree[row]);
//}

//printf("Data:\n");
//for(row=0;row<eMatrix->Elements;row++){
	//printf("(%d,%d)\n", ckt->NonZeroElements_matfree[row]->Row, ckt->NonZeroElements_matfree[row]->Col);
//}

//abort();

  //return;
//}

void note_vdiff( int mode, RealNumber diff_in ){
  
  static int step = 0;
  static bool firstcall = true;
  FILE * fp1;
  FILE * fp2;
  static RealNumber accum = 0;
  
  if(firstcall){
    fp1 = fopen ("./../graphs/raw2/vdiff_indi.csv","w");
    fprintf (fp1, "innerloop, vdiff per iter");

    fp2 = fopen ("./../graphs/raw2/vdiff_cumu.csv","w");
    fprintf (fp2, "innerloop, vdiff per iter");
    
    fclose(fp2);
    fclose(fp1);
    
    


    firstcall = false;
  }
  
  switch (mode) {
    
    case(0): //start a new line.
              step++;
              fp1 = fopen ("./../graphs/raw2/vdiff_indi.csv","a");
              fp2 = fopen ("./../graphs/raw2/vdiff_cumu.csv","a");
              fprintf (fp1, "\n%d", step);
              fprintf (fp2, "\n%d", step);
              fclose(fp2);
              fclose(fp1);

              break;
              
    case(1): //add an entry.
              accum += diff_in;

              fp1 = fopen ("./../graphs/raw2/vdiff_indi.csv","a");
              fp2 = fopen ("./../graphs/raw2/vdiff_cumu.csv","a");
              fprintf (fp1, ", %f", diff_in);
              fprintf (fp2, ", %f", accum);
              fclose(fp2);
              fclose(fp1);
              break;

    default: //when something hath gone wrong
              printf("Unknown mode %d. Aborting...\n", mode);
              abort();
    
  };
  
  return;
  
}

int depth_gauge( int mode, int val ){
	
	//internal static stuff
	static int local_accum_log = 0;
	static int local_max_log = 0;
	static int local_accum_lin = 0;
	static int local_max_lin = 0;
	
	static int total_buildstatictrees_log = 0;
    static int total_buildstatictrees_lin = 0;
    
	static int total_computestaticsigmas_log = 0;
	static int total_computestaticsigmas_lin = 0;
	
	static int total_supernodegatherterms_log = 0;
	static int total_supernodegatherterms_lin = 0;
	
	static int total_invertsigmas_log = 0;
	static int total_invertsigmas_lin = 0;
	
	static int total_resolveequations_log = 0;
	static int total_resolveequations_lin = 0;
	
	static int total_matfreepremult_log = 0;
	static int total_matfreepremult_lin = 0;
	
	static int total_computesigmas_log = 0;
	static int total_computesigmas_lin = 0;
	
	static int total_matfreesolve_log = 0;
	static int total_matfreesolve_lin = 0;
	
	static int total_handleinternals_log = 0;
	static int total_handleinternals_lin = 0;
	
	//some temporary variables
	int log_accum;
	int retval = 0;
	
	//mode select:
	switch (mode){
		
		case (0): //zero local values.
			local_accum_lin = 0;
			local_accum_log = 0;
			local_max_lin = 0;
			local_max_log = 0;
			retval = 0;
			break;
		
		case (1): //accumulate across width.
			local_accum_lin += val;
			retval = local_accum_lin;
			break;
		
		case (-1): //consolidate one accumulation (arithmetic)
			if(local_accum_lin > local_max_lin){
				local_max_lin = local_accum_lin;
			}
			local_accum_lin = 0;
			retval = local_max_lin;
			break;
		
		case (2): //accumulate across width.
			local_accum_log += val;
			retval = local_accum_log;
			break;


		case (-2): //consolidate one accumulation (log)
			if(local_accum_log !=0){
				local_accum_log = (int)ceil( log2((float)local_accum_log));
			}
			if(local_accum_log > local_max_log){
				local_max_log = local_accum_log;
			}
			local_accum_log = 0;
			retval = local_max_log;
			break;
			
		case(4): //remember/return the depth of build_static_trees
			if(val == 0){
				total_buildstatictrees_log = local_max_log;
				total_buildstatictrees_lin = local_max_lin;
				local_accum_log = 0;
				local_max_log = 0;
				local_accum_lin = 0;
				local_max_lin = 0;
				retval = 0;
			}
			else if (val == 1){
			    retval = total_buildstatictrees_lin;
			}
			else if (val == 2){
				retval = total_buildstatictrees_log;
			}
			else{
				retval = 0;
			}
			break;
		
		case(5): //remember/return the depth of compute_static_sigmas
			if(val == 0){
				total_computestaticsigmas_log = local_max_log;
				total_computestaticsigmas_lin = local_max_lin;
				local_accum_log = 0;
				local_max_log = 0;
				local_accum_lin = 0;
				local_max_lin = 0;
				retval = 0;
			}
			else if (val == 1){
			    retval = total_computestaticsigmas_lin;
			}
			else if (val == 2){
				retval = total_computestaticsigmas_log;
			}
			else{
				retval = 0;
			}
			break;
			
		case(6): //remember/return the depth of supernode_gather_terms
			if(val == 0){
				total_supernodegatherterms_log = local_max_log;
				total_supernodegatherterms_lin = local_max_lin;
				local_accum_log = 0;
				local_max_log = 0;
				local_accum_lin = 0;
				local_max_lin = 0;
				retval = 0;
			}
			else if (val == 1){
			    retval = total_supernodegatherterms_lin;
			}
			else if (val == 2){
				retval = total_supernodegatherterms_log;
			}
			else{
				retval = 0;
			}
			break;
			
		case(7): //remember/return the depth of invert_sigmas
			if(val == 0){
				total_invertsigmas_log = local_max_log;
				total_invertsigmas_lin = local_max_lin;
				local_accum_log = 0;
				local_max_log = 0;
				local_accum_lin = 0;
				local_max_lin = 0;
				retval = 0;
			}
			else if (val == 1){
			    retval = total_invertsigmas_lin;
			}
			else if (val == 2){
				retval = total_invertsigmas_log;
			}
			else{
				retval = 0;
			}
			break;
			
		case(8): //remember/return the depth of resolve_equations
			if(val == 0){
				total_resolveequations_log = local_max_log;
				total_resolveequations_lin = local_max_lin;
				local_accum_log = 0;
				local_max_log = 0;
				local_accum_lin = 0;
				local_max_lin = 0;
				retval = 0;
			}
			else if (val == 1){
			    retval = total_resolveequations_lin;
			}
			else if (val == 2){
				retval = total_resolveequations_log;
			}
			else{
				retval = 0;
			}
			break;
			
		case(9): //remember/return the depth of matfree_premult
			if(val == 0){
				total_matfreepremult_log = local_max_log;
				total_matfreepremult_lin = local_max_lin;
				local_accum_log = 0;
				local_max_log = 0;
				local_accum_lin = 0;
				local_max_lin = 0;
				retval = 0;
			}
			else if (val == 1){
			    retval = total_matfreepremult_lin;
			}
			else if (val == 2){
				retval = total_matfreepremult_log;
			}
			else{
				retval = 0;
			}
			break;
			
		case(10): //remember/return the depth of compute_sigmas
			if(val == 0){
				total_computesigmas_log = local_max_log;
				total_computesigmas_lin = local_max_lin;
				local_accum_log = 0;
				local_max_log = 0;
				local_accum_lin = 0;
				local_max_lin = 0;
				retval = 0;
			}
			else if (val == 1){
			    retval = total_computesigmas_lin;
			}
			else if (val == 2){
				retval = total_computesigmas_log;
			}
			else{
				retval = 0;
			}
			break;
			
		case(11): //remember/return the depth of matfree_solve
			if(val == 0){
				total_matfreesolve_log = local_max_log;
				total_matfreesolve_lin = local_max_lin;
				local_accum_log = 0;
				local_max_log = 0;
				local_accum_lin = 0;
				local_max_lin = 0;
				retval = 0;
			}
			else if (val == 1){
			    retval = total_matfreesolve_lin;
			}
			else if (val == 2){
				retval = total_matfreesolve_log;
			}
			else{
				retval = 0;
			}
			break;
			
		case(12): //remember/return the depth of handle_internals
			if(val == 0){
				total_handleinternals_log = local_max_log;
				total_handleinternals_lin = local_max_lin;
				local_accum_log = 0;
				local_max_log = 0;
				local_accum_lin = 0;
				local_max_lin = 0;
				retval = 0;
			}
			else if (val == 1){
			    retval = total_handleinternals_lin;
			}
			else if (val == 2){
				retval = total_handleinternals_log;
			}
			else{
				retval = 0;
			}
			break;
			
	}
	
	return retval; //return whatever
}

int depth_gauge_new(int mode, int val_1, int val_2){
	
	int retval = 0; //returns 0 by default
	
	static int * res_eqs_array;
	
	static int mylog_accum = 0;
	static int mylog_max   = 0;
	static int mylin_accum = 0;
	static int mylin_max   = 0;
	
	static int buildstatictrees_log = 0;
    static int buildstatictrees_lin = 0;
    
    static int computestaticsigmas_log = 0;
    static int computestaticsigmas_lin = 0;
    
    static int supernodegatherterms_log = 0;
    static int supernodegatherterms_lin = 0;
    
    static int invertsigmas_log = 0;
    static int invertsigmas_lin = 0;
    
    static int resolveequations_log = 0;
    static int resolveequations_lin = 0;
    
    static int matfreepremult_log = 0;
    static int matfreepremult_lin = 0;
    
    static int computesigmas_log = 0;
    static int computesigmas_lin = 0;
    static int computesigmas_accum = 0;
    
    static int matfreesolve_log = 1; //doesn't change.
    static int matfreesolve_lin = 0;
    
    static int handleinternals_lin = 0;
    static int handleinternals_log = 0;
	
	switch(mode){
		
		case(0): //initialise the log and lin counters to a particular pair of values (normally 0)
			mylog_max = val_1;
			mylin_max = val_2;
			//also zero the accumulated matfree_iter_depths for this modeval loop iteration.
			computesigmas_accum = 0;
		break;
			
		case(1): //build static trees
			switch(val_1){
				
				case(0): //initalise mylin_accum to a value (normally 0.)
					mylog_accum = val_2;
					mylin_accum = val_2;
					retval = mylin_accum;
				break;
				
				case(1): //increment mylin_accum.
					mylin_accum++;
					retval = mylin_accum;
				break;
				
				case(-1): //decrement mylin_accum.
					mylin_accum--;
					retval = mylin_accum;
				break;
				
				case(2): //return mylin_accum without doing anything.
					retval = mylin_accum;
				break;
				
				case(3): //reached the end of a tree.
					if (mylin_accum > mylin_max){
						mylin_max = mylin_accum;
					}
					retval = mylin_max;
				break;
				
				case(-9): //finished the function, looking to consolidate.
					if(mylog_max < 2){
						buildstatictrees_log = mylog_max;
					}
					else{
						buildstatictrees_log = (int)ceil( log2((float)mylog_max));
					}
					buildstatictrees_lin = mylin_max;
					retval = mylin_max;

					//zero everything.
					mylin_max = 
					mylin_accum = 
					mylog_max = 
					mylog_accum = 0;
				break;
				
				case (-8): //return relevant values.
					retval = buildstatictrees_lin;
				break;
			};
		break;
		
		case(2): //compute static sigmas.
			switch(val_1){
				
				case(0): //initalise mylog_max to a value (normally 0.)
					mylog_max = val_2;
					mylin_max = val_2;
					retval = mylog_max;
				break;
				
				case(1): //check-in a value. (we don't need the accum.)
					if (val_2 > mylog_max){
						mylog_max = val_2;
					}
					retval = mylog_max;
				break;
				
				
				case(-9): //finished the function, looking to consolidate.
					if(mylog_max < 2){ //because 0 means no work, 1 is just a lone copy.
						computestaticsigmas_log = mylog_max;
					}
					else{
						computestaticsigmas_log = (int)ceil( log2((float)mylog_max));
					}
					computestaticsigmas_lin = mylin_max; // should be 0.
					retval = computestaticsigmas_log;

					//zero everything.
					mylin_max = 
					mylin_accum = 
					mylog_max = 
					mylog_accum = 0;
				break;
				
				case (-8): //return relevant values.
					retval = computestaticsigmas_log;
				break;
				
			};
		break;
			
		case(3): //supernode gather terms.
			switch(val_1){
				
				case(0): //initalise mylin_accum to a value (normally 0.)
					mylog_accum = val_2;
					mylin_accum = val_2;
					retval = mylin_accum;
				break;
				
				case(1): //increment mylin_accum.
					mylin_accum++;
					retval = mylin_accum;
				break;
				
				case(-1): //decrement mylin_accum.
					mylin_accum--;
					retval = mylin_accum;
				break;
				
				case(2): //return mylin_accum without doing anything.
					retval = mylin_accum;
				break;
				
				case(3): //reached the end of a walk.
					if (mylin_accum > mylin_max){
						mylin_max = mylin_accum;
					}
					//zero stuff for the next pass.
					mylin_accum = 0;
					mylog_accum = 0;
					retval = mylin_max;
				break;
				
				case(-9): //consolidate.
					if(mylog_max < 2){ //because 0 means no work, 1 is just a lone copy.
						supernodegatherterms_log = mylog_max;// should be 0.
					}
					else{
						supernodegatherterms_log = (int)ceil( log2((float)mylog_max));
					}
					supernodegatherterms_lin = mylin_max;
					
					if (supernodegatherterms_lin > 0){ // add on mult (then accum) op.
						supernodegatherterms_lin++;
						supernodegatherterms_lin++;
					}

					retval = supernodegatherterms_lin;

					//zero everything.
					mylin_max = 
					mylin_accum = 
					mylog_max = 
					mylog_accum = 0;
				break;
				
				case (-8): //return relevant values.
					retval = supernodegatherterms_lin;
				break;

			};
		break;
		
		case(4): //invert sigmas. (this is overkill, invery sigmas is depth 1. Ideally.)
			switch(val_1){

				case(0): //initalise mylin_accum to a value (normally 0.)
					mylog_accum = val_2;
					mylin_accum = val_2;
					retval = mylin_accum;
				break;
				
				case(1): //increment mylin_accum.
					mylin_accum++;
					retval = mylin_accum;
				break;
				
				case(-1): //decrement mylin_accum.
					mylin_accum--;
					retval = mylin_accum;
				break;
				
				case(2): //return mylin_accum without doing anything.
					retval = mylin_accum;
				break;
				
				case(3): //consolidate.
					if (mylin_accum > mylin_max){
						mylin_max = mylin_accum;
					}
				break;
				
				case(-9): //finished the function, looking to consolidate.
					if(mylog_max < 2){
						invertsigmas_log = mylog_max;
					}
					else{
						invertsigmas_log = (int)ceil( log2((float)mylog_max));
					}
					invertsigmas_lin = mylin_max;
					retval = mylin_max;

					//zero everything.
					mylin_max = 
					mylin_accum = 
					mylog_max = 
					mylog_accum = 0;
				break;
				
				case (-8): //return relevant values.
					retval = invertsigmas_lin;
				break;
				
			};
		break;
		
		case(5): //resolve equations.
			switch(val_1){
				
				case(5): //make temporary storage
					res_eqs_array = (int*)malloc(sizeof(int)*val_2);
					retval = val_2;
				break;
				
				case(-5): //destroy temporary storage
					free(res_eqs_array);
					retval = 0;
				break;
				
				case(0): //initialise storage
					for(retval = 0; retval <val_2; retval++){
						res_eqs_array[retval] = 0;
					}
					mylog_accum = 0;
				break;
				
				case(1): //increment add.
					mylog_accum += val_2;
					retval = mylog_accum;
				break;
				
				case(2): //look for collisions.
					res_eqs_array[val_2]++;
					retval = res_eqs_array[val_2];
				break;
				
				case(3): //compare/reduce.
					for(retval = 0; retval <val_2; retval++){
						if (res_eqs_array[retval] > mylog_max){
							mylog_max = res_eqs_array[retval];
						}
					}
					if(mylog_accum > mylog_max){
						mylog_max = mylog_accum;
					}
					
					//zero stuff for the next pass.
					mylin_accum = 0;
					mylog_accum = 0;
					retval = mylog_max;
					
				break;
				
				case(-9): //finished the function, looking to consolidate.
					if(mylog_max < 2){
						resolveequations_log = mylog_max;
					}
					else{
						resolveequations_log = (int)ceil( log2((float)mylog_max));
					}
					resolveequations_lin = mylin_max;
					retval = mylin_max;

					//zero everything.
					mylin_max = 
					mylin_accum = 
					mylog_max = 
					mylog_accum = 0;
				break;
				
				case (-8): //return relevant values.
					retval = resolveequations_log;
				break;
				
			};
		break;
		
		case(6): //matfree premult.
			switch(val_1){
				case (1):
					mylog_accum = val_2;
					retval = mylog_accum;
				break;
				
				case (2): //consolidate
					if(mylog_accum > mylog_max){
						mylog_max = mylog_accum;
					}
					mylog_accum = 0;
					retval = mylog_max;
				break;
				
				case (-9): //finished the function, looking to consolidate.
					if(mylog_max < 2){
						matfreepremult_log = mylog_max;
					}
					else{
						matfreepremult_log = (int)ceil( log2((float)mylog_max));
					}
					matfreepremult_log++; //for the multiply.
					matfreepremult_lin = mylin_max;
					retval = matfreepremult_log;

					//zero everything.
					mylin_max = 
					mylin_accum = 
					mylog_max = 
					mylog_accum = 0;
				break;
				
				case (-8): //return relevant values.
					retval = matfreepremult_log;
				break;

			};
		break;
		
		case (7): //compute sigmas. (the bulk of the work.)
			switch(val_1){
				
				case(1): // check in a thing. (no need to do the whole accum/max thing; this is simple.
					if(mylog_max < val_2){
						mylog_max = val_2;
					}
					retval = mylog_max;
				break;
				
				case (-9): //consolidate.
					if(mylog_max < 2){
						computesigmas_log = mylog_max;
					}
					else{
						computesigmas_log = (int)ceil( log2((float)mylog_max));
					}
					if(computesigmas_log > 0){
						computesigmas_log++; //for the multiply.
					}
					computesigmas_lin = mylin_max;
					retval = computesigmas_log;
					computesigmas_accum += computesigmas_log; //over the whole modeval.

					//zero everything.
					mylin_max = 
					mylin_accum = 
					mylog_max = 
					mylog_accum = 0;
				break;
				
				case (-8): //return relevant values.
					retval = computesigmas_accum;
				break;


			};
		break;
		
		case(8): //matfree "solve"
			//this is ALWAYS depth 1 - we're just doing a sub at each node.
			retval = 1;
		break;

		case(9): //handle internals.
			switch(val_1){
				case(1):
					mylin_accum += val_2;
					retval = mylin_accum;
				break;
				
				case(-1):
					mylin_accum -= val_2;
					retval = mylin_accum;
				break;
				
				case(2):
					if(mylin_accum > mylin_max){
						mylin_max = mylin_accum;
					}
					retval = mylin_max;
				break;
				
				case (-9): //consolidate.

					handleinternals_lin = mylin_max;
					retval = handleinternals_lin;

					//zero everything.
					mylin_max = 
					mylin_accum = 
					mylog_max = 
					mylog_accum = 0;
				break;
				
				case (-8): //return relevant values.
					retval = handleinternals_lin;
				break;
				
				
			};
		break;
		
		case (10): //return the total depth.
			retval = buildstatictrees_lin
					+ computestaticsigmas_log
					+ supernodegatherterms_lin
					+ invertsigmas_lin
					+ resolveequations_log
					+ matfreepremult_log
					+ computesigmas_accum
					+ matfreesolve_log
					+ handleinternals_lin;
		break;

	};
	
	return retval;
}
