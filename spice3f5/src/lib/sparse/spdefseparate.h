// avoid overlapping bs

#include "spmatrix.h"

typedef spREAL RealNumber, *RealVector;

struct  MatrixElement
{   RealNumber   Real;
#if spCOMPLEX
    RealNumber   Imag;
#endif
    int          Row;
    int          Col;
    struct MatrixElement  *NextInRow;
    struct MatrixElement  *NextInCol;
#if INITIALIZE
    char        *pInitInfo;
#endif
};

typedef  struct MatrixElement  *ElementPtr;
typedef  ElementPtr  *ArrayOfElementPtrs;

