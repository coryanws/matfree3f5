/**********
Copyright 1990 Regents of the University of California.  All rights reserved.
Author: 1985 Thomas L. Quarles
**********/

#include "spice.h"
#include "cktdefs.h"
#include <stdio.h>
#include "sperror.h"
#include "trandefs.h"
#include "util.h"
#include "suffix.h"

#include "papi.h"

extern SPICEanalysis *analInfo[];
extern int ANALmaxnum;

int
CKTdoJob(inCkt,reset,inTask, noplot, klu)
    GENERIC *inCkt;
    int reset;
    GENERIC *inTask;
{
    CKTcircuit	*ckt = (CKTcircuit *)inCkt;
    TSKtask	*task = (TSKtask *)inTask;
    JOB		*job;
    double	startTime;
    int		error, i, error2;

#ifdef HAS_SENSE2
    int		senflag;
    static int	sens_num = -1;

    /* Sensitivity is special */
    if (sens_num < 0) {
	for (i = 0; i <  ANALmaxnum; i++)
	    if (!strcmp("SENS2", analInfo[i]->public.name))
		break;
	sens_num = i;
    }
#endif


    ckt->CKTstat->STATanalyzeTime=0.0;
    startTime = (*(SPfrontEnd->IFseconds))( );

    ckt->CKTtemp  = task->TSKtemp;
    ckt->CKTnomTemp  = task->TSKnomTemp;
    ckt->CKTmaxOrder  = task->TSKmaxOrder;
    ckt->CKTintegrateMethod  = task->TSKintegrateMethod;
    ckt->CKTbypass  = task->TSKbypass;
    ckt->CKTdcMaxIter  = task->TSKdcMaxIter;
    ckt->CKTdcTrcvMaxIter  = task->TSKdcTrcvMaxIter;
    ckt->CKTtranMaxIter  = task->TSKtranMaxIter;
    ckt->CKTnumSrcSteps  = task->TSKnumSrcSteps;
    ckt->CKTnumGminSteps  = task->TSKnumGminSteps;
    ckt->CKTminBreak  = task->TSKminBreak;
    ckt->CKTabstol  = task->TSKabstol;
    ckt->CKTpivotAbsTol  = task->TSKpivotAbsTol;
    ckt->CKTpivotRelTol  = task->TSKpivotRelTol;
    ckt->CKTreltol  = task->TSKreltol;
    ckt->CKTchgtol  = task->TSKchgtol;
    ckt->CKTvoltTol  = task->TSKvoltTol;
    ckt->CKTgmin  = task->TSKgmin;
    ckt->CKTdelmin  = task->TSKdelmin;
    ckt->CKTtrtol  = task->TSKtrtol;
    ckt->CKTdefaultMosL  = task->TSKdefaultMosL;
    ckt->CKTdefaultMosW  = task->TSKdefaultMosW;
    ckt->CKTdefaultMosAD  = task->TSKdefaultMosAD;
    ckt->CKTdefaultMosAS  = task->TSKdefaultMosAS;
    ckt->CKTfixLimit  = task->TSKfixLimit;
    ckt->CKTnoOpIter  = task->TSKnoOpIter;
    ckt->CKTtryToCompact = task->TSKtryToCompact;
    ckt->CKTbadMos3 = task->TSKbadMos3;
    ckt->CKTkeepOpInfo = task->TSKkeepOpInfo;
    ckt->CKTtroubleNode  = 0;
    ckt->CKTtroubleElt  = NULL;
#ifdef NEWTRUNC
    ckt->CKTlteReltol = task->TSKlteReltol;
    ckt->CKTlteAbstol = task->TSKlteAbstol;
#endif /* NEWTRUNC */

    error = 0;

    if (reset) {

	ckt->CKTdelta = 0.0;
	ckt->CKTtime = 0.0;
	ckt->CKTcurrentAnalysis = 0;

#ifdef HAS_SENSE2
	senflag = 0;
	if (sens_num < ANALmaxnum)
	    for (job = task->jobs; !error && job; job = job->JOBnextJob) {
		if (job->JOBtype == sens_num) {
		    senflag = 1;
		    ckt->CKTcurJob = job;
		    ckt->CKTsenInfo = (SENstruct *) job;
		    error = (*(analInfo[sens_num]->an_func))(ckt, reset);
		}
	    }

	if (ckt->CKTsenInfo && (!senflag || error))
	    FREE(ckt->CKTsenInfo);
#endif

	/* normal reset */
	if (!error)
	    error = CKTunsetup(ckt);
	if (!error)
	    error = CKTsetup(ckt);
	if (!error)
	    error = CKTtemp(ckt);
	if (error)
	    return error;
    }

    error2 = OK;

    // Add PAPI measurement support for PAPI_FP_INS, PAPI_TOT_INS
    // Maybe even PAPI_L2_DCA, PAPI_L2_DCM?
    long_long papi_counts[2],old_papi_counts[2];
    int events[2];
    if(ckt->papi==1) {
	    if(ckt->papifp==1) {
	    	events[0]=PAPI_FP_INS; events[1]=PAPI_TOT_INS;
	    } else {
	    	events[0]=PAPI_L2_TCA; events[1]=PAPI_L2_TCM;
	    }
	    //int events[4]={PAPI_FP_INS, PAPI_TOT_INS, PAPI_L2_TCA, PAPI_L2_TCM};
	    int retval=0;
	    retval = PAPI_library_init(PAPI_VER_CURRENT);
	    if (retval != PAPI_VER_CURRENT && retval > 0) {
		    printf("PAPI library version mismatch!\n");
		    exit(1);
	    }
	    retval = PAPI_is_initialized();
	    if (retval != PAPI_LOW_LEVEL_INITED) {
		    printf("PAPI library low-level not initialized, err=%s\n",PAPI_strerror(retval));
		    exit(1);
	    }
	    retval = PAPI_start_counters(events,2);
	    if(retval!=PAPI_OK) {
		    printf("Failed to start counters : err=%s\n",PAPI_strerror(retval));
		    exit(-1);
	    }
    }


    // record command-line arguments to pass to the simulator guts
    int local_papi=ckt->papi;
    int local_papifp=ckt->papifp;
    int local_extract=ckt->extract;
    int local_noplot=ckt->noplot;
    int local_klu=ckt->klu;
    int local_matfree=ckt->matfree;

	ckt->total_counts=(long*)malloc(2*sizeof(long_long));
	ckt->modeleval_counts=(long*)malloc(2*sizeof(long_long));
	ckt->matsolve_counts=(long*)malloc(2*sizeof(long_long));
  ckt->matfree_counts=(long*)malloc(2*sizeof(long_long));
	ckt->total_counts[0]=0; ckt->total_counts[1]=0;
	ckt->modeleval_counts[0]=0; ckt->modeleval_counts[1]=0;
	ckt->matsolve_counts[0]=0; ckt->matsolve_counts[1]=0;
  ckt->matfree_counts[0]=0; ckt->matfree_counts[1]=0;

    /* Analysis order is important */
    for (i = 0; i < ANALmaxnum; i++) {

#ifdef HAS_SENSE2
	if (i == sens_num)
	    continue;
#endif

    ckt->noplot = local_noplot;
    ckt->klu = local_klu;
	ckt->extract = local_extract;

	ckt->papi = local_papi; //printf("ckt->papi=%d, ckt->klu=%d\n",ckt->papi, ckt->klu);
	ckt->papifp = local_papifp;
  ckt->matfree = local_matfree;
	//printf("extract=%d\n",ckt->extract);



	for (job = task->jobs; job; job = job->JOBnextJob) {
	    if (job->JOBtype == i) {
                ckt->CKTcurJob=job;
		error = OK;
		if (analInfo[i]->an_init)
		    error = (*(analInfo[i]->an_init))(ckt, job);
		if (!error && analInfo[i]->do_ic)
		    error = CKTic(ckt);
		if (!error)
		    error = (*(analInfo[i]->an_func))(ckt, reset);
		if (error)
		    error2 = error;
	    }
	}
    }


    if(ckt->klu==0) {ckt->CKTstat->STATanalyzeTime=0.0;}
    ckt->CKTstat->STATtotAnalTime += (*(SPfrontEnd->IFseconds))( ) - startTime;
    double tempTime = ckt->CKTstat->STATloadTime +
		ckt->CKTstat->STATdecompTime + 
		ckt->CKTstat->STATsolveTime +
		ckt->CKTstat->STATreorderTime +
		ckt->CKTstat->STATanalyzeTime;
//		printf("Temp Time=%g\n", tempTime);
    ckt->CKTstat->STAToverheadTime = ckt->CKTstat->STATtranTime - tempTime;

#ifdef HAS_SENSE2
    if (ckt->CKTsenInfo)
	SENdestroy(ckt->CKTsenInfo);
#endif

    // PAPI
    if(local_papi==1) {    
	PAPI_stop_counters(papi_counts,2);

	ckt->total_counts[0]=ckt->modeleval_counts[0]+ckt->matsolve_counts[0]+ckt->matfree_counts[0];
	ckt->total_counts[1]=ckt->modeleval_counts[1]+ckt->matsolve_counts[1]+ckt->matfree_counts[1];

	printf("PAPI");
	if(local_papifp==1) {
		printf("TOTAL: PAPI_FP_INS=%lld, PAPI_TOT_INS=%lld\n", ckt->total_counts[0], ckt->total_counts[1]);
		printf("MODELEVAL: PAPI_FP_INS=%lld, PAPI_TOT_INS=%lld\n", ckt->modeleval_counts[0], ckt->modeleval_counts[1]);
		printf("MATSOLVE: PAPI_FP_INS=%lld, PAPI_TOT_INS=%lld\n", ckt->matsolve_counts[0], ckt->matsolve_counts[1]);
    printf("MATFREE: PAPI_FP_INS=%lld, PAPI_TOT_INS=%lld\n", ckt->matfree_counts[0], ckt->matfree_counts[1]);
	} else {
		printf("TOTAL: PAPI_L2_TCA=%lld, PAPI_L2_TCM=%lld\n", ckt->total_counts[0], ckt->total_counts[1]);
		printf("MODELEVAL: PAPI_L2_TCA=%lld, PAPI_L2_TCM=%lld\n", ckt->modeleval_counts[0], ckt->modeleval_counts[1]);
		printf("MATSOLVE: PAPI_L2_TCA=%lld, PAPI_L2_TCM=%lld\n", ckt->matsolve_counts[0], ckt->matsolve_counts[1]);
    printf("MATFREE: PAPI_L2_TCA=%lld, PAPI_L2_TCM=%lld\n", ckt->matfree_counts[0], ckt->matfree_counts[1]);
	}

    }

    /** Support non-PAPI based timing measurement readout.. **/
    printf("\tTotal Iterations=%d\n",ckt->CKTstat->STATnumIter);
    printf("\tTotal Reorderings=%d\n",ckt->CKTstat->STATreorderCount);
    printf("\tTotal Transient Iterations=%d\n",ckt->CKTstat->STATtranIter);
    printf("\tTotal Old Iterations=%d\n",ckt->CKTstat->STAToldIter);
    printf("\tTotal Time points=%d\n",ckt->CKTstat->STATtimePts);
    printf("\tTotal Accepted Time points=%d\n",ckt->CKTstat->STATaccepted);
    printf("\tTotal Rejected Time points=%d\n",ckt->CKTstat->STATrejected);
    printf("\tTotal Analysis Time=%.16e\n",ckt->CKTstat->STATtotAnalTime);
    printf("\tTotal DC Analysis Time=%.16e\n",ckt->CKTstat->STATdcTime);
    printf("\tTotal Transient Analysis Time=%.16e\n",ckt->CKTstat->STATtranTime);
    printf("\tTotal Device Loading Time=%.16e\n",ckt->CKTstat->STATloadTime);
    printf("\tTotal LU Decomposition Time=%.16e\n",ckt->CKTstat->STATdecompTime);
    printf("\tTotal F-B substitution Time=%.16e\n",ckt->CKTstat->STATsolveTime);
    printf("\tTotal Reordering Time=%.16e\n",ckt->CKTstat->STATreorderTime);
    printf("\tTotal KLU Analyze Time=%.16e\n",ckt->CKTstat->STATanalyzeTime);
    printf("\tTotal Transient LU decomposition Time=%.16e\n",ckt->CKTstat->STATtranDecompTime);
    printf("\tTotal Transient F-B substitution Time=%.16e\n",ckt->CKTstat->STATtranSolveTime);
    printf("\tTotal Overhead Time=%.16e\n",ckt->CKTstat->STAToverheadTime);
    printf("\tTotal convTest Time=%.16e\n",ckt->CKTstat->STATconvTestTime);
    printf("\tTotal timestepCalc Time=%.16e\n",ckt->CKTstat->STATtimestepCalcTime);


ckt->CKTstat->STATloadTime=0;
ckt->CKTstat->STATdecompTime=0;
ckt->CKTstat->STATsolveTime=0;
ckt->CKTstat->STATreorderTime=0;
ckt->CKTstat->STATanalyzeTime=0;

    return(error2);
}

