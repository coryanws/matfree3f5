/**********
Copyright 2010 University of Pennsylvania, California Institute of Technology.
All rights reserved.
Author: 2010 Nachiket Kapre
**********/

#include "cktdefs.h"
#include "smpdefs.h"

// Stores routines used to splice in KLU calls.

void wrapper_KLU_factor(char* cMatrix, RealVector rhs, int iter, CKTcircuit* ckt);
void wrapper_KLU_solve(char* cMatrix, RealVector rhs, int iter, CKTcircuit* ckt);
