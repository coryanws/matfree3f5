/**********
Copyright 1990 Regents of the University of California.  All rights reserved.
Author: 1985 Thomas L. Quarles
**********/

#include "spice.h"
#include <stdio.h>
#include "trandefs.h"
#include "cktdefs.h"
#include "smpdefs.h"
#include "sperror.h"
#include "util.h"
#include "suffix.h"
#include "klu.h"
#include "spdefs.h"

#include "klu_interface.h"

// Added by Nachiket to permit analysis and reordering of the matrix for the KLU solve phase
void wrapper_KLU_analyze(char* cMatrix, CKTcircuit* ckt) {
	
	//printf("Starting KLU analysis...\n");
	MatrixPtr eMatrix = (MatrixPtr) cMatrix;

	// Sparse 1.3 data structures
	//printf("Setting up Sparse 1.3 structures\n");
	ElementPtr pElement;
	ElementPtr pColumn;
	int Size=eMatrix->Size;
	int NonZeroes=eMatrix->Elements;
	int convert; // index for conversion
	//printf("Dimensions are Size=%d, Nonzeroes=%d\n",Size, NonZeroes);

	// Default KLU Call
	// dont need to define Symbolic anymore...klu_symbolic *Symbolic;
	// dont need to define Numeric anymore.. klu_numeric *Numeric;
	klu_common Common;

	klu_defaults(&Common);
	//Common.tol=ckt->CKTpivotAbsTol; // set pivotabstol!

	// Also build the Ap, Ai and EntryPtr structures...
	// KLU compressed sparse column data-structures
	ckt->Ap=malloc(sizeof(int)*(Size+1));
	ckt->Ai=malloc(sizeof(int)*NonZeroes);
	ckt->NonZeroElements=malloc(sizeof(ElementPtr)*NonZeroes);
	ckt->NonZeroValues=malloc(sizeof(double)*NonZeroes);

	// Convert linked-list into Compressed-Sparse-Column Arrays
	int Step;
	int Index=0;

	ckt->Ap[0]=0;	
	for(Step=1;Step<=Size;Step++) { // foreach row 1..N ordering in Sp1.3

		//printf("Preparing matrix with Step=%d\n",Step);

		pElement = eMatrix->FirstInCol[Step]; // linked-list for each col..
		while(pElement!=NULL) {
			ckt->NonZeroElements[Index]=pElement;
			ckt->Ai[Index]=pElement->Row-1; // row indexing 1..N
			
			// move to next element in column
			Index++;
			pElement=pElement->NextInCol;
		}
		// record index for next column at end of row
		ckt->Ap[Step]=Index;
	}

	if(ckt->Ap[Size]!=NonZeroes) {
		printf("Nonzero count mistmatch!!");
		exit(-1);
	}

	// Now convert the ElementPtr Array into double array for the KLU solver! Yeah!
	for(convert=0;convert<NonZeroes;convert++) {
		ckt->NonZeroValues[convert]=(double)ckt->NonZeroElements[convert]->Real;
	}
	
	ckt->Symbolic = klu_analyze(Size, ckt->Ap, ckt->Ai, &Common);
}


// Added by Nachiket to allow plugging in other sparse matrix solvers
// For now, we will integrate this with KLU solver
void wrapper_KLU_factor(char* cMatrix, RealVector rhs, int iter, CKTcircuit* ckt) {


// Conventional -e flow
	if(ckt->extract==1) { 
		printf("Writing out a matrix\n");
        	spFileMatrix(ckt->CKTmatrix,"spice3f5","",0,1,1);
		spFileVector(ckt->CKTmatrix,"spice3f5","",ckt->CKTrhs, NULL);
		exit(0);
	}

/* 
// For the Thesis example.circuit matrix refactorization steps..
printf("extract=%d iter=%d\n",ckt->extract, iter);
	if(ckt->extract==1 && iter==139) { 
		printf("Writing out a matrix\n");
        	spFileMatrix(ckt->CKTmatrix,"spice3f5_1","",0,1,1);
		spFileVector(ckt->CKTmatrix,"spice3f5_1","",ckt->CKTrhs, NULL);
//		exit(0);
	}

	if(ckt->extract==1 && iter==141) { 
		printf("Writing out a matrix\n");
        	spFileMatrix(ckt->CKTmatrix,"spice3f5_2","",0,1,1);
		spFileVector(ckt->CKTmatrix,"spice3f5_2","",ckt->CKTrhs, NULL);
//		exit(0);
	}
*/

	//printf("Starting KLU conversion\n");
	MatrixPtr eMatrix = (MatrixPtr) cMatrix;

	// Sparse 1.3 data structures
	//printf("Setting up Sparse 1.3 structures\n");
	ElementPtr pElement;
	ElementPtr pColumn;
	int Size=eMatrix->Size;
	int NonZeroes=eMatrix->Elements;
	int convert; // index for conversion
	//printf("Dimensions are Size=%d, Nonzeroes=%d\n",Size, NonZeroes);

	// Default KLU Call
	// dont need to define Symbolic anymore...klu_symbolic *Symbolic;
	// dont need to define Numeric anymore.. klu_numeric *Numeric;
	klu_common Common;

	//printf("Defaults\n");
	klu_defaults(&Common);
	// Factor it atleast once..
	if(ckt->firstCall==1) {
		ckt->firstCall=0;

                double startTime = (*(SPfrontEnd->IFseconds))();		
		wrapper_KLU_analyze(cMatrix, ckt);
		ckt->CKTstat->STATanalyzeTime += (*(SPfrontEnd->IFseconds))()-startTime;

	
		ckt->Symbolic = klu_analyze(Size, ckt->Ap, ckt->Ai, &Common);
		ckt->Numeric = klu_factor(ckt->Ap, ckt->Ai, ckt->NonZeroValues, ckt->Symbolic, &Common);
	} else {
		// Now convert the ElementPtr Array into double array for the KLU solver! Yeah!
		for(convert=0;convert<NonZeroes;convert++) {
			ckt->NonZeroValues[convert]=(double)ckt->NonZeroElements[convert]->Real;
		}

                double startTime = (*(SPfrontEnd->IFseconds))();		
		klu_refactor(ckt->Ap, ckt->Ai, ckt->NonZeroValues, ckt->Symbolic, ckt->Numeric, &Common);
		ckt->CKTstat->STATdecompTime += (*(SPfrontEnd->IFseconds))()-startTime;

	}

} 

// Separating solve step from factorization to allow smoother integration
// of solution into the flow.. grr 

void wrapper_KLU_solve(char* cMatrix, RealVector rhs, int iter, CKTcircuit* ckt) {
	
	MatrixPtr eMatrix = (MatrixPtr) cMatrix;

	int Step=0;
	klu_common Common;
	int Size=eMatrix->Size;
	double *b=malloc(sizeof(double)*Size);

	// Record RHS vector
	// Perform row reordering here... for consistency.. wtfrak maan!
	for(Step=1;Step<=Size;Step++) {
		b[Step-1] = (double)rhs[eMatrix->IntToExtRowMap[Step]];
	}

	//printf("Defaults\n");
	klu_defaults(&Common);

	//printf("Solve\n");
        double startTime = (*(SPfrontEnd->IFseconds))();		
	int retVal = klu_solve(ckt->Symbolic, ckt->Numeric, Size, 1, b, &Common);
	ckt->CKTstat->STATsolveTime += (*(SPfrontEnd->IFseconds))()-startTime;
	if(retVal==0) {
		printf("Solve failed?");
		exit(-1);
	}

	int fg;
	for(fg=0; fg<Size; fg++){
		rhs[fg+1] = b[eMatrix->ExtToIntColMap[fg+1]-1];
	}

}
