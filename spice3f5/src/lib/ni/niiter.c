/**********
Copyright 1990 Regents of the University of California.  All rights reserved.
Author: 1985 Thomas L. Quarles
**********/

/*
 * NIiter(ckt,maxIter)
 *
 *  This subroutine performs the actual numerical iteration.
 *  It uses the sparse matrix stored in the circuit struct
 *  along with the matrix loading program, the load data, the
 *  convergence test function, and the convergence parameters
 */


//added by Coryan - useful bits for the matfree solver.
#define USE_MATFREE
#define MAX_ITERATIONS 50000000
#define TOLERANCE 0.000001

#include "spice.h"
#include <stdio.h>
#include "trandefs.h"
#include "cktdefs.h"
#include "smpdefs.h"
#include "sperror.h"
#include "util.h"
#include "suffix.h"
#include <stdbool.h> //because Coryan likes bools.

// Added by Nachiket on 2/27/2010
#include "klu_interface.h"

// Added by Nachiket on 6/11/2010 (under protest)
#include "papi.h"



/* NIiter() - return value is non-zero for convergence failure */

/* added by Coryan - Matfree functions and static variables */
#ifdef USE_MATFREE
#include "matfree_interface.h"


/* This little function performs the MatFree iterations.
 */
/*
int magic(CKTcircuit * cktin){
	
	printf("***G***\n");
	print_nodes();
	
	int iterations = 1; //for counting how many iterations this needs.
	bool solution_found = false; //if we've converged on a solution.
	
	//build_node_jotter should already have been called.

    //depth_new
	depth_gauge_new(0,0,0);

	depth_gauge_new(1,0,0);
	//we know which nodes are supernodes, but we don't yet know
	//which of them are static.
	//so the next thing to do is to find and solve static trees.
	build_static_trees(0,0,0,0);
	depth_gauge_new(1,-9,0);
	
	printf("***H***\n");



    //to save us paying the cost of solving sigma_I and sigma_G at each node
    //at every matfree iteration, we calculate them here.
    //compute_static_sigmas();
    //depth_gauge_new(2,-9,0);

    printf("***I***\n");
    
    //to save the cost of traversing a supernode every time we solve its
    //nodes, we calculate the sum of its conductances and internal partial
    //currents here.
    supernode_gather_terms();
    depth_gauge_new(3,-9,0);

    printf("***J***\n");
    
    //To prevent having to do bazillions of divides throughout the iteration,
    //we divide each of the conductances and currents into a node by its
    //sigma_g (or, in the case of supernodes, its grand_sigma_g.)
    invert_sigmas();
    depth_gauge_new(4,-9,0);

    printf("***K***\n");

    //in order to avoid having to walk supernodes multiple times, we do it here,
    //once. Internals and Isums are rolled up into a single value: the
    //premult_offset. Essentially, this changes the sum at every node to being
    //a sum of G'Vs, plus a premult_offset.
    resolve_equations();
    depth_gauge_new(5,-9,0);

    //print_nodes();

    //printf("  Premultiplying NOW.\n");
    
    //matfree_pause();
    
    printf("***L***\n");

  ////Omitting this until pointer method is proven to work.

    ////finally, if we'd like to premultiply/prepower the matrix, we can do so here.
    //matfree_premult(cktin->matfree);  // raises matrix to power (2^matfree)
    //depth_gauge_new(6,-9,0);


    printf("***M***\n");
	
	//now we can iterate.
	while(solution_found == false && iterations <= MAX_ITERATIONS){
	  iterations++;
	  //printf("Iteration #%d:\n", iterations);
	  //print_nodes();
	  zero_sigmas();
	  //printf("  sigmas zeroed...\n");
	  compute_sigmas();
	  depth_gauge_new(7,-9,0);
	  //printf("  sigmas recalculated...\n");
	  //matfree_pause();
	  solution_found = matfree_solve();
	  //printf("Break 4.%d: matfree solve #%d\n", iterations, iterations);
    }
    
    printf("***N***\n");
    
    //if solution found, do stuff.
    if(solution_found){
	
	  //printf("CONVERGED! \n");
      
      //printf("sigmas zeroed \n");
      //printf("isums zeroed \n");
      
	}
    
    else{
		printf("Nonconvergence :( \n");
		iterations = -1;
	}

	//once that's completed, clean up ghosts
	ghost_buster();
	
	//then fill in the internal nodes.
	handle_internals();
	depth_gauge_new(9,-9,0);

	//print the solution.
	print_nodes();
	printf("END OF ITERS.\n");
	matfree_pause();
	 
	//print personal performace
	print_personal_(iterations);

	//zero sigmas, Isums for next round.
	//clear_jotter();


	return iterations;
}
*/

#endif



int
NIiter(ckt,maxIter)
    register CKTcircuit * ckt;
    int maxIter;
{
    int iterno;
    int ipass;
    int error;
    int i,j; /* temporaries for finding error location */
    char *message;  /* temporary message buffer */
    double *temp;
    double startTime;
    static char *msg = "Too many iterations without convergence";

    iterno=0;
    ipass=0;
    
    //the original place in the function that CKTload is called.
    //error = CKTload(ckt);

#ifdef USE_MATFREE
    build_node_jotter( ckt , ckt->CKTmatrix );
#endif // important!!

    CKTload(ckt);


//print_matfree();
//print_nodes();
//abort();

    // PAPI support
    int retval=0;
    long_long papi_counts[2], old_papi_counts[2];

    if( (ckt->CKTmode & MODETRANOP) && (ckt->CKTmode & MODEUIC)){
        temp = ckt->CKTrhsOld;
        ckt->CKTrhsOld = ckt->CKTrhs;
        ckt->CKTrhs = temp;

	// PAPI support
	if(ckt->papi==1) {
		retval = PAPI_read_counters(old_papi_counts,2);
		if(retval != PAPI_OK) {
			printf("Failed to read counters err=%s\n",PAPI_strerror(retval));
		}
	}

	error = CKTload(ckt);
	

	// PAPI support
	if(ckt->papi==1) {
		retval = PAPI_read_counters(papi_counts,2);
		if(retval != PAPI_OK) {
			printf("Failed to read counters err=%s\n",PAPI_strerror(retval));
		}

	if(papi_counts[0]-old_papi_counts[0]>0){
		ckt->modeleval_counts[0]+=papi_counts[0]-old_papi_counts[0];
	}
	if(papi_counts[1]-old_papi_counts[1]>0){
		ckt->modeleval_counts[1]+=papi_counts[1]-old_papi_counts[1];
	}
//	printf("MODELEVAL: 0=%lld, 1=%lld\n",papi_counts[0]-old_papi_counts[0], papi_counts[1]-old_papi_counts[1]);
	}
							
	if(error) {
	    return(error);
	}
        return(OK);
    }

//get around all this PAPI stuff.
#ifdef USE_MATFREE
printf("MNA_interface launching\n");
    MNA_interface();
    printf("***A***\n");
    print_new_matfree();
    abort();
    //link_sn();
    printf("***B***\n");
    //clear_jotter();
    printf("***C***\n");
#endif // important!!

#ifdef HAS_SENSE2
    if(ckt->CKTsenInfo){
        error = NIsenReinit(ckt);
        if(error) return(error);
    }
#endif
    if(ckt->CKTniState & NIUNINITIALIZED) {
        error = NIreinit(ckt);
        if(error){
#ifdef STEPDEBUG
            printf("re-init returned error \n");
#endif
            return(error);
        }
    }
    
printf("***D***\n");
    
    for(;;){
/*        
        printf("==========LOOP:\n");
	SMPprint(ckt->CKTmatrix,stdout);
*/
        ckt->CKTnoncon=0;
#ifdef NEWPRED
        if(!(ckt->CKTmode & MODEINITPRED)) {
#else /* NEWPRED */
        if(1) { /* } */
#endif /* NEWPRED */

/*
printf("BEFORE:\n");
            SMPprint(ckt->CKTmatrix,stdout);
*/
	// PAPI support
	if(ckt->papi==1) {
		retval = PAPI_read_counters(old_papi_counts,2);
		if(retval != PAPI_OK) {
			printf("Failed to read counters err=%s\n",PAPI_strerror(retval));
		}
	}

//check_neighbours(5);
	error = CKTload(ckt);
//check_neighbours(5);
    printf("***E***\n");
#ifdef USE_MATFREE
    //matfree_cktload();
    printf("***F***\n");

#endif // important!!

	// PAPI support
	if(ckt->papi==1) {
		retval = PAPI_read_counters(papi_counts,2);
		if(retval != PAPI_OK) {
			printf("Failed to read counters err=%s\n",PAPI_strerror(retval));
		}
	
	if(papi_counts[0]-old_papi_counts[0]>0){
		ckt->modeleval_counts[0]+=papi_counts[0]-old_papi_counts[0];
	}
	if(papi_counts[1]-old_papi_counts[1]>0){
		ckt->modeleval_counts[1]+=papi_counts[1]-old_papi_counts[1];
	}

//	printf("MODELEVAL: 0=%lld, 1=%lld\n",papi_counts[0]-old_papi_counts[0], papi_counts[1]-old_papi_counts[1]);

	}

/*
            SMPprint(ckt->CKTmatrix,stdout);
printf("AFTER:\n");
*/
            /*printf("loaded, noncon is %d\n",ckt->CKTnoncon);*/
            /*fflush(stdout);*/
            iterno++;
            if(error) {
                ckt->CKTstat->STATnumIter += iterno;
#ifdef STEPDEBUG
                printf("load returned error \n");
#endif
                return(error);
            }
            
            /*printf("after loading, before solving\n");*/
            /*CKTdump(ckt);*/

            if(!(ckt->CKTniState & NIDIDPREORDER)) {
                error = SMPpreOrder(ckt->CKTmatrix);
                if(error) {
                    ckt->CKTstat->STATnumIter += iterno;
#ifdef STEPDEBUG
                    printf("pre-order returned error \n");
#endif
                    return(error); /* badly formed matrix */
                }
                ckt->CKTniState |= NIDIDPREORDER;
            }

            if( (ckt->CKTmode & MODEINITJCT) || 
                    ( (ckt->CKTmode & MODEINITTRAN) && (iterno==1))) {
                ckt->CKTniState |= NISHOULDREORDER;
            }

            int reordered=0; /* Added by Nachiket on May 11th */

            if(ckt->CKTniState & NISHOULDREORDER) {
                startTime = (*(SPfrontEnd->IFseconds))();
                reordered=1;
                error = SMPreorder(ckt->CKTmatrix,ckt->CKTpivotAbsTol,
                        ckt->CKTpivotRelTol,ckt->CKTdiagGmin);


                ckt->CKTstat->STATreorderCount += 1;
                ckt->CKTstat->STATreorderTime += 
                        (*(SPfrontEnd->IFseconds))()-startTime;
                if(error) {
                    /* new feature - we can now find out something about what is
                     * wrong - so we ask for the troublesome entry 
                     */
                    SMPgetError(ckt->CKTmatrix,&i,&j);
                    message = (char *)MALLOC(1000); /* should be enough */
                    (void)sprintf(message,
                            "singular matrix:  check nodes %s and %s\n",
                            NODENAME(ckt,i),NODENAME(ckt,j));
                    (*(SPfrontEnd->IFerror))(ERR_WARNING,message,(IFuid *)NULL);
                    FREE(message);
                    ckt->CKTstat->STATnumIter += iterno;
#ifdef STEPDEBUG
                    printf("reorder returned error \n");
#endif
                    return(error); /* can't handle these errors - pass up! */
                }
                ckt->CKTniState &= ~NISHOULDREORDER;
            } else {

		/** =============================  **/
		/**  Factorize with KLU if asked.. **/
		/** =============================  **/
	        // PAPI support
        	if(ckt->papi==1) {
        		retval = PAPI_read_counters(old_papi_counts,2);
        		if(retval != PAPI_OK) {
        			printf("Failed to read counters err=%s\n",PAPI_strerror(retval));
        		}
        	}

		if(ckt->klu && ckt->enableKLU==1) {
			wrapper_KLU_factor(ckt->CKTmatrix, ckt->CKTrhs, ckt->CKTstat->STATnumIter, ckt); // dont know why the ptr is frakking null



		} else {
                	startTime = (*(SPfrontEnd->IFseconds))();
                	error=SMPluFac(ckt->CKTmatrix,ckt->CKTpivotAbsTol, ckt->CKTdiagGmin);
                	ckt->CKTstat->STATdecompTime += (*(SPfrontEnd->IFseconds))()-startTime;



		}

	        // PAPI support
	        if(ckt->papi==1) {
        		retval = PAPI_read_counters(papi_counts,2);
        		if(retval != PAPI_OK) {
        			printf("Failed to read counters err=%s\n",PAPI_strerror(retval));
        		}
	
		if(papi_counts[0]-old_papi_counts[0]>0){
			ckt->matsolve_counts[0]+=papi_counts[0]-old_papi_counts[0];
		}
		if(papi_counts[1]-old_papi_counts[1]>0){
			ckt->matsolve_counts[1]+=papi_counts[1]-old_papi_counts[1];
		}

//       	        printf("MATSOLVE: 0=%lld, 1=%lld\n",papi_counts[0]-old_papi_counts[0], papi_counts[1]-old_papi_counts[1]);
        	}
							
		/** =============================  **/

                if(error) {
                    if( error == E_SINGULAR ) {
                        ckt->CKTniState |= NISHOULDREORDER;
                        DEBUGMSG(" forced reordering....\n");
                        continue;
                    }
                    /*CKTload(ckt);*/
                    /*SMPprint(ckt->CKTmatrix,stdout);*/
                    /* seems to be singular - pass the bad news up */
                    ckt->CKTstat->STATnumIter += iterno;
#ifdef STEPDEBUG
                    printf("lufac returned error \n");
#endif
                    return(error);
                }
            } 

		/** =============================  **/
	        /** Solve with KLU if requested.. **/
		/** =============================  **/
	        // PAPI support
        	if(ckt->papi==1) {
        		retval = PAPI_read_counters(old_papi_counts,2);
        		if(retval != PAPI_OK) {
        			printf("Failed to read counters err=%s\n",PAPI_strerror(retval));
        		}
        	}

		if(ckt->klu && (ckt->enableKLU==1 && !reordered)) {
//			printf("KLU solve\n");
			wrapper_KLU_solve(ckt->CKTmatrix,ckt->CKTrhs,ckt->CKTstat->STATnumIter,ckt);
		} else {
                        /* Added by Coryan - to get an idea for some of the MatFree stats. */
                        
                        #ifdef USE_MATFREE
                        
                        //printf("Magic commencing...");
                        
                        /*
                        printf("FOUND! use_matfree = %d\n", ckt->use_matfree);
                        
                        if(ckt->use_matfree){
							printf("triggered.\n");
						}
						
						abort();
						*/
                        
						// PAPI support
						/*
						if(ckt->papi==1) {
							retval = PAPI_read_counters(old_papi_counts,2);
							if(retval != PAPI_OK) {
								printf("Failed to read counters err=%s\n",PAPI_strerror(retval));
							}
						}
						*/
                        
                        if(ckt->matfree != -1){
							//if(magic(ckt) == (-1)){
              if(false){
								printf("Matfree solver failed to converge; aborting...\n");
								abort();
							}

						}
						
						//printf(" magic complete.\n");
						
                        
                        // PAPI support
                        /*
	                    if(ckt->papi==1) {
        		          retval = PAPI_read_counters(papi_counts,2);
        		          if(retval != PAPI_OK) {
        			         printf("Failed to read counters err=%s\n",PAPI_strerror(retval));
        		          }
	
						  if(papi_counts[0]-old_papi_counts[0]>0){
							ckt->matfree_counts[0]+=papi_counts[0]-old_papi_counts[0];
						  }
						  if(papi_counts[1]-old_papi_counts[1]>0){
							ckt->matfree_counts[1]+=papi_counts[1]-old_papi_counts[1];
						  }
						}
						*/
                        
                        //printf("STAT : iteration work:\n");
                        //matfree_stat();
                        /* end magic. */
                        
                        #endif
                        
                        
            		startTime = (*(SPfrontEnd->IFseconds))();
			SMPsolve(ckt->CKTmatrix,ckt->CKTrhs,ckt->CKTrhsSpare);
            		ckt->CKTstat->STATsolveTime += (*(SPfrontEnd->IFseconds))()-startTime;

            		
		}

		
	        // PAPI support
	        if(ckt->papi==1) {
        		retval = PAPI_read_counters(papi_counts,2);
        		if(retval != PAPI_OK) {
        			printf("Failed to read counters err=%s\n",PAPI_strerror(retval));
        		}
	
		if(papi_counts[0]-old_papi_counts[0]>0){
			ckt->matsolve_counts[0]+=papi_counts[0]-old_papi_counts[0];
		}
		if(papi_counts[1]-old_papi_counts[1]>0){
			ckt->matsolve_counts[1]+=papi_counts[1]-old_papi_counts[1];
		}

//       	        printf("MATSOLVE: 0=%lld, 1=%lld\n",papi_counts[0]-old_papi_counts[0], papi_counts[1]-old_papi_counts[1]);
        	}
							

							
		/** =============================  **/
#ifdef STEPDEBUG
/*XXXX*/
	    if (*ckt->CKTrhs != 0.0)
		    printf("NIiter: CKTrhs[0] = %g\n", *ckt->CKTrhs);
	    if (*ckt->CKTrhsSpare != 0.0)
		    printf("NIiter: CKTrhsSpare[0] = %g\n", *ckt->CKTrhsSpare);
	    if (*ckt->CKTrhsOld != 0.0)
		    printf("NIiter: CKTrhsOld[0] = %g\n", *ckt->CKTrhsOld);
/*XXXX*/
#endif
            *ckt->CKTrhs = 0;
            *ckt->CKTrhsSpare = 0;
            *ckt->CKTrhsOld = 0;

            if(iterno > maxIter) {
                /*printf("too many iterations without convergence: %d iter's\n",
                        iterno);*/
                ckt->CKTstat->STATnumIter += iterno;
                errMsg = MALLOC(strlen(msg)+1);
                strcpy(errMsg,msg);
#ifdef STEPDEBUG
                    printf("iterlim exceeded \n");
#endif
                return(E_ITERLIM);
            }
            if(ckt->CKTnoncon==0 && iterno!=1) {
                startTime = (*(SPfrontEnd->IFseconds))();
                ckt->CKTnoncon = NIconvTest(ckt);
		ckt->CKTstat->STATconvTestTime+=
                        (*(SPfrontEnd->IFseconds))()-startTime;

            } else {
                ckt->CKTnoncon = 1;
            }
        }

        if(ckt->CKTmode & MODEINITFLOAT) {
            if ((ckt->CKTmode & MODEDC) &&
                    ( ckt->CKThadNodeset)  ) {
                if(ipass) {
                    ckt->CKTnoncon=ipass;
                }
                ipass=0;
            }
            if(ckt->CKTnoncon == 0) {
                ckt->CKTstat->STATnumIter += iterno;
                return(OK);
            }
        } else if(ckt->CKTmode & MODEINITJCT) {
            ckt->CKTmode = (ckt->CKTmode&(~INITF))|MODEINITFIX;
            ckt->CKTniState |= NISHOULDREORDER;
        } else if (ckt->CKTmode & MODEINITFIX) {
            if(ckt->CKTnoncon==0) ckt->CKTmode =
                    (ckt->CKTmode&(~INITF))|MODEINITFLOAT;
            ipass=1;
        } else if (ckt->CKTmode & MODEINITSMSIG) {
            ckt->CKTmode = (ckt->CKTmode&(~INITF))|MODEINITFLOAT;
        } else if (ckt->CKTmode & MODEINITTRAN) {
            if(iterno<=1) ckt->CKTniState |= NISHOULDREORDER;
            ckt->CKTmode = (ckt->CKTmode&(~INITF))|MODEINITFLOAT;
        } else if (ckt->CKTmode & MODEINITPRED) {
            ckt->CKTmode = (ckt->CKTmode&(~INITF))|MODEINITFLOAT;
        } else {
            ckt->CKTstat->STATnumIter += iterno;
#ifdef STEPDEBUG
                printf("bad initf state \n");
#endif
            return(E_INTERN);
            /* impossible - no such INITF flag! */
        }
    
        /* build up the lvnim1 array from the lvn array */
        temp = ckt->CKTrhsOld;
        ckt->CKTrhsOld = ckt->CKTrhs;
        ckt->CKTrhs = temp;
        /*printf("after loading, after solving\n");*/
        /*CKTdump(ckt);*/
    }
    /*NOTREACHED*/
}


