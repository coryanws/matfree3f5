#!/usr/bin/perl -w
use File::Copy;

$maxlevel = 9; #the maximum level of premultiplication we're using

#first of all, empty the "raw" folder. (Important for later.)
opendir($rawhandle, "./../graphs/raw2");
@files = readdir($rawhandle);

foreach $file (@files) {
  if($file !~ /^\./){
    unlink("./../graphs/raw2/$file") or warn "Could not unlink $file: $!";
  }
}

#alright. Now start processing the benchmark files.

opendir($benchmarks, "./../benchmarks/circuit_benchmarks/residue");
@files = readdir($benchmarks);

foreach $file (@files) {
if($file !~ /^\./){

  @frags = split(/\//, $file);
  $fname = $frags[-1];
  @namefrags = split(/\./, $fname);
  $ext = $namefrags[-1];

#  print $file . "\n";
#  print $fname . "\n";
#  print $ext . "\n";
  
  #if it's a .cir or a .sp, we can run it.
  if(($ext =~ /cir/ || $ext =~ /sp/) && ($namefrags[-2] !~ /^ibmpg[3-9]/)){
    
     for($matfree = 0; $matfree <= $maxlevel; $matfree++){
    
  #    print "runnable!!\n";
       print "now running " . $fname . " at level " . $matfree . "...";
       system("./src/bin/spice3_res", "-b", "-d$matfree", "./../benchmarks/circuit_benchmarks/residue/".$file);
       print " complete!\n";
       
       #create and clear a place for files to go.
       #if directory does not exist, make it.
       mkdir("./../graphs/perlgen/".$namefrags[-2]."_res_".$matfree);
       
       #delete stuff already in this directory.
       opendir ($dirhandle, "./../graphs/perlgen/".$namefrags[-2]."_res_".$matfree);
       @contents = readdir($dirhandle);
       foreach $content (@contents){
         if($content !~ /^\./){
           unlink ("./../graphs/perlgen/".$namefrags[-2]."_res_".$matfree."/".$content) or warn "Could not unlink $content: $!";
         }
       }
       
       #rename and move generated output files.
       opendir($ophandle, "./../graphs/raw2");
       @outputs = readdir($ophandle);
       foreach $output (@outputs) {
         if($output !~ /^\./){
           #move files to directory.
           move("./../graphs/raw2/".$output, "./../graphs/perlgen/".$namefrags[-2]."_res_".$matfree."/".$output);
         }
       }
       
        #print "Press enter to continue\n";
        #$input = <>;
       

       
     }

     #rename .cir to let us know that it's done.
     move($file, $file."done");
     
  }
#  print "\n";
}
}
