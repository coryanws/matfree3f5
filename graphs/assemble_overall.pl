#!/usr/bin/perl -w
use File::Copy;
use File::Path;
use utf8; # I hope this will fix the output.

# This script should combobulate all the generated ops_overall.csv files into one.
# When I wrote this script, spice was outputting the data in a weird line (I'd written /n rather than \n to indicate a page break)
# So some of this formatting will be irrelevant in the current version.

# Assuming this is in the main graphs folder: open the perlgen folder.

opendir (my $perlgen, "./perlgen/")
 or die "Cannot open directory: $!";

# get a list of all the dudes in this directory
my @contents = readdir($perlgen);

#sort them, because reasons
@contents = sort @contents;

# also, make a file to dump all this guff into.
open (BIGFILE, '>overall_overall.csv');

my $titled = 0;

# now we're ready.

foreach $content (@contents){
  if ($content !~ /^\./){
  
    print "Found $content \n";
  
    # Open a file.
    open (LITTLEFILE, "<./perlgen/$content/ops_overall.csv")
      or die "Cannot open file: $!";
      
    #next bit only makes sense for this batch of "broken" files.
    #read first line.
    
    my $line = scalar <LITTLEFILE>;	# Grab the first line
    
    my @frags = split("/n", $line);
    
    if($titled == 0){ #print the title.
      print BIGFILE "Name, $frags[0]\n";
      $titled = 1;
    }
    
    print BIGFILE "$content, $frags[1]\n";
  
  }
}


# print some stuff to this file.

print BIGFILE "Some shit\n"; 



# Close the output file. Because we do things properly in Philly.

close(BIGFILE);


