#
# line graph
#
# some of this is copied from http://freesoftware.zona-m.net/how-to-create-stacked-area-graphs-with-gnuplot/
#

set key
set border 3

set output 'E22_total_ops_per_timestep.png'
set datafile separator "," 
set title "total operations per timestep at busiest node\n(E21, transient, 10u 6m UIC)"

# Where to put the legend
# and what it should contain
set key invert reverse Left outside
set key autotitle columnheader

unset yrange
unset xrange

set ylabel "Operations"
set xlabel "Outer Iter"
set xtics 0,20,364
set xtics out nomirror
set ytics out nomirror

#mean (gnuplot can work this out, but I couldn't get the command to work - instead,
#this is a manual result.
f(x) = 2739

set style line 1 linetype 1 pointtype 0 linewidth 1 linecolor rgb "#FFFF00"
set style line 2 linetype 2 pointtype 0 linewidth 1 linecolor rgb "#FF0000"
set style line 3 linetype 3 pointtype 0 linewidth 1 linecolor rgb "#00FF00"
set style line 4 linetype 4 pointtype 0 linewidth 1 linecolor rgb "#0000FF"
set style line 5 linetype 4 pointtype 0 linewidth 1 linecolor rgb "#000000"

# We are plotting columns 2, 3 and 4 as y-values,
# the x-ticks are coming from column 1
plot 'stacked_stats.csv' using 1:6 t "adds" w filledcurves x1 linestyle 4, \
     'stacked_stats.csv' using 1:5 t "subs" w filledcurves x1 linestyle 3, \
     'stacked_stats.csv' using 1:4 t "mults" w filledcurves x1 linestyle 2, \
     'stacked_stats.csv' using 1:3 t "divs" w filledcurves x1 linestyle 1, \
     f(x) with lines lt 5 t "mean = 2739"

set term png size 1024,768        
set output "printme.png"
replot
set term x11

