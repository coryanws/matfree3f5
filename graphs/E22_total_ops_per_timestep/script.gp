#
# line graph
#

set key
set border 3

set output 'E22_total_ops_per_timestep.png'
set datafile separator "," 
set title "total operations per timestep"

# Where to put the legend
# and what it should contain
set key invert reverse Left outside
set key autotitle columnheader

set yrange [30000:90000]
set ylabel "Operations"
set xlabel "Timestep"
set xtics 5
set xtics out nomirror
set ytics out nomirror

# We are plotting columns 2, 3 and 4 as y-values,
# the x-ticks are coming from column 1
plot 'E22_total_ops_per_timestep.csv' using 3:xtic(1) smooth csplines ls 1 lc rgb "green" t "ops per timestep" with lines

set term png size 1024,768        
set output "printme.png"
replot
set term x11

