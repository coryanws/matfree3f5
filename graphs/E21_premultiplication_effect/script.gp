#
# line graph
#
# some of this is copied from http://freesoftware.zona-m.net/how-to-create-stacked-area-graphs-with-gnuplot/
#

set key
set border 3

set output 'E21_total_ops.png'
set datafile separator "," 
set title "total operations\n(E21, transient, 10u 6m UIC)"

# Where to put the legend
# and what it should contain
set key invert reverse Left outside
set key autotitle columnheader

unset yrange
unset xrange

set ylabel "Operations"
set xlabel "Premultiplication factor"
set xtics 0,20,364
set xtics out nomirror
set ytics out nomirror

set style data histogram
set style histogram rowstacked
set style fill solid border -1
set boxwidth 0.75

# We are plotting columns 2, 3 and 4 as y-values,
# the x-ticks are coming from column 1
plot 'premult_effect.csv' using 2:xtic(1) t "adds", \
     '' using 3 t "subs", \
     '' using 4 t "mults", \
     '' using 5 t "divs"

set term png size 1024,768        
set output "printme.png"
replot
set term x11

