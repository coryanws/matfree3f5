/**********
Copyright 2010 University of Pennsylvania, California Institute of Technology.
All rights reserved.
Author: 2013 Coryan Wilson-Shah
**********/

#include "cktdefs.h"
#include "smpdefs.h"
#include <stdbool.h> //because I love me a good bool once in a while.

#define MATFREE_MAX_FANOUT 32
#define MAX_ITER 10000

//reads in a matrix and stores it in matfree_interface.c
int readin_matrix( CKTcircuit * cktin, char * inmatrix  );

//Prints the matrix. Not optimal; only really for debugging.
void print_matfree(void);

//solves the matrix.
int solve_matfree(void);

//print the solution.
void matfree_print_sol(void);

//initialises each of the nodes in the matfree structure.
//should be called before CKTload.
void initialise_matfree( CKTcircuit * cktin );

/////////////////////
// 2nd gen functions:
/////////////////////

//the improved matfree solver.
bool matfree_solve( void );

//builds/initialises node jotter.
void build_node_jotter( CKTcircuit * inckt, char * inmatrix );

//links supernodes (sort of like link_rows)
void link_sn( void );

//updates the boolean properties at a node.
void update_node_properties( int index,
                           bool update_supernode,
                           bool update_static_tree,
                           bool update_external,
                           bool update_ground_neighbour,
                           bool update_gi_inputs,
                           bool update_v_inputs,
                           
                           bool is_supernode,
                           bool is_static_tree,
                           bool is_external,
                           bool is_ground_neighbour,
                           bool has_gi_inputs,
                           bool has_v_inputs );

//loads voltages into static trees. Call AFTER CKTload.
void build_static_trees( int here_node, int prev_node, RealNumber voltage, int step);

//add up g's at each node.
void compute_static_sigmas( void );

//gather supernode internals
void supernode_gather_terms( void );

//divide each G (and Isum/internals) by sigmaG at the (super)node.
void invert_sigmas( void );

//perform the "premultiply" - "matrix" is raised to power (2^n).
void matfree_premult( int n );

//A function for debugging the premultiply.
//can be safely deleted now.
//void check_neighbours( int node );



//handles external nodes.
void handle_external( int thisnode, int from, RealNumber Vtotal, RealNumber * partials, RealNumber * conductances );

//fill in the internal voltages.
void handle_internals( void );

//zeroes the sigma_GVs at each node.
void zero_sigmas( void );

//clears the sigmas, G_Sums, I_Sums, and link and hop data in the jotter.
void clear_jotter( void );

//calculates the sigmas at each node.
void compute_sigmas( void );

//adds a number to a node's sigma_G
void sigma_G_add( int index, int neighbour, RealNumber G_in );

//depreciated:
//adds a number to a node's sigma_GV
void sigma_GV_add( int index, int neighbour, RealNumber G_in );

//adds an isource value to a node's sigma_GV
void sigma_I_add( int index, RealNumber I_in );

//adds a vsource value to a supernode
void supernode_add( int index, int neighbour, RealNumber V_in );

void matfree_pause( void );



//check node info
void print_nodes( void );

//check stats
void matfree_stat( void );

//function to print personal performace
void print_personal( void );
